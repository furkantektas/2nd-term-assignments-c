/*############################################################################*/
/* HW03_111044029.c                                                           */
/* ----------------------                                                     */
/* Created on 08.04.2012 by Furkan Tektas                                     */
/* ­­­­­                                                                           */
/* Description                                                                */
/* ----------------------                                                     */
/* This program calculates the line equations of either one point and slope   */
/* known line or two points known line. Users should first select the         */
/* appropriate method to calculate line equations.                            */
/*                                                                            */
/*                                                                            */
/*############################################################################*/


/*############################################################################*/
/*                                  Includes                                  */
/*############################################################################*/
#include <stdio.h>

/*############################################################################*/
/*                                   Macros                                   */
/*############################################################################*/
#define DEBUG 0

/*############################################################################*/
/*                                  Typedefs                                  */
/*############################################################################*/
typedef enum
    {twoPoints = 1, onePoint = 2}
form_t;

/*­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­----------------------------------------------------------------------------*/
/*                            Function Prototypes                             */
/*­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­----------------------------------------------------------------------------*/

/*
 * Displays the user menu, then inputs and returns as the function value
 * the problem number selected.
 */
form_t GetProblem(void);

/*
 * Prompts the user for the x-y coordinates of both points, inputs the four
 * coordinates, and returns them to the calling function through output
 * parameters.
 */
void Get2Pt(double *pt1x, double *pt1y, double *pt2x, double *pt2y);

/*
 * Prompts the user for the slope and x-y coordinates of both point, inputs the
 * three coordinates, and returns them to the calling function through output
 * parameters.
 */
void GetPtSlope(double *slope, double *ptx, double *pty);

/*
 * Takes four input parameters, the x-y coordinates of two points, and returns
 * through output parameters the slope(m) and y-intercept(b)
 */
void SlopeIntcptFrom2Pt(double pt1x, double pt1y, double pt2x, double pt2y,
                        double *slope, double *yIntercept);
/*
 * Takes three input parameters, the x-y coordinates of one point and the slope
 * and returns as the function value the y-intercept(b).
 */
double IntcptFromPtSlope(double ptx, double pty, double slope);

/*
 * Takes four input parameters, the x-y coordinates of two points, and displays
 * the two-point line equation with a heading.
 */
void Display2Pt(double pt1x, double pt1y, double pt2x, double pt2y,
           double *slope, double *yIntercept);

/*
 * Takes two input parameters, the slope and y-intercept, and displays the
 * slope-intercept line equation with a heading. 
 */
void DisplayPtSlope(double pt1x, double pt1y, double slope);

void DisplaySlopeIntercept(double pt1x, double pt1y, double slope);

/*
 * Takes a double number and prints it with its sign.
 */
void PrintSignedValue(double value);

int
main(void)
{
    double pt1x, pt1y, /* x-y coordinates of the first point */
           pt2x, pt2y, /* x-y coordinates of the second point */
           slope,      /* m */
           yIntercept; /* b */
    char   cont = 'n'; /* sentinel */
           
    do{
        /* Printing problem and return GetProblem() functions result */
        switch(GetProblem())
        {
            case onePoint: /* one point and slope will be given */
                GetPtSlope(&slope, &pt1x, &pt1y); /* Getting slope */

                if(DEBUG)
                    printf("Point = (%.2f,%.2f)\nSlope = %.2f\n", pt1x, pt1y, slope);
                
                /* Calculating y-intercept */
                yIntercept = IntcptFromPtSlope(pt1x, pt1y, slope);
                
                /* Displaying point-slope form*/
                DisplayPtSlope(pt1x, pt1y, slope);
                
                /* Displaying slope-intercept form*/
                DisplaySlopeIntercept(pt1x, pt1y, slope);
                
                break;
            case twoPoints: /* two points will be given */
                Get2Pt(&pt1x, &pt1y, &pt2x, &pt2y); /* Getting two points */

                if(DEBUG)
                    printf("1st point = (%.2f,%.2f)\n2nd point = (%.2f,%.2f)\n",
                            pt1x, pt1y, pt2x, pt2y);
                /* Calculating slope and y-intercept */
                SlopeIntcptFrom2Pt(pt1x, pt1y, pt2x, pt2y, &slope, &yIntercept);
                
                /* Displays the two-point line equation */
                Display2Pt(pt1x, pt1y, pt2x, pt2y, &slope, &yIntercept);
                
                /* Displaying slope-intercept form*/
                DisplaySlopeIntercept(pt1x, pt1y, slope);
                
                break;
            default:
                printf("You've entered an invalid character!\n");
        }
        printf("Do another conversion? (Press Y for yes) =>");
        scanf(" %c", &cont);
    }while((cont == 'y') || (cont == 'Y' ));
    return 1;
}

/*----------------------------------------------------------------------------­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­*/
/*                            Function Implementations                        */
/*----------------------------------------------------------------------------­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­*/
form_t
GetProblem(void)
{
    form_t userChoice;
    printf("Select the form that you would like to convert to slope-intercept form:\n");
    printf("\t[%d] Two-point form (you know two points of line)\n", twoPoints);
    printf("\t[%d] Point slope form (you know line's slope and one point)\n", onePoint);
    
    /* I did type casting to avoid warnings, it does the same thing */
    scanf("%d", (int *)&userChoice);
    
    return userChoice;    
}

void
Get2Pt(double *pt1x, double *pt1y, double *pt2x, double *pt2y)
{
    printf("Enter the x-y coordinates of the first point seperated by a space=>");
    scanf("%lf %lf", pt1x, pt1y);
    
    printf("Enter the x-y coordinates of the second point seperated by a space=>");
    scanf("%lf %lf", pt2x, pt2y);
}

void
GetPtSlope(double *slope, double *ptx, double *pty)
{
    printf("Enter the slope=>");
    scanf("%lf", slope);
    
    printf("Enter the x-y coordinates of the point seperated by a space=>");
    scanf("%lf %lf", ptx, pty);
}

void
SlopeIntcptFrom2Pt(double pt1x, double pt1y, double pt2x, double pt2y,
                   double *slope, double *yIntercept)
{
    /* Checking the denominator not to be zero */
    if( ( (pt2x - pt1x) > 0.0001) || ( (pt2x - pt1x) < -0.0001) )
        *slope =  (pt2y - pt1y) / (pt2x - pt1x);
    else 
        *slope = 0;
    /* Calculating y-intercept by IntcptFromPtSlope function */
    *yIntercept = IntcptFromPtSlope(pt1x, pt1y, *slope);
}

double
IntcptFromPtSlope(double ptx, double pty, double slope)
{
    return pty - (slope * ptx );
}

void
Display2Pt(double pt1x, double pt1y, double pt2x, double pt2y,
           double *slope, double *yIntercept)
{
    printf("\n");
    printf("Two Point Form\n");
    printf("\t%3c((%.2f) - (%.2f))\n", ' ', pt2x, pt1x);
    printf("\tm = -------------------\n");
    printf("\t%3c((%.2f) - (%.2f))\n", ' ', pt2y, pt1y);
    printf("\n");

}

void
DisplayPtSlope(double pt1x, double pt1y, double slope)
{
    printf("\n");
    printf("Point-slope form\n");
    printf("\ty");
    
    PrintSignedValue(-pt1y); /* printing the y0 */
    
    /* show the right-hand of the equation if the slope is not zero */
    if( (slope > 0.0001) || (slope < -0.0001) )
    {
        /* hide coefficient of x if coefficient is 1 */
        if( (slope < 1.0001) && (slope > 0.9999) )
            {
                printf("= x");
                PrintSignedValue(-pt1x);
            }/*if*/
        else
        {
            printf("= %.2f(x", slope);
            PrintSignedValue(-pt1x);
            printf(")");
        }/*else*/
        
    }/*if*/
    else
        printf("=0"); /* print equals zero if slope is zero */
    
}

void
DisplaySlopeIntercept(double pt1x, double pt1y, double slope)
{
    printf("\n");
    printf("Slope-intercept form\n");
    printf("\ty = ");
    
    /* show the x if the slope is not zero */
    if( (slope < -0.0001) || (slope > 0.0001) )
    {
        /* hide coefficient of x if coefficient is 1 */
        if( (slope > 1.0001) || (slope < 0.9999) ) 
            printf("%.2f",  slope);
            
        printf("x");

    }/*if*/
    
    /* Printing y0-m*x0 with sign */
        PrintSignedValue( pt1y - (slope * pt1x) );
        
    printf("\n\n");
}
void
PrintSignedValue(double value)
{
    /* Print '+' (plus sign) if the value is positive */
    if(value > 0)
        printf("+");
    printf("%.2f", value);
}
/*############################################################################*/
/*                             End of HW03_111044029.c                        */
/*############################################################################*/
