/*############################################################################*/
/* HW01_111044029_PART_1.c                                                    */
/* ----------------------                                                     */
/* Created on 13.03.2012 by Furkan Tektas                                     */
/*                                                                            */
/* Description                                                                */
/* ----------------------                                                     */
/* Calculates the given function in hw02c.pdf with user                       */
/* input values within function domain range. Displays error for              */
/* inappropriate input values.                                                */
/*                                                                            */
/*############################################################################*/

/*############################################################################*/
/*                            Includes                                        */
/*############################################################################*/
#include <stdio.h>
#include <math.h>

/*############################################################################*/
/*                                   Macros                                   */
/*############################################################################*/
#define MAX_N 10 /* Maximum value of n */

/*­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­----------------------------------------------------------------------------*/
/*                            Function Prototypes                             */
/*­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­----------------------------------------------------------------------------*/
/*############################################################################*/
/*                                                                            */
/* void printResult(double z, double n, double result)                        */
/* ­­­­­­­­­­­                                                                           */
/*                                                                            */
/*      z, n     ­        arguments of f(z,n)                                  */
/*      result           result of f(z,n), calculated by calculateFunction()  */
/*                                                                            */
/* Description                                                                */
/* ­­­­­­­­­­­                                                                           */
/*     This function takes arguments of f(z,n) and result of the function     */
/*     then prints it with appropriate formatting.                            */
/*                                                                            */
/*############################################################################*/
void printResult(double z, double n, double result);

/*############################################################################*/
/*                                                                            */
/* double calculateFunction(double z, double n)                               */
/* ­­­­­­­­­­­                                                                           */
/*                                                                            */
/*      z, n     ­        arguments of f(z,n)                                  */
/*                                                                            */
/* Return                                                                     */
/* ­­­­­­                                                                           */
/*      (double)         Result of given formula                              */
/*                                                                            */
/* Description                                                                */
/* ­­­­­­­­­­­                                                                           */
/*     This function takes arguments of f(z,n) then calculates and returns    */
/*     the calculated function result.                                        */
/*                                                                            */
/*############################################################################*/
double calculateFunction(double z, double n);

/*############################################################################*/
/*  int main()                                                                */
/*  ----------                                                                */  
/*  Return                                                                    */
/*  ----------                                                                */
/*       0 on success                                                         */
/*      -1 on failure                                                         */
/*############################################################################*/
int
main(void)
{

    double  inputZ, /* user given input */
            valueN, /* second variable of function and counter */
            result; /* calculated value of the function */

    /* Prompt and get inputZ value */
    printf("Enter the z: \t");
    scanf("%lf",&inputZ);

    /* Checking whether inputZ value satisfies the domain of function or not */
    if( (inputZ < 0.99) || (inputZ > 1.001 ) )
    {
        /* Calculating and displaying function for different n values */
        for(valueN=1;valueN<=MAX_N;++valueN)
        {
            /* Calculate the formula */
            result = calculateFunction(inputZ, (double)valueN);
            /* Display result */
            printResult(inputZ, valueN, result);
        }
        /* Everything works fine. Terminate. */
        return 0;
    }
    else
    {
        /* User input does not satisfies the domain. Prompt and terminate */
        printf("z value cannot be zero. Terminating.\n");
        return -1;
    }
}

void
printResult(double z, double n, double result)
{
    printf("\tf(%.0f,%.0f)\t= %-11f\n", z, n, result);    
}

double
calculateFunction(double z, double n)
{
    return sqrt(z * ( ( 1.0 -( (n + 1.0) * pow(z,n) ) + n * pow(z, (n + 1.0) ) )
                    / pow( (1.0 -z), 2.0 ) )
                    + exp(-z * n) );
}
/*############################################################################*/
/*                        End of HW02_111044029_PART_1.c                      */
/*############################################################################*/
