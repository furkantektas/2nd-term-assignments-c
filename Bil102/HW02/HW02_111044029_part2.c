/*############################################################################*/
/* HW02_111044029_PART_2.c                                                    */
/* ----------------------                                                     */
/* Created on 13.03.2012 by Furkan Tektas                                     */
/* ­­­­­                                                                           */
/* Description                                                                */
/* ----------------------                                                     */
/* This program get 5 chars from user and classify each of them with          */
/* different algorithms selected by user. Classification table is defined     */
/* below.                                                                     */
/*                                                                            */
/* Notes                                                                      */
/* ­­­­­-----                                                                      */
/* Number of loops can be changed by TESTCOUNT macro.                         */
/*                                                                            */
/* Classification Table                                                       */
/* --------------------                                                       */
/*                                                                            */
/*  Main Group Number  Sub-Group Number       Letters                         */
/*                           0            a, b, c, A, B, C                    */
/*        0                  1               d, e, D, E                       */
/*                           2               f, g, F, G                       */
/*                           0               h, i, H, I                       */
/*        1                  1               j, k, J, K                       */
/*                           2               l, m, L, M                       */
/*                           0               n, o, N, O                       */
/*        2                  1            P, q, r, P, Q, R                    */
/*                           2               s, t, S, T                       */
/*                           0            u, v, w, U, V, W                    */
/*        3                  1               x, y, X, Y                       */
/*                           2                  z, Z                          */
/*                                                                            */
/*############################################################################*/


/*############################################################################*/
/*                                  Includes                                  */
/*############################################################################*/
#include <stdio.h>

/*############################################################################*/
/*                                   Macros                                   */
/*############################################################################*/
/* Error Codes for printError Method */
#define SWITCHERROR 0
#define IFERROR 1
#define USERDEFINEDERROR 2
#define ILLEGALINPUTERROR 3
#define TERMINATINGERROR 4

/* Method numbers to choosen by user */
#define IFMETHOD 1
#define SWITCHMETHOD 2
#define USERDEFINEDFUNC 3

/* Number of how many letters will be entered */
#define TESTCOUNT 5


/*­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­----------------------------------------------------------------------------*/
/*                            Function Prototypes                             */
/*­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­----------------------------------------------------------------------------*/
/*############################################################################*/
/*                                                                            */
/* int isGroupN(char userInput)                                               */
/* ­­­­­­­­­­­                                                                           */
/*                                                                            */
/*      userInput     ­   Char to determine main group                         */
/*                                                                            */
/* Return                                                                     */
/* ­­­­­­                                                                           */
/*      (int)            Main group number on success, -1 on failure          */
/*                                                                            */
/* Description                                                                */
/* ­­­­­­­­­­­                                                                           */
/*     These functions take a char and returns the main group number if       */
/*     char belongs to main group "N". Note that N is between 0 and 3         */
/*                                                                            */
/*############################################################################*/
int isGroup0(char userInput);
int isGroup1(char userInput);
int isGroup2(char userInput);
int isGroup3(char userInput);

/*############################################################################*/
/*                                                                            */
/* int isSubGroupX2Y(char userInput)                                          */
/* ­­­­­­­­­­­                                                                           */
/*                                                                            */
/*      userInput     ­   Char to determine sub group                          */
/*                                                                            */
/* Return                                                                     */
/* ­­­­­­                                                                           */
/*      (int)            Sub group number on success, -1 on failure           */
/*                                                                            */
/* Description                                                                */
/* ­­­­­­­­­­­                                                                           */
/*     These functions take a char and returns the sub group number if char   */
/*     is between "X" and "Y" characters. "X" and "Y" are english letters.    */
/*                                                                            */
/*############################################################################*/
int isSubGroupA2C(char userInput);
int isSubGroupD2E(char userInput);
int isSubGroupF2G(char userInput);
int isSubGroupH2I(char userInput);
int isSubGroupJ2K(char userInput);
int isSubGroupL2M(char userInput);
int isSubGroupN2O(char userInput);
int isSubGroupP2R(char userInput);
int isSubGroupS2T(char userInput);
int isSubGroupU2W(char userInput);
int isSubGroupX2Y(char userInput);
int isSubGroupZ(char userInput);

/*############################################################################*/
/*                                                                            */
/* int XMethod(char userInput)                                                */
/* ­­­­­­­­­­­                                                                           */
/*                                                                            */
/*      userInput     ­   Char to determine main (and sub) group               */
/*                                                                            */
/* Return                                                                     */
/* ­­­­­­                                                                           */
/*      (int)            +1 on success, -1 on failure                         */
/*                                                                            */
/* Description                                                                */
/* ­­­­­­­­­­­                                                                           */
/*     These functions take a char and determine the main group (and sub)     */
/*     group number of char and pass the group numbers and the letter to the  */
/*     printResult function to display. If function cannot determine the      */
/*     the group numbers, printError function will be called to display an    */
/*     error message. Notice that printError function takes a errorCode       */
/*     argument to determine which error code will be display. Error codes    */
/*     are defined as macro.                                                  */
/*                                                                            */
/*  Note                                                                      */
/*     userDefinedFunction determines only the main group as written in the   */
/*     homework file.                                                         */
/*                                                                            */
/*############################################################################*/
int switchMethod(char userInput);
int ifMethod(char userInput);
int userDefinedMethod(char userInput);

/*############################################################################*/
/*                                                                            */
/* int printResult(char userInput, char mainGroup, char subGroup)             */
/* ­­­­­­­­­­­                                                                           */
/*                                                                            */
/*      userInput     ­   Char to display main (and sub) group                 */
/*      mainGroup        Main group number of char.                           */
/*      subGroup         Sub group number of char. (-1 for not display)       */
/*                                                                            */
/* Return                                                                     */
/* ­­­­­­                                                                           */
/*      (int)            +1 on success                                        */
/*                                                                            */
/* Description                                                                */
/* ­­­­­­­­­­­                                                                           */
/*     This function takes a char and two integer,main group and sub group    */
/*     number of char, and displays the group numbers of the char.            */
/*                                                                            */
/*  Note                                                                      */
/*     If the the sub group number will not be displayed, it should be        */
/*     passed as -1.                                                          */
/*                                                                            */
/*############################################################################*/
int printResult(char userInput, char mainGroup, char subGroup);

/*############################################################################*/
/*                                                                            */
/* int printError(int errorCode)                                              */
/* ­­­­­­­­­­­                                                                           */
/*                                                                            */
/*      errorCode     ­   Corresponding macro of the error.                    */
/*                                                                            */
/* Return                                                                     */
/* ­­­­­­                                                                           */
/*      (int)            -1 on success (Note that this is an error function)  */
/*                                                                            */
/* Description                                                                */
/* ­­­­­­­­­­­                                                                           */
/*     This function takes an error code and displays a corresponding error   */
/*     message. Error codes are defined as macro on the top of the file.      */
/*                                                                            */
/*############################################################################*/
int printError(int errorCode);

/*############################################################################*/
/*                                                                            */
/* int userChoice(void)                                                       */
/* ­­­­­­­­­­­                                                                           */
/*                                                                            */
/* Return                                                                     */
/* ­­­­­­                                                                           */
/*      (int)            +1 on success                                        */
/*      (int)            -1 on failure                                        */
/*                                                                            */
/* Description                                                                */
/* ­­­­­­­­­­­                                                                           */
/*     This function displays a message of which method to be used and passes */
/*     the method number to the methodLoop function.                          */
/*                                                                            */
/* Notes                                                                      */
/*    Function numbers are defined as macro.                                  */
/*                                                                            */
/*############################################################################*/
int userChoice(void);

/*############################################################################*/
/*                                                                            */
/* int methodLoop(int method)                                                 */
/* ­­­­­­­­­­­                                                                           */
/*                                                                            */
/*      method     ­   Method code of which method will be used.               */
/*                                                                            */
/* Return                                                                     */
/* ­­­­­­                                                                           */
/*      (int)            +1 on success                                        */
/*      (int)            -1 on failure                                        */
/*                                                                            */
/* Description                                                                */
/* ­­­­­­­­­­­                                                                           */
/*     This function determines which method to be used and gets a char from  */
/*     from user and filter the input value. If input value is a letter of    */
/*     english alphabet, function passes char to the appropriate method to    */
/*     find main and sub group numbers.                                       */
/*                                                                            */
/*############################################################################*/
int methodLoop(int method);

/*############################################################################*/
/*                                                                            */
/* void userPromptForChar(int tryNumber)                                      */
/* ­­­­­­­­­­­                                                                           */
/*                                                                            */
/*      tryNumber     ­   Loop counter.                                        */
/*                                                                            */
/* Description                                                                */
/* ­­­­­­­­­­­                                                                           */
/*     This function prompts a message for informing user to input a value    */
/*     and how many letter will be used. This function also displays the      */
/*     terminating code(0).                                                   */
/*                                                                            */
/*############################################################################*/
void userPromptForChar(int tryNumber);

/*############################################################################*/
/*                                                                            */
/* int inputControl(char userInput)                                           */
/* ­­­­­­­­­­­                                                                           */
/*                                                                            */
/*      userInput     ­   User input value.                                    */
/*                                                                            */
/* Return                                                                     */
/* ­­­­­­                                                                           */
/*      (int)            +1 if char is an english letter.                     */
/*      (int)            -1 if its not an english letter.                     */
/*                                                                            */
/* Description                                                                */
/* ­­­­­­­­­­­                                                                           */
/*     This function checks whether the user input value is an english letter */
/*     or not. Note that numbers are not letters and value should be between  */
/*     [a-z][A-Z].                                                            */
/*                                                                            */
/*############################################################################*/
int inputControl(char userInput);

/*############################################################################*/
/*                                                                            */
/* void displayEndMessage(void)                                               */
/*                                                                            */
/* Description                                                                */
/* ­­­­­­­­­­­                                                                           */
/*     This function displays the message "Program is terminated due to       */
/*     completion of the maximum number of classifications." before           */
/*     terminating.                                                           */
/*                                                                            */
/*############################################################################*/
void displayEndMessage(void);

/*############################################################################*/
/*  int main()                                                                */
/*  ----------                                                                */  
/*  Return                                                                    */
/*  ----------                                                                */
/*      +1 on success                                                         */
/*      -1 on failure                                                         */
/*############################################################################*/

int
main(void)
{
    return userChoice();
}

/*----------------------------------------------------------------------------­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­*/
/*                            Function Implementations                        */
/*----------------------------------------------------------------------------­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­*/

/* Functions isGroupN                                                         */
/* ­­­­­­­­­­­---------------------------                                                */
/*     This function takes char and returns main group number if the          */
/*     char belongs main group 1. If not, returns -1.                         */
int
isGroup0(char userInput)
{
    return (
              ( isSubGroupA2C(userInput) >= 0 ) ||
              ( isSubGroupD2E(userInput) >= 0 ) ||
              ( isSubGroupF2G(userInput) >= 0 )
           ) ? 0 : -1 ;
}
/* Function isGroupN                                                          */
/* ­­­­­­­­­­­---------------------------                                                */
/*     These functions take a char and returns the main group number if       */
/*     char belongs to main group "N". Note that N is between 0 and 3         */
int
isGroup1(char userInput)
{
    return (
              ( isSubGroupH2I(userInput) >= 0 ) ||
              ( isSubGroupJ2K(userInput) >= 0 ) ||
              ( isSubGroupL2M(userInput) >= 0 )
           ) ? 1 : -1 ;
}

int
isGroup2(char userInput)
{
    return (  
              ( isSubGroupN2O(userInput) >= 0 ) ||
              ( isSubGroupP2R(userInput) >= 0 ) ||
              ( isSubGroupS2T(userInput) >= 0 )
           ) ? 2 : -1 ;
}

int
isGroup3(char userInput)
{
    return (
              ( isSubGroupU2W(userInput) >= 0 ) ||
              ( isSubGroupX2Y(userInput) >= 0 ) ||
              ( isSubGroupZ(userInput)   > 0 )
           ) ? 3 : -1 ;
}

/* Function isSubGroupX2Y                                                     */
/* ­­­­­­­­­­­---------------------------                                                */
/*     These functions take a char and returns the sub group number if char   */
/*     is between "X" and "Y" characters. "X" and "Y" are english letters.    */
int
isSubGroupA2C(char userInput)
{
    return  ( ( ( userInput >= 'a' ) && ( userInput <= 'c' ) ) ||
              ( ( userInput >= 'A' ) && ( userInput <= 'C' ) ) ) ? 0 : -1 ;
}

int
isSubGroupD2E(char userInput)
{
    return  ( ( ( userInput >= 'd' ) && ( userInput <= 'e' ) ) ||
              ( ( userInput >= 'D' ) && ( userInput <= 'E' ) ) ) ? 1 : -1 ;
}

int
isSubGroupF2G(char userInput)
{
    return  ( ( ( userInput >= 'f' ) && ( userInput <= 'g' ) ) ||
              ( ( userInput >= 'F' ) && ( userInput <= 'G' ) ) ) ? 2 : -1 ;
}

int
isSubGroupH2I(char userInput)
{
    return  ( ( ( userInput >= 'h' ) && ( userInput <= 'i' ) ) ||
              ( ( userInput >= 'H' ) && ( userInput <= 'I' ) ) ) ? 0 : -1 ;
}

int
isSubGroupJ2K(char userInput)
{
    return  ( ( ( userInput >= 'j' ) && ( userInput <= 'k' ) ) ||
              ( ( userInput >= 'J' ) && ( userInput <= 'K' ) ) ) ? 1 : -1 ;
}

int
isSubGroupL2M(char userInput)
{
    return  ( ( ( userInput >= 'l' ) && ( userInput <= 'm' ) ) ||
              ( ( userInput >= 'L' ) && ( userInput <= 'M' ) ) ) ? 2 : -1 ;
}

int
isSubGroupN2O(char userInput)
{
    return  ( ( ( userInput >= 'n' ) && ( userInput <= 'o' ) ) ||
              ( ( userInput >= 'N' ) && ( userInput <= 'O' ) ) ) ? 0 : -1 ;
}

int
isSubGroupP2R(char userInput)
{
    return  ( ( ( userInput >= 'p' ) && ( userInput <= 'r' ) ) ||
              ( ( userInput >= 'P' ) && ( userInput <= 'R' ) ) ) ? 1 : -1 ;
}

int
isSubGroupS2T(char userInput)
{
    return  ( ( ( userInput >= 's' ) && ( userInput <= 't' ) ) ||
              ( ( userInput >= 'S' ) && ( userInput <= 'T' ) ) ) ? 2 : -1 ;
}

int
isSubGroupU2W(char userInput)
{
    return  ( ( ( userInput >= 'u' ) && ( userInput <= 'w' ) ) ||
              ( ( userInput >= 'U' ) && ( userInput <= 'W' ) ) ) ? 0 : -1 ;
}

int
isSubGroupX2Y(char userInput)
{
    return  ( ( ( userInput >= 'x' ) && ( userInput <= 'y' ) ) ||
              ( ( userInput >= 'X' ) && ( userInput <= 'Y' ) ) ) ? 1 : -1 ;
}

int
isSubGroupZ(char userInput)
{
    return  ( ( userInput == 'z' ) || ( userInput == 'Z' )  ) ? 2 : -1 ;
}

/* Function XMethod                                                           */
/* ­­­­­­­­­­­---------------------------                                                */
/*     These functions take a char and determine the main group (and sub)     */
/*     group number of char and pass the group numbers and the letter to the  */
/*     printResult function to display. If function cannot determine the      */
/*     the group numbers, printError function will be called to display an    */
/*     error message. Notice that printError function takes a errorCode       */
/*     argument to determine which error code will be display. Error codes    */
/*     are defined as macro.                                                  */
int
switchMethod(char userInput)
{
    int mainGroup, subGroup;

    mainGroup = -1;
    subGroup  = -1;
    switch(userInput)
    {
        case 'a':
        case 'b':
        case 'c':
        case 'A':
        case 'B':
        case 'C':
           mainGroup = 0;
           subGroup  = 0;
        break;
        case 'd':
        case 'e':
        case 'D':
        case 'E':
           mainGroup = 0;
           subGroup  = 1;
        break;
        case 'f':
        case 'g':
        case 'F':
        case 'G':
           mainGroup = 0;
           subGroup  = 2;
        break;
        case 'h':
        case 'i':
        case 'H':
        case 'I':
           mainGroup = 1;
           subGroup  = 0;
        break;
        case 'j':
        case 'k':
        case 'J':
        case 'K':
           mainGroup = 1;
           subGroup  = 1;
        break;
        case 'l':
        case 'm':
        case 'L':
        case 'M':
           mainGroup = 1;
           subGroup  = 2;
        break;
        case 'n':
        case 'o':
        case 'N':
        case 'O':
           mainGroup = 2;
           subGroup  = 0;
        break;
        case 'p':
        case 'q':
        case 'r':
        case 'P':
        case 'Q':
        case 'R':
           mainGroup = 2;
           subGroup  = 1;
        break;
        case 's':
        case 't':
        case 'S':
        case 'T':
           mainGroup = 2;
           subGroup  = 2;
        break;
        case 'u':
        case 'v':
        case 'w':
        case 'U':
        case 'V':
        case 'W':
           mainGroup = 3;
           subGroup  = 0;
        break;
        case 'x':
        case 'y':
        case 'X':
        case 'Y':
           mainGroup = 3;
           subGroup  = 1;
        break;
        case 'z':
        case 'Z':
           mainGroup = 3;
           subGroup  = 2;
        break;
        default:
            printf("Cannot find group in switchMethod function.\n");
    }
    if( (mainGroup >= 0) && (subGroup >= 0) )
        return printResult(userInput, mainGroup, subGroup);
    else
        return printError(SWITCHERROR);
}
int ifMethod(char userInput)
{
    int mainGroup, subGroup;
    mainGroup = -1;
    subGroup  = -1;
    
    if( isGroup0(userInput) >= 0 )
    {
        mainGroup = 0;
        if( isSubGroupA2C(userInput) >= 0 )
            subGroup = isSubGroupA2C(userInput);
        else if( isSubGroupD2E(userInput) >= 0 )
            subGroup = isSubGroupD2E(userInput);
        else if( isSubGroupF2G(userInput) >= 0 )
            subGroup = isSubGroupF2G(userInput);
    }
    else if( isGroup1(userInput) >= 0 )
    {
        mainGroup = 1;
        if( isSubGroupH2I(userInput) >= 0 )
            subGroup = isSubGroupH2I(userInput);
        else if( isSubGroupJ2K(userInput) >= 0 )
            subGroup = isSubGroupJ2K(userInput);
        else if( isSubGroupL2M(userInput) >= 0 )
            subGroup = isSubGroupL2M(userInput);
    }
    else if( isGroup2(userInput) >= 0 )
    {
        mainGroup = 2;
        if( isSubGroupN2O(userInput) >= 0 )
            subGroup = isSubGroupN2O(userInput);    
        else if( isSubGroupP2R(userInput) >= 0 )
            subGroup = isSubGroupP2R(userInput);
        else if( isSubGroupS2T(userInput) >= 0 )
            subGroup = isSubGroupS2T(userInput);
    }
    else if( isGroup3(userInput) >= 0 )
    {
        mainGroup = 3;
        if( isSubGroupU2W(userInput) >= 0 )
            subGroup = isSubGroupU2W(userInput);
        else if( isSubGroupX2Y(userInput) >= 0 )
            subGroup = isSubGroupX2Y(userInput);
        else if( isSubGroupZ(userInput) >= 0 )
            subGroup = isSubGroupZ(userInput);
    }
    if( (mainGroup >= 0) && (subGroup >= 0) )
        return printResult(userInput, mainGroup, subGroup);
    else
        return printError(IFERROR);
}

int
userDefinedMethod(char userInput)
{
    int mainGroup;
    
    if( isGroup0(userInput) >= 0)
        mainGroup = isGroup0(userInput);
    else if( isGroup1(userInput) >= 0 )
        mainGroup = isGroup1(userInput);
    else if( isGroup2(userInput) >= 0 )
        mainGroup = isGroup2(userInput);
    else if( isGroup3(userInput) >= 0 )
        mainGroup = isGroup3(userInput);
    else mainGroup = -1;
    printResult(userInput, mainGroup,-1);
    return mainGroup;
}

int
printResult(char userInput, char mainGroup, char subGroup)
{
    if(subGroup >= 0)
        printf("\t%c is in group %d.%d\n", userInput, mainGroup, subGroup);
    else
        printf("\t%c is in group %d\n", userInput, mainGroup);
     return 1;
}

/* Function printError                                                        */
/* ­­­­­­­­­­­---------------------------                                                */
/*     This function takes an error code and displays a corresponding error   */
/*     message. Error codes are defined as macro on the top of the file.      */
int
printError(int errorCode)
{
    switch(errorCode)
    {
        case IFERROR:
            printf("\nSomething goes wrong in ifMethod. Terminating.\n");
            break;
        case SWITCHERROR:
            printf("\nSomething goes wrong in switchMethod! Terminating.\n");
            break;
        case USERDEFINEDERROR:
            printf("\nSomething goes wrong in userDefinedMethod! Terminating.\n");
            break;
        case ILLEGALINPUTERROR:
            printf("\tIllegal Choice!\n");
            break;
        case TERMINATINGERROR:
            printf("\tTerminating..");
            break;
        default:
            printf("\tAn error occured!\n");
     }
     return -1;
}

/* Function userChoice                                                        */
/* ­­­­­­­­­­­---------------------------                                                */
/*     This function determines which method to be used and gets a char from  */
/*     from user and filter the input value. If input value is a letter of    */
/*     english alphabet, function passes char to the appropriate method to    */
/*     find main and sub group numbers.                                       */
int
userChoice(void)
{
    int ChoosenMethod;
    printf("Which implementation method do you prefer \n");
    printf("\t%d - If Clauses\n", IFMETHOD);
    printf("\t%d - Switch Clauses\n", SWITCHMETHOD);
    printf("\t%d - User-defined Functions\n", USERDEFINEDFUNC);
    scanf("%d", &ChoosenMethod);
    if ( methodLoop(ChoosenMethod) >= 0)
        return 1;
    else
        return -1;
}

/* Function methodLoop                                                        */
/* ­­­­­­­­­­­---------------------------                                                */
/*     This function prompts a message for informing user to input a value    */
/*     and how many letter will be used. This function also displays the      */
/*     terminating code(0).                                                   */
int
methodLoop(int method)
{
    int counter;
    char userInput,
         tempgarbage;
    
    counter = 1;
    
    switch(method)
    {
        case IFMETHOD:
            while(counter <= TESTCOUNT)
            {
                userPromptForChar(counter);
                scanf(" %c%c", &userInput, &tempgarbage);
                if( userInput == '0')
                {
                    printError(TERMINATINGERROR);
                    return -1;
                }
                else if( inputControl(userInput) >= 0 )
                    ifMethod(userInput);
                else
                {
                    printError(ILLEGALINPUTERROR);
                    
                }
                ++counter;
            }
        break;
        case SWITCHMETHOD:
            while(counter <= TESTCOUNT)
            {
                userPromptForChar(counter);
                scanf(" %c%c", &userInput, &tempgarbage);
                if( userInput == '0')
                {
                    printError(TERMINATINGERROR);
                    return -1;
                }
                else if( inputControl(userInput) >= 0 )
                    switchMethod(userInput);
                else
                {
                    printError(ILLEGALINPUTERROR);
                }
                counter++;
            }
        break;
        case USERDEFINEDFUNC:
            while(counter <= TESTCOUNT)
            {
                userPromptForChar(counter);
                scanf(" %c%c", &userInput, &tempgarbage);
                if( userInput == '0')
                {
                    printError(TERMINATINGERROR);
                    return -1;
                }
                else if( inputControl(userInput) >= 0 )
                    userDefinedMethod(userInput);
                else
                {
                    printError(ILLEGALINPUTERROR);
                }
                counter++;
            }
        break;
        default:
            printError(ILLEGALINPUTERROR); /* Display illegal choice */
            return -1;
    }
    displayEndMessage();
    return 1;
}

/* Function userPromptForChar                                                 */
/* ­­­­­­­­­­­---------------------------                                                */
/*     This function prompts a message for informing user to input a value    */
/*     and how many letter will be used. This function also displays the      */
/*     terminating code(0).                                                   */
void
userPromptForChar(int tryNumber)
{
    printf ("Enter the character: (0 to terminate)(%d/%d)\t", tryNumber, TESTCOUNT);
}

/* Function inputControl                                                      */
/* ­­­­­­­­­­­---------------------------                                                */
/*     This function checks whether the user input value is an english letter */
/*     or not. Note that numbers are not letters and value should be between  */
/*     [a-z][A-Z].                                                            */
int
inputControl(char userInput)
{
    if( ( (userInput >= 'a') && (userInput <= 'z' ) ) ||
        ( (userInput >= 'A') && (userInput <= 'Z' ) ) )
        return 1;
    else
        return -1;
}
/* Function displayEndMessage                                                 */
/* ­­­­­­­­­­­---------------------------                                                */
/*     This function displays the message "Program is terminated due to       */
/*     completion of the maximum number of classifications." before           */
/*     terminating.                                                           */
void
displayEndMessage(void)
{
    printf("Program is terminated due to completion of the maximum number of classifications.");    
}
/*############################################################################*/
/*                        End of HW02_111044029_PART_2.c                      */
/*############################################################################*/
