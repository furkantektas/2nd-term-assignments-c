/*############################################################################*/
/* HW05_111044029_PART2.c                                                     */
/* ----------------------                                                     */
/* Created on 27.04.2012 by Furkan Tektas                                     */
/* ­­­­­                                                                           */
/* Description                                                                */
/* ----------------------                                                     */
/* This program gets two polynomial from INPFILE1 and INPFILE2 files and can  */
/* multiply or sum them. Users can evaluate the value of them.                */
/*                                                                            */
/*############################################################################*/


/*############################################################################*/
/*                                  Includes                                  */
/*############################################################################*/
#include <stdio.h>
#include <string.h>

/*############################################################################*/
/*                                   Macros                                   */
/*############################################################################*/
#define NOT_FOUND -1
#define DEBUG 1
#define MAXSTRSIZE 80 /* default terminal line length */

char * MakePlural(char *str, int strSize);

char * Insert(char *source, const char *to_insert, int index);

int Pos(const char *source, const char *to_find);

int
main(void)
{
    char words[MAXSTRSIZE];
    
    printf("Enter word(s) seperated by space to make them plural:\n");
    
    gets(words);
    words[MAXSTRSIZE-1] = '\0';
    
    MakePlural(words, strlen(words));
    
    printf("Plural forms of the words above are:\n");
    printf("%s", words);
    
    return 1;
}

char *
MakePlural(char *str, int strSize)
{
    char *word, 
         postfix[3];
    int  positionStr  = 0,
         positionWord = 0;

    word = strtok (str," ");/*splitting words */

    while (word != NULL && positionStr < strSize)
    {
        if(DEBUG)
            printf ("word: %s\n",word);

        positionWord = strlen(word);
        
        if(DEBUG)
            printf("Last char of word: %c\n", word[positionWord-1]);
            
        switch(word[positionWord-1])
        {
            case 's':
            case 'S':
                strcpy(postfix, "es");
                break;
            case 'y':
            case 'Y':
                strcpy(postfix, "ies");
                --positionWord;
                break;
            default:
                strcpy(postfix, "s");
        }
        
        positionStr += positionWord + strlen(postfix);
        
        Insert(str, postfix, positionStr+positionWord);
        
        if(DEBUG)
            printf("str after insert: %s\n", str);
        
        
        if(DEBUG)
            printf("positionStr:%d | strSize:%d\n", positionStr, strSize);
            
        if(positionStr < strSize)
        {
            printf("%d", Pos(str, ''));
            str[Pos(str, NULL)] = ' ';
        }

        if(DEBUG)
            printf("new word: %s \n", word);

        word = strtok(&str[positionStr]," ");
        if(DEBUG)
            printf("get word:%s\n",word);
    }
     printf("ok:%s\n",str);
     for(positionStr = 0;positionStr<strSize;++positionStr)
        printf("%c", str[positionStr]);
    printf("\n");
    
    str[strSize+1] = '\0';
    
    return str;
}

char *
Insert(char *source, const char *to_insert, int index)
{
    char rest_str[MAXSTRSIZE];
    
    if(strlen(source) <= index)
        strcat(source, to_insert);
    else
    {
        strcpy(rest_str, &source[index]);
        strcpy(&source[index], to_insert);
        strcat(source, rest_str);
    }
    
    return (source);
}

int
Pos(const char *source, const char *to_find)
{
    int i = 0, find_len, found = 0, position;
    char substring[MAXSTRSIZE];
    
    find_len = strlen(to_find);
    
    while(!found && i <= strlen(source) - find_len)
    {
        strncpy(substring, &source[i], find_len);
        substring[find_len] = '\0';
        
        if(strcmp(substring, to_find) == 0)
            found = 1;
        else
            ++i;
    }

    if(found)
        position = i;
    else
        position = NOT_FOUND;
        
    return (position);
}

/*############################################################################*/
/*                         End of HW05_111044029_PART2.c                      */
/*############################################################################*/
