#include<stdio.h>
#include<stdlib.h> /* system() */
#include<unistd.h> /* usleep() */
#include<ctype.h> /* toupper() */

#include "HW5_111044029_PART1_Postures.c"

/* Platform Dependent Codes. Default macros are valid for *nix-like systems. */
#define CLEARCMD "clear" /* LINUX & *NIX */
/* Uncomment the following line and comment the above for windows */
/* #define CLEARCMD "cls"  WINDOWS */


#define SLEEPTIME 30*1000 /* time between gestures */


/*
 * Matrix Operations
 */
void InitializeMatrix(char matrix[][WIDTH]);

/*
 * Screen operations
 */
void Clear(void); 
void PrintMatrix(char matrix[][WIDTH]);

/*
 * Movements
 */
void Move(char cinAli[][WIDTH]);
void WalkLeft(char cinAli[][WIDTH]);
void WalkRight(char cinAli[][WIDTH]);
void Jump(char cinAli[][WIDTH]);
void Stand(char cinAli[][WIDTH]);


int
main(void)
{
    char cinAli[HEIGHT][WIDTH];
    int i;
    InitializeMatrix(cinAli);
    
    do{
    for(i=0; i<3; ++i){
    zeybek1(cinAli);
    PrintMatrix(cinAli);
    
    zeybek2(cinAli);
    PrintMatrix(cinAli);
    
    zeybek3(cinAli);
    PrintMatrix(cinAli);
    }
    zeybek4(cinAli);
    PrintMatrix(cinAli);
    
    zeybek5(cinAli);
    PrintMatrix(cinAli);
    
    zeybek6(cinAli);
    PrintMatrix(cinAli);
    
    zeybek7(cinAli);
    PrintMatrix(cinAli);
    
    zeybek8(cinAli);
    PrintMatrix(cinAli);
    
    zeybek9(cinAli);
    PrintMatrix(cinAli);
    
    zeybek10(cinAli);
    PrintMatrix(cinAli);
    
    /* just4fun :) */
    subliminalMessage(cinAli);
    PrintMatrix(cinAli);
    }while  (1);
    
    
    Move(cinAli);
    
    return 0;
}

void
Move(char cinAli[][WIDTH])
{
    char command, ignore;
    
    do
    {
        Stand(cinAli);
        
        printf("Select the movement: (Q(uit)\n\n");
        
        printf("\tWalk:\n");
        printf("\t\tL(eft), R(ight)\n\n");
        
        printf("\tArtistic Movemnts:\n");
        printf("\t\tJ(ump), Z(eybek)\n");
        
        scanf(" %c", &command);

        do
            ignore = getchar();
        while(ignore != '\n');
        
        command = toupper(command);
        
        Stand(cinAli);
        switch(command)
        {
            case 'L':
                WalkLeft(cinAli);
                break;
            case 'R':
                WalkRight(cinAli);
                break;
            case 'J':
                Jump(cinAli);
                break;
            case 'Q':
                printf("Ali goes home! Bye bye!\n");
                break;
            default:
                printf("Let Ali move!\n");
                usleep(SLEEPTIME*5);
        }

    }while( command != 'Q' );
}
/*
 * Pre: WIDTH and HEIGHT macros are both defined.
 */
void
PrintMatrix(char matrix[][WIDTH])
{
    int i,j;

    Clear(); /* clearing screen */
    for(i = 0; i < HEIGHT; ++i)
    {
        for(j = 0; j < WIDTH; ++j)
            printf("%c", matrix[i][j]);
        printf("\n");
    }
    /*Displaying Ground */
    printf("------------------------------------------\n");
    InitializeMatrix(matrix);
    usleep(SLEEPTIME); /* sleep */
}

/*
 * Pre: WIDTH and HEIGHT macros are both defined.
 */
void
InitializeMatrix(char matrix[][WIDTH])
{
    int i,j;
    for(i = 0; i < HEIGHT; ++i)
        for(j = 0; j < WIDTH; ++j)
            matrix[i][j] = ' ';
}

/*
 * Note that system() function's parameters are platform-dependent.
 * You should change the macros, according to your OS.
 * Default CLEARCMD macro is valid for Unix - Linux systems.
 */
void
Clear(void)
{
    system(CLEARCMD);
}

/*
 * Pre: WIDTH and HEIGHT macros are both defined.
 *      cinAli should be filled with ' ' ( by InitializeMatrix() )
 */
void
WalkLeft(char cinAli[][WIDTH])
{
    toLeftFootsDown(cinAli);
    PrintMatrix(cinAli);

    toLeftBeckFootUp(cinAli);
    PrintMatrix(cinAli);

    toLeftRightFootUp(cinAli);
    PrintMatrix(cinAli);

    toLeftCenteral(cinAli);
    PrintMatrix(cinAli);

    Stand(cinAli);
}

/*
 * Pre: WIDTH and HEIGHT macros are both defined.
 *      cinAli should be filled with ' ' ( by InitializeMatrix() )
 */
void
WalkRight(char cinAli[][WIDTH])
{
    toLeftFootsDown(cinAli);
    PrintMatrix(cinAli);

    toLeftBeckFootUp(cinAli);
    PrintMatrix(cinAli);

    toLeftRightFootUp(cinAli);
    PrintMatrix(cinAli);

    toLeftCenteral(cinAli);
    PrintMatrix(cinAli);

    Stand(cinAli);
}

/*
 * Pre: WIDTH and HEIGHT macros are both defined.
 *      cinAli should be filled with ' ' ( by InitializeMatrix() )
 */
void
Jump(char cinAli[][WIDTH])
{
    jump1(cinAli);
    PrintMatrix(cinAli);

    jump2(cinAli);
    PrintMatrix(cinAli);

    jump3(cinAli);
    PrintMatrix(cinAli);

    jump2(cinAli);
    PrintMatrix(cinAli);
    
    jump1(cinAli);
    PrintMatrix(cinAli);

    Stand(cinAli);
}

/*
 * Pre: WIDTH and HEIGHT macros are both defined.
 *      cinAli should be filled with ' ' ( by InitializeMatrix() )
 */
void
Stand(char cinAli[][WIDTH])
{
    frontPosture(cinAli);
    PrintMatrix(cinAli);
}
