/*############################################################################*/
/* HW05_111044029_PART2.c                                                     */
/* ----------------------                                                     */
/* Created on 27.04.2012 by Furkan Tektas                                     */
/* ­­­­­                                                                           */
/* Description                                                                */
/* ----------------------                                                     */
/* This program gets word(s) from user and prints its plural forms.           */
/* ­­­­­                                                                           */
/* Notes                                                                      */
/* ------                                                                     */
/* This program can convert right only singular words which has a regular     */
/* plural form.                                                               */
/* ­­­­­                                                                           */
/*############################################################################*/


/*############################################################################*/
/*                                  Includes                                  */
/*############################################################################*/
#include <stdio.h>
#include <string.h>
#include <ctype.h> /* toupper */

/*############################################################################*/
/*                                   Macros                                   */
/*############################################################################*/
#define NOT_FOUND -1
#define DEBUG 0
#define MAXSTRSIZE 80 /* default terminal line length */

char * MakePlural(char *str, int strSize);

char * Insert(char *source, const char *to_insert, int index);
char *Delete(char *source, int index, int n);
char *FindAndReplace(char* source, const char* toFind, const char* toReplace); 


int Pos(const char *source, const char *to_find);

int
main(void)
{
    char words[MAXSTRSIZE];
    
    printf("Enter word(s) seperated by space to make them plural:\n");
    
    gets(words);
    Insert(words, " ", strlen(words));
    words[MAXSTRSIZE-1] = '\0';
    
    MakePlural(words, strlen(words));
    
    printf("Plural forms of the words above are:\n");
    printf("%s", words);
    
    return 1;
}

char *
MakePlural(char *str, int strSize)
{
    char postfix[3],
         temp;
    int  positionWord = 0;

    positionWord += Pos(&str[positionWord], " ");
    while (positionWord != NOT_FOUND && positionWord < strSize)
    {
        
        if(DEBUG)
            printf("positionWord:%d\n", positionWord);
        if(DEBUG)
            printf("Last char of word: %c\n", str[positionWord-1]);


        switch(toupper(str[positionWord-1]))
        {
            case 'H': /* word ends with 'ch', or 'sh' */
                temp = toupper(str[positionWord-2]);
                if(temp == 'C' || temp == 'S')
            case 'S': 
            case 'X':
            case 'O':
                strcpy(postfix, "es");
                break;
            case 'Y':
                strcpy(postfix, "ies");
                Delete(str, positionWord-1, 1);
                --positionWord;
                break;
            default:
                strcpy(postfix, "s");
        }     
        Insert(str, postfix, positionWord);
        positionWord += strlen(postfix) + 1;
        strSize += strlen(postfix);

        
        if(DEBUG)
            printf("str after insert: |%s|\n", str);
        
        
        
        positionWord += Pos(&str[positionWord], " ");
        if(DEBUG)
            printf("positionWord:%d | strSize:%d\n", positionWord, strSize);
            
            
    }
    printf("\n");
        
    str[strSize+1] = '\0';
    
    return str;
}

char *
Insert(char *source, const char *to_insert, int index)
{
    char rest_str[MAXSTRSIZE];
    
    if(strlen(source) <= index)
        strcat(source, to_insert);
    else
    {
        strcpy(rest_str, &source[index]);
        strcpy(&source[index], to_insert);
        strcat(source, rest_str);
    }
    
    return (source);
}

char *
Delete(char *source,  /* input/output - string from which to delete part */
       int   index,   /* input - index of first char to delete           */
       int   n)       /* input - number of chars to delete               */
{
      char rest_str[MAXSTRSIZE];  /* copy of source substring following 
                                  characters to delete */

      /*  If there are no characters in source following portion to 
          delete, delete rest of string */
      if (strlen(source) <= index + n) {
            source[index] = '\0';

      /*  Otherwise, copy the portion following the portion to delete
          and place it in source beginning at the index position        */
      } else {
            strcpy(rest_str, &source[index + n]);
            strcpy(&source[index], rest_str);
      }

      return (source);
}

char *
FindAndReplace(char* source, const char* toFind, const char* toReplace)
{
    int position;
    
    position = Pos(source, toFind);
    if(strcmp(toFind, toReplace) == 0)
        printf("Strings are equal!\n");
    else
    {
        while(position != NOT_FOUND)
        {
            Delete(source, position, strlen(toFind));
            Insert(source, toReplace, position);
            position = Pos(source, toFind);
        }
    }
    
    return (source);
}

int
Pos(const char *source, const char *to_find)
{
    int i = 0, find_len, found = 0, position;
    char substring[MAXSTRSIZE];
    
    find_len = strlen(to_find);
    
    while(!found && i <= strlen(source) - find_len)
    {
        strncpy(substring, &source[i], find_len);
        substring[find_len] = '\0';
        
        if(strcmp(substring, to_find) == 0)
            found = 1;
        else
            ++i;
    }

    if(found)
        position = i;
    else
        position = NOT_FOUND;
        
    return (position);
}
/*############################################################################*/
/*                         End of HW05_111044029_PART2.c                      */
/*############################################################################*/
