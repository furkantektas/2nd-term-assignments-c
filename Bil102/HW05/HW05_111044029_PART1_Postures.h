/* HW05_111044029_PART1-Postures.h */
#ifndef POSTURES_H_INCLUDED
#define POSTURES_H_INCLUDED

/* Dimension of the character array */
#define HEIGHT 12
#define WIDTH 7

/* Zeybek default position */
#define ZEYBEKHEIGHT 0

/* Texture shape of the character */
#define TEXTURE 'o'

/* terminal colors */
#define RESETCOLOR  "\033[0m"
#define BROWNBG     "\033[43m"
#define WHITEFG     "\033[37m"
#define BOLDTEXT    "\033[1m"

/* State Postures */
void frontPosture(char [][WIDTH]);
void leftPosture(char [][WIDTH]);
void rightPosture(char [][WIDTH]);

/* Right Motion Postures */
void toRightFootsDown(char [][WIDTH]);
void toRightBeckFootUp(char [][WIDTH]);
void toRightCenteral(char [][WIDTH]);
void toRightFrontFootUp(char [][WIDTH]);

/* Left Motion Postures */
void toLeftFootsDown(char [][WIDTH]);
void toLeftBeckFootUp(char [][WIDTH]);
void toLeftRightFootUp(char [][WIDTH]);
void toLeftCenteral(char [][WIDTH]);


void jump1(char character[][WIDTH]);
void jump2(char character[][WIDTH]);
void jump3(char character[][WIDTH]);


/* Play Zeybek Body Parts' Movements */
void zeybekBody(char character[][WIDTH]);

/* left arm */
void zeybekLeftArm1 (char character[][WIDTH], int height);
void zeybekLeftArm2 (char character[][WIDTH], int height);
void zeybekLeftArm3 (char character[][WIDTH], int height);
void zeybekLeftArm4 (char character[][WIDTH], int height);

/* right arm */
void zeybekRightArm1 (char character[][WIDTH], int height);
void zeybekRightArm2 (char character[][WIDTH], int height);
void zeybekRightArm3 (char character[][WIDTH], int height);
void zeybekRightArm4 (char character[][WIDTH], int height);

/* foot */
void zeybekFootBase(char character[][WIDTH]);
void zeybekFoot1(char character[][WIDTH]);
void zeybekFoot2(char character[][WIDTH]);
void zeybekFoot3(char character[][WIDTH]);
void zeybekFoot4(char character[][WIDTH]);
void zeybekFoot5(char character[][WIDTH]);
void zeybekFoot6(char character[][WIDTH]);

/* Play Zeybek Postures */
void zeybek1 (char character[][WIDTH]);
void zeybek2 (char character[][WIDTH]);
void zeybek3 (char character[][WIDTH]);
void zeybek4 (char character[][WIDTH]);
void zeybek5 (char character[][WIDTH]);
void zeybek6 (char character[][WIDTH]);
void zeybek7 (char character[][WIDTH]);
void zeybek8 (char character[][WIDTH]);
void zeybek9 (char character[][WIDTH]);
void zeybek10(char character[][WIDTH]);

#endif /* POSTURES_H_INCLUDED */
