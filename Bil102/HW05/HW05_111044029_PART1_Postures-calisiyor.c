#include<string.h>
#include "HW05_111044029_PART1_Postures.h"





/*
* Change the Character to fornt state posture
* Pre:  character[][WIDTH] should be initialized
*       global parameters are defined
* Post: character[][WIDTH] is modified
*/
void frontPosture(char character[][WIDTH])
{
    character[3][3]=TEXTURE;
    character[4][2]=TEXTURE;
    character[4][4]=TEXTURE;
    character[5][3]=TEXTURE;
    character[6][2]=TEXTURE;
    character[6][3]=TEXTURE;
    character[6][4]=TEXTURE;
    character[7][1]=TEXTURE;
    character[7][3]=TEXTURE;
    character[7][5]=TEXTURE;
    character[8][3]=TEXTURE;
    character[9][2]=TEXTURE;
    character[9][4]=TEXTURE;
    character[10][2]=TEXTURE;
    character[10][4]=TEXTURE;
    character[11][1]=TEXTURE;
    character[11][2]=TEXTURE;
    character[11][4]=TEXTURE;
    character[11][5]=TEXTURE;
}


/*
* Function Objective
* Pre: (if exist, write the requirements)
* Post: (if exist, write the effects)
*/
void leftPosture(char character[][WIDTH])
{
    character[3][3]=TEXTURE;
    character[4][2]=TEXTURE;
    character[4][4]=TEXTURE;
    character[5][3]=TEXTURE;
    character[6][3]=TEXTURE;
    character[7][2]=TEXTURE;
    character[7][3]=TEXTURE;
    character[8][3]=TEXTURE;
    character[9][3]=TEXTURE;
    character[10][3]=TEXTURE;
    character[11][2]=TEXTURE;
    character[11][3]=TEXTURE;
}


/*
* Function Objective
* Pre: (if exist, write the requirements)
* Post: (if exist, write the effects)
*/
void rightPosture(char character[][WIDTH])
{
    character[3][3]=TEXTURE;
    character[4][2]=TEXTURE;
    character[4][4]=TEXTURE;
    character[5][3]=TEXTURE;
    character[6][3]=TEXTURE;
    character[7][3]=TEXTURE;
    character[7][4]=TEXTURE;
    character[8][3]=TEXTURE;
    character[9][3]=TEXTURE;
    character[10][3]=TEXTURE;
    character[11][3]=TEXTURE;
    character[11][4]=TEXTURE;
}


/*
* Function Objective
* Pre: (if exist, write the requirements)
* Post: (if exist, write the effects)
*/
void toRightFootsDown(char character[][WIDTH])
{
    character[4][3]=TEXTURE;
    character[5][2]=TEXTURE;
    character[5][4]=TEXTURE;
    character[6][3]=TEXTURE;
    character[7][2]=TEXTURE;
    character[7][3]=TEXTURE;
    character[7][4]=TEXTURE;
    character[7][5]=TEXTURE;
    character[8][1]=TEXTURE;
    character[8][3]=TEXTURE;
    character[9][3]=TEXTURE;
    character[9][4]=TEXTURE;
    character[10][2]=TEXTURE;
    character[10][5]=TEXTURE;
    character[11][1]=TEXTURE;
    character[11][5]=TEXTURE;
}


/*
* Function Objective
* Pre: (if exist, write the requirements)
* Post: (if exist, write the effects)
*/
void toRightBeckFootUp(char character[][WIDTH])
{
    character[3][3]=TEXTURE;
    character[4][2]=TEXTURE;
    character[4][4]=TEXTURE;
    character[5][3]=TEXTURE;
    character[6][2]=TEXTURE;
    character[6][3]=TEXTURE;
    character[6][4]=TEXTURE;
    character[7][2]=TEXTURE;
    character[7][3]=TEXTURE;
    character[7][5]=TEXTURE;
    character[8][3]=TEXTURE;
    character[9][2]=TEXTURE;
    character[9][3]=TEXTURE;
    character[10][1]=TEXTURE;
    character[10][3]=TEXTURE;
    character[11][3]=TEXTURE;
}


/*
* Function Objective
* Pre: (if exist, write the requirements)
* Post: (if exist, write the effects)
*/
void toRightCenteral(char character[][WIDTH])
{
    character[3][3]=TEXTURE;
    character[4][2]=TEXTURE;
    character[4][4]=TEXTURE;
    character[5][3]=TEXTURE;
    character[6][2]=TEXTURE;
    character[6][3]=TEXTURE;
    character[6][4]=TEXTURE;
    character[7][2]=TEXTURE;
    character[7][3]=TEXTURE;
    character[7][4]=TEXTURE;
    character[8][3]=TEXTURE;
    character[9][3]=TEXTURE;
    character[9][4]=TEXTURE;
    character[10][2]=TEXTURE;
    character[10][3]=TEXTURE;
    character[11][3]=TEXTURE;
}


/*
* Function Objective
* Pre: (if exist, write the requirements)
* Post: (if exist, write the effects)
*/
void toRightFrontFootUp(char character[][WIDTH])
{
    character[3][3]=TEXTURE;
    character[4][2]=TEXTURE;
    character[4][4]=TEXTURE;
    character[5][3]=TEXTURE;
    character[6][2]=TEXTURE;
    character[6][3]=TEXTURE;
    character[6][4]=TEXTURE;
    character[7][1]=TEXTURE;
    character[7][3]=TEXTURE;
    character[7][4]=TEXTURE;
    character[8][3]=TEXTURE;
    character[8][4]=TEXTURE;
    character[9][2]=TEXTURE;
    character[9][5]=TEXTURE;
    character[10][2]=TEXTURE;
    character[10][5]=TEXTURE;
    character[11][2]=TEXTURE;
}


/*
* Function Objective
* Pre: (if exist, write the requirements)
* Post: (if exist, write the effects)
*/
void toLeftFootsDown(char character[][WIDTH])
{
    character[4][3]=TEXTURE;
    character[5][2]=TEXTURE;
    character[5][4]=TEXTURE;
    character[6][3]=TEXTURE;
    character[7][1]=TEXTURE;
    character[7][2]=TEXTURE;
    character[7][3]=TEXTURE;
    character[7][4]=TEXTURE;
    character[8][3]=TEXTURE;
    character[8][5]=TEXTURE;
    character[9][2]=TEXTURE;
    character[9][3]=TEXTURE;
    character[10][1]=TEXTURE;
    character[10][4]=TEXTURE;
    character[11][1]=TEXTURE;
    character[11][5]=TEXTURE;
}


/*
* Function Objective
* Pre: (if exist, write the requirements)
* Post: (if exist, write the effects)
*/
void toLeftBeckFootUp(char character[][WIDTH])
{
    character[3][3]=TEXTURE;
    character[4][2]=TEXTURE;
    character[4][4]=TEXTURE;
    character[5][3]=TEXTURE;
    character[6][2]=TEXTURE;
    character[6][3]=TEXTURE;
    character[6][4]=TEXTURE;
    character[7][1]=TEXTURE;
    character[7][3]=TEXTURE;
    character[7][4]=TEXTURE;
    character[8][3]=TEXTURE;
    character[9][3]=TEXTURE;
    character[9][4]=TEXTURE;
    character[10][3]=TEXTURE;
    character[10][5]=TEXTURE;
    character[11][3]=TEXTURE;
}


/*
* Function Objective
* Pre: (if exist, write the requirements)
* Post: (if exist, write the effects)
*/
void toLeftCenteral(char character[][WIDTH])
{
    character[3][3]=TEXTURE;
    character[4][2]=TEXTURE;
    character[4][4]=TEXTURE;
    character[5][3]=TEXTURE;
    character[6][2]=TEXTURE;
    character[6][3]=TEXTURE;
    character[6][4]=TEXTURE;
    character[7][2]=TEXTURE;
    character[7][3]=TEXTURE;
    character[7][4]=TEXTURE;
    character[8][3]=TEXTURE;
    character[9][2]=TEXTURE;
    character[9][3]=TEXTURE;
    character[10][3]=TEXTURE;
    character[10][4]=TEXTURE;
    character[11][3]=TEXTURE;
}


/*
* Function Objective
* Pre: (if exist, write the requirements)
* Post: (if exist, write the effects)
*/
void toLeftRightFootUp(char character[][WIDTH])
{
    character[3][3]=TEXTURE;
    character[4][2]=TEXTURE;
    character[4][4]=TEXTURE;
    character[5][3]=TEXTURE;
    character[6][2]=TEXTURE;
    character[6][3]=TEXTURE;
    character[6][4]=TEXTURE;
    character[7][2]=TEXTURE;
    character[7][3]=TEXTURE;
    character[7][5]=TEXTURE;
    character[8][2]=TEXTURE;
    character[8][3]=TEXTURE;
    character[9][1]=TEXTURE;
    character[9][4]=TEXTURE;
    character[10][1]=TEXTURE;
    character[10][4]=TEXTURE;
    character[11][4]=TEXTURE;
}


void jump1(char character[][WIDTH])
{
    character[4][3]=TEXTURE;
    character[5][2]=TEXTURE;
    character[5][4]=TEXTURE;
    character[6][3]=TEXTURE;
    character[7][1]=TEXTURE;
    character[7][2]=TEXTURE;
    character[7][3]=TEXTURE;
    character[7][4]=TEXTURE;
    character[7][5]=TEXTURE;
    character[8][3]=TEXTURE;
    character[9][2]=TEXTURE;
    character[9][3]=TEXTURE;
    character[9][4]=TEXTURE;
    character[10][2]=TEXTURE;
    character[10][4]=TEXTURE;
    character[11][1]=TEXTURE;
    character[11][2]=TEXTURE;
    character[11][4]=TEXTURE;
    character[11][5]=TEXTURE;
}

void jump2(char character[][WIDTH])
{
    character[2][3]=TEXTURE;
    character[3][2]=TEXTURE;
    character[3][4]=TEXTURE;
    character[4][3]=TEXTURE;
    character[5][2]=TEXTURE;
    character[5][3]=TEXTURE;
    character[5][4]=TEXTURE;
    character[6][1]=TEXTURE;
    character[6][3]=TEXTURE;
    character[6][5]=TEXTURE;
    character[7][3]=TEXTURE;
    character[8][2]=TEXTURE;
    character[8][4]=TEXTURE;
    character[9][2]=TEXTURE;
    character[9][4]=TEXTURE;
    character[10][2]=TEXTURE;
    character[10][4]=TEXTURE;
    character[11][1]=TEXTURE;
    character[11][5]=TEXTURE;
}

void jump3(char character[][WIDTH])
{
    character[0][3]=TEXTURE;
    character[1][2]=TEXTURE;
    character[1][4]=TEXTURE;
    character[2][3]=TEXTURE;
    character[3][2]=TEXTURE;
    character[3][3]=TEXTURE;
    character[3][4]=TEXTURE;
    character[4][2]=TEXTURE;
    character[4][3]=TEXTURE;
    character[4][4]=TEXTURE;
    character[5][3]=TEXTURE;
    character[6][2]=TEXTURE;
    character[6][4]=TEXTURE;
    character[7][2]=TEXTURE;
    character[7][4]=TEXTURE;
    character[8][2]=TEXTURE;
    character[8][4]=TEXTURE;
    character[9][2]=TEXTURE;
    character[9][4]=TEXTURE;
}

void zeybekBody(char character[][WIDTH])
{
    /* body (common part)*/
    character[6][3] =TEXTURE;
    character[7][3] =TEXTURE;
    character[8][3] =TEXTURE;
    character[9][3] =TEXTURE;

}

/*head */
void zeybekHeadHigher(char character[][WIDTH])
{
    character[4][2]  =TEXTURE;
    character[4][4]  =TEXTURE;
    character[5][3]  =TEXTURE;
    character[3][3]  =TEXTURE;
}

void zeybekHeadLower(char character[][WIDTH])
{
    character[5][2]  =TEXTURE;
    character[5][4]  =TEXTURE;
    character[6][3]  =TEXTURE;
    character[4][3]  =TEXTURE;
}

/* Left Arm */
void zeybekLeftArm1 (char character[][WIDTH])
{
    /* o
     * o
     */
    character[7][2] = TEXTURE;
    character[8][2] = TEXTURE;
}

void zeybekLeftArm2 (char character[][WIDTH])
{
    /*  o
     * o
     */
    character[7][2] = TEXTURE;
    character[8][1] = TEXTURE;
}

void zeybekLeftArm3 (char character[][WIDTH])
{
    /*  oo
     * 
     */
    character[7][2] = TEXTURE;
    character[7][1] = TEXTURE;
}

void zeybekLeftArm4 (char character[][WIDTH])
{
    /*  o
     *   o
     */
    character[6][1] = TEXTURE;
    character[7][2] = TEXTURE;
}

/* Right Arm */
void zeybekRightArm1 (char character[][WIDTH])
{
    /* o
     * o
     */
    character[7][4] = TEXTURE;
    character[8][4] = TEXTURE;
}

void zeybekRightArm2 (char character[][WIDTH])
{
    /* o
     *  o
     */
    character[7][4] = TEXTURE;
    character[8][5] = TEXTURE;
}

void zeybekRightArm3 (char character[][WIDTH])
{
    /*  oo
     * 
     */
    character[7][4] = TEXTURE;
    character[7][5] = TEXTURE;
}

void zeybekRightArm4 (char character[][WIDTH])
{
    /*  o
     *   o
     */
    character[6][4] = TEXTURE;
    character[7][5] = TEXTURE;
}

/* foot */
/* used in zeybekBody */
void zeybekFootBase(char character[][WIDTH])
{
    /*    o   
     *    o 
     *    o        
     */

     character[9][3] = TEXTURE;
     character[10][3] = TEXTURE;
     character[11][3] = TEXTURE;
}

void zeybekFoot1(char character[][WIDTH])
{
    /*    o o   
     *    o  o
     *    o        
     */

    zeybekFootBase(character);
    character[10][4] = TEXTURE;
    character[11][5] = TEXTURE;
}

void zeybekFoot2(char character[][WIDTH])
{
    /*    o o    
     *   o  o  
     *      o        
     */
    zeybekFootBase(character);
    character[10][2] = TEXTURE;
    character[11][1] = TEXTURE;
}

void zeybekFoot3(char character[][WIDTH])
{
    /*   o o 
        o   o
        o   o
            o        
     */

     character[9][2] = TEXTURE;
     character[9][3] = TEXTURE;
     character[10][1] = TEXTURE;
     character[10][4] = TEXTURE;
     character[11][4] = TEXTURE;
}

void zeybekFoot4(char character[][WIDTH])
{
    /*   o o 
        o   o
        o   o
        o        
     */

     character[8][2]  = TEXTURE;
     character[8][3]  = TEXTURE;
     character[9][1]  = TEXTURE;
     character[9][4]  = TEXTURE;
     character[10][1] = TEXTURE;
     character[10][1] = TEXTURE;
     character[11][4] = TEXTURE;
}

void zeybekFoot5(char character[][WIDTH])
{
    /*  o o  
       o   o 
       o    o
     */

     character[9][2] = TEXTURE;
     character[9][3] = TEXTURE;
     character[10][1] = TEXTURE;
     character[10][4] = TEXTURE;
     character[11][1] = TEXTURE;
     character[11][5] = TEXTURE;
}

void zeybekFoot6(char character[][WIDTH])
{
    /*   o o  
        o   o 
       o    o
     */

     character[9][3] = TEXTURE;
     character[9][4] = TEXTURE;
     character[10][2] = TEXTURE;
     character[10][5] = TEXTURE;
     character[11][1] = TEXTURE;
     character[11][5] = TEXTURE;
}

void zeybek1 (char character[][WIDTH])
{
    zeybekHeadHigher(character);
    zeybekBody(character);
    zeybekLeftArm2(character);
    zeybekRightArm1(character);
    zeybekFoot1(character);
}

void zeybek2 (char character[][WIDTH])
{
    zeybekHeadHigher(character);
    zeybekBody(character);
    zeybekFootBase(character);
}

void zeybek3 (char character[][WIDTH])
{
    zeybekHeadHigher(character);
    zeybekBody(character);
    zeybekLeftArm1(character);
    zeybekRightArm2(character);
    zeybekFoot2(character);
}

void zeybek4 (char character[][WIDTH])
{
    zeybekHeadHigher(character);
    zeybekBody(character);
    zeybekLeftArm3(character);
    zeybekRightArm1(character);
    zeybekFoot1(character);
}

void zeybek5 (char character[][WIDTH])
{
    zeybekHeadHigher(character);
    zeybekBody(character);
    zeybekLeftArm1(character);
    zeybekRightArm3(character);
    zeybekFoot2(character);
}

void zeybek6 (char character[][WIDTH])
{
    zeybekHeadHigher(character);
    zeybekBody(character);
    zeybekLeftArm1(character);
    zeybekRightArm4(character);
    zeybekFoot3(character);
}

void zeybek7 (char character[][WIDTH])
{
    zeybekHeadLower(character);
    zeybekBody(character);
    zeybekLeftArm3(character);
    zeybekRightArm4(character);
    zeybekFoot5(character);
}

void zeybek8 (char character[][WIDTH])
{
    zeybekHeadLower(character);
    zeybekBody(character);
    zeybekLeftArm4(character);
    zeybekRightArm3(character);
    zeybekFoot6(character);
}

void zeybek9 (char character[][WIDTH])
{
    zeybekHeadHigher(character);
    zeybekBody(character);
    zeybekLeftArm1(character);
    zeybekRightArm4(character);
    zeybekFoot3(character);
}

void zeybek10 (char character[][WIDTH])
{
    zeybekHeadHigher(character);
    zeybekBody(character);
    zeybekLeftArm1(character);
    zeybekRightArm1(character);
    zeybekFoot1(character);
}

/* easter egg:) */
void subliminalMessage(char character[][WIDTH])
{
    char str[HEIGHT*WIDTH];
    int i,j;

    strcpy(str, "SUBLIMINAL  MESSAGE     ALI PLAYS   ZEYBEK WELL");    
    
    for(i=0; i<WIDTH; ++i)
        for(j=0; j<HEIGHT && (i*HEIGHT+j) < strlen(str); ++j)
            character[j][2*i] = str[i*HEIGHT+j];
            
}
