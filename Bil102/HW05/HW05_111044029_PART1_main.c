/* HW05_111044029_PART1_main.c */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "HW05_111044029_PART1_Postures.c"


#define WAITING_TIME 0.02   /* Wait time between postures */
#define START_POSITION 30   /* Initial Position of the character */


/* Directions that the character can turn or move */
typedef enum
{
    LEFT=-1,FRONT,RIGHT
} courses;

/* Possible error values */
typedef enum
{
    incorrectInput
} errors;


/* Display and initialization functions */
char menu();
void initialize(int ,int* , char [][WIDTH]);

/* Turning functions */
void turnLeft(int, int* , char [][WIDTH]);
void turnRight(int, int* , char [][WIDTH]);
void turnFront(int, int* , char [][WIDTH]);

/* Movement functions */
void playZeybek(courses , int* , char [][WIDTH]);
void walk(int, courses, int*, char [][WIDTH]);
void walkToRight(int , courses , int* , char [][WIDTH]);
void walkToLeft(int , courses , int* , char [][WIDTH]);
void jump(int, int, courses, char character[][WIDTH]);
void move(int , int , char [][WIDTH], void (char [][WIDTH]));

/* Accessory functions */
void deleteCinAli(char [][WIDTH]);
void printCinAli(int , char [][WIDTH]);
void setStepNumber(int *step);
void error(errors i);
void gameOver();
void flushInputBuffer();
void wait ( double );



int main()
{
    courses direction=FRONT;    /* Direction of the character */
    int     currentPosition=START_POSITION, /* Position of the character */
            step;   /* The step number that athe character moves in a direction */

    char    cinAli[HEIGHT][WIDTH], /* the Character of the Game*/
            action;    /* Type of action that user enters */

    /* Initialize Cin Ali */
    initialize(currentPosition, &direction, cinAli);

    /* Play game until user exits */
    do
    {
        /* Display menu and read the order user gave*/
        action=menu();
        /* Do the action */
        switch (action)
        {
            case '>': /* Move Right */
                setStepNumber(&step); /* Read and set the step number */
                turnRight(currentPosition, &direction, cinAli);
                walk(step, direction, &currentPosition, cinAli);
                break;
            case '<': /* Move left */
                setStepNumber(&step);
                turnLeft(currentPosition, &direction, cinAli);
                walk(step, direction, &currentPosition, cinAli);
                break;
            case '=': /* Turn Front */
                turnFront(currentPosition, &direction, cinAli);
                break;
            case 'r': /* Turn Right */
                turnRight(currentPosition, &direction, cinAli);
                break;
            case 'l': /* Turn Left */
                turnLeft(currentPosition, &direction, cinAli);
                break;
            case 'x': /* Exit */
                gameOver();
                break;
            case 'j':
                setStepNumber(&step);
                turnFront(currentPosition, &direction, cinAli);
                jump(step, currentPosition, direction, cinAli);
                break;
            case 'z':
                   playZeybek(direction, &currentPosition, cinAli);
                break;
            default:
                error(incorrectInput); /* Incorrect input from the user */
                break;
        }
        flushInputBuffer(); /* Clean the input buffer */

    }while(action!='x');

    return 0;
}


/*
 *  Displays options for the user to select an available action
 *  Pre:  database accesses a binary file of product_t records that has
 *        been opened as an input file, and params is defined
 *  Post: It returns the chosen action
 */
char menu()
{
    char select;
    printf("\n\nEnter '>' for Cin Ali to walk right\n");
    printf("Enter '<' for Cin Ali to walk left\n");
    printf("Enter '=' for Cin Ali to turn front\n");
    printf("Enter 'r' for Cin Ali to turn right\n");
    printf("Enter 'l' for Cin Ali to turn left\n");
    printf("Enter 'j' for Cin Ali to jump\n");
    printf("Enter 'z' for Cin Ali to play zeybek\n");
    scanf("%c",&select);
    return select;
}


/*
* Display cin ali on the begining of the program.
* Pre: input  currentPosition : Where ali is. Should be defined
*      input-output direction : The direction ali looks. Should be defined. 
*      input                  : cin ali character holder array
* Post: 
*      output       direction : The direction ali looks after ali's new state 
*                               printed.
*/
void initialize(int currentPosition, courses* direction, char character[][WIDTH])
{
    *direction=FRONT;
    move(currentPosition, *direction, character, frontPosture);
}


/*
* Modifies the step variable for ali's movements (used in loop)
* Pre: input -output step: integer pointer to set ali's movement loop count.
*/
void setStepNumber(int *step)
{
    printf("Step Number: ");
    scanf("%d", step);
}


/*
* Fills character array with spaces so that it becomes invisible.
* Pre: character array should be initialized.
* Post: character array will be filled with space.
*/
void deleteCinAli(char character[][WIDTH])
{
    int i, j;
    for(i=0; i<HEIGHT; ++i)
    {
        for(j=0; j<WIDTH; ++j)
        {
            character[i][j]=' ';
        }
    }
}


/*
* Prints cin ali to the terminal screen on its current position.
* 
* Pre: parameters should be defined and WIDTH and HEIGHT macros should also be
* defined.
*/
void printCinAli(int currentPosition, char character[][WIDTH])
{
    int i, j;
    for(i=0; i<HEIGHT; ++i)
    {
        for(j=0; j<currentPosition; ++j)
        {
            printf(" ");
        }
        for(j=0; j<WIDTH; ++j)
        {
            printf("%c", character[i][j]);
        }
        printf("\n");
    }
    /* ground */
    for(i=0; i < WIDTH + START_POSITION*2; ++i)
        printf("-");
    printf("\n");
}


/*
* This function handles ali's movements. Clears the terminal screen, fills
* character array with spaces then fill with new posture then prints the new
* character and waits for a short period to display each posture seperately.
* 
* Pre: currentPosition, character, posture should be defined. direction wont be
* used. posture is a pointer of a function, implemented in postures file.
* Post: character will be filled with the new posture.
*/
void move(int currentPosition, courses direction, char character[][WIDTH], void posture(char [][WIDTH]))
{
    system("clear");
    deleteCinAli(character);
    posture(character);
    printCinAli(currentPosition, character);
    wait(WAITING_TIME);
}


/*
* Makes ali's direction left.
* Pre: all arguments should be defined. leftPosture() should be defined in 
* Postures file.
* Post: direction will be left.
*/
void turnLeft(int currentPosition, courses* direction, char character[][WIDTH])
{
    *direction=LEFT;
    move(currentPosition, *direction, character, leftPosture);
}


/*
* Makes ali's direction right.
* Pre: all arguments should be defined. rightPosture() should be defined in 
* Postures file.
* Post: direction will be right.
*/
void turnRight(int currentPosition, courses* direction, char character[][WIDTH])
{
    *direction=RIGHT;
    move(currentPosition, *direction, character, rightPosture);
}


/*
* Makes ali's direction front.
* Pre: all arguments should be defined. frontPosture() should be defined in 
* Postures file.
* Post: direction will be front.
*/
void turnFront(int currentPosition, courses* direction, char character[][WIDTH])
{
    *direction=FRONT;
    move(currentPosition, *direction, character, frontPosture);
}


/*
* This function lets ali walk either left or right. Step number is given by
* user.
* Pre: all arguments should be initialized and direction should be courses type.
* Post: currentPosition will be changed according to direction ali walk.
*/
void walk(int step, courses direction, int *currentPosition, char character[][WIDTH])
{
    switch (direction)
    {
        case LEFT:
            walkToLeft(step, direction, currentPosition, character);
            break;
        case RIGHT:
            walkToRight(step, direction, currentPosition, character);
            break;
        default:
            break;
    }
}


/*
* Let ali play zeybek with different wait period.
* Pre: zeybek* postures should be defined in Postures.c. direction and
* currentPosition should also be initialized.
* Post: currentPosition will be changed
*/
void playZeybek(courses direction, int* currentPosition, char character[][WIDTH])
{
    int i;
    /* changing the terminal colors to make it more authentic :) */
    system(BROWNBG WHITEFG BOLDTEXT);
    
/* printf("%d", *currentPosition); 
 move((*currentPosition)++, direction, character, zeybek7);
        wait(2.7);*/
    /* moving ali character to the left side before he begins playing zeybek */
    if(*currentPosition > 0)
    {
        turnLeft(*currentPosition, &direction, character);
        walk(*currentPosition/4, direction, currentPosition, character);
    }

    /* prelude */
    for(i=0; i<3; ++i)
    {
        move((*currentPosition)++, direction, character, zeybek1);
        wait(0.3);
        move((*currentPosition)++, direction, character, zeybek2);
        wait(0.5);
        move((*currentPosition)++, direction, character, zeybek3);
        wait(0.3);
        move((*currentPosition)++, direction, character, zeybek4);
        wait(0.4);
        move((*currentPosition)++, direction, character, zeybek5);
        wait(0.3);
    }
    
    /* artistic movements */
    for(i=0; i<2; ++i)
    {
        move((*currentPosition)--, direction, character, zeybek4);
        wait(0.8);
        move((*currentPosition)++, direction, character, zeybek5);
        wait(0.8);
        move((*currentPosition)++, direction, character, zeybek6);
        wait(0.4);
    }
    
    /* artistic movements in reverse direction */
    for(i=0; i<2; ++i)
    {
        move((*currentPosition)--, direction, character, zeybek7);
        wait(0.6);
        move((*currentPosition)--, direction, character, zeybek8);
        wait(0.3);
        move((*currentPosition)--, direction, character, zeybek9);
        wait(0.4);
        move((*currentPosition)--, direction, character, zeybek10);
        wait(0.5);
    }
    
    /* artistic movements in reverse direction */
    for(i=0; i<3; ++i)
    {
        move((*currentPosition)++, direction, character, zeybek10);
        wait(0.2);
        move((*currentPosition)++, direction, character, zeybek9);
        wait(0.4);
        move((*currentPosition)++, direction, character, zeybek8);
        wait(0.7);
        move((*currentPosition)++, direction, character, zeybek7);
        wait(0.3);
    }
    
    /* finishing */
    for(i=0; i<4; ++i)
    {
        move((*currentPosition)--, direction, character, zeybek1);
        wait(0.5);
        move((*currentPosition)--, direction, character, zeybek2);
        wait(0.8);
        move((*currentPosition)--, direction, character, zeybek3);
        wait(0.4);
    }
    
    /* resetting terminal colors */
    system(RESETCOLOR);
    
    /* displaying front posture */
    move((*currentPosition), direction, character, frontPosture);
    
}

/*
* Let ali walk left side for user given steps.
* Pre: step > 0. direction should be initialized as a courses enum type, 
*      character, and currentposition should also be initialized.
* Post: current position will be increased,
*       ali's direction will be right
*/
void walkToRight(int step, courses direction, int* currentPosition, char character[][WIDTH])
{
    int i;
    for(i=0; i<step; ++i)
    {
        move((*currentPosition)++, direction, character, toRightFootsDown);
        move((*currentPosition)++, direction, character, toRightBeckFootUp);
        move((*currentPosition)++, direction, character, toRightCenteral);
        move((*currentPosition)++, direction, character, toRightFrontFootUp);
    }
    move((*currentPosition), direction, character, rightPosture);
}

/*
* Let ali jump.
* Pre: step > 0. direction should be initialized as a courses enum type, 
*      character, and currentposition should also be initialized.
* Post: current position will be the same,
*       ali's posture will be front
*/
void jump(int step, int currentPosition, courses direction, char character[][WIDTH])
{
    int i;
    for(i=0; i<step; ++i)
    {
        move((currentPosition), direction, character, frontPosture);
        move((currentPosition), direction, character, jump1);
        move((currentPosition), direction, character, jump2);
        move((currentPosition), direction, character, jump3);
        move((currentPosition), direction, character, jump2);
        move((currentPosition), direction, character, frontPosture);
    }
}

/*
* Let ali walk left side for user given steps.
* Pre: step > 0. direction should be initialized as a courses enum type, 
*      character, and currentposition should also be initialized.
* Post: current position will be decreased,
*       ali's direction will be left
*/
void walkToLeft(int step, courses direction, int* currentPosition, char character[][WIDTH])
{
    int i;
    for(i=0; i<step; ++i)
    {
        move((*currentPosition)--, direction, character, toLeftFootsDown);
        move((*currentPosition)--, direction, character, toLeftBeckFootUp);
        move((*currentPosition)--, direction, character, toLeftCenteral);
        move((*currentPosition)--, direction, character, toLeftRightFootUp);
    }
    move((*currentPosition), direction, character, leftPosture);
}


/*
* Error handler for wrong selections on menu.
* Pre: unmerated type errors should be defined and i has a corresponding value
*      in errors enum.
*/
void error(errors i)
{
    switch(i)
    {
        case incorrectInput:
            printf("Please, enter the right input\n");
            break;
        default:
            break;
    }
}


/*
* Terminating message.
*/
void gameOver()
{
    printf("Game Over!");
    wait(1);
}


/*
* Pauses the program for user given seconds.
* Pre: input: seconds should be initialized.
*/
void wait ( double seconds )
{
    clock_t endwait;
    endwait = clock () + seconds * CLOCKS_PER_SEC ;
    while (clock() < endwait) {}
}



/*
* Clean the input buffer till encounter with return character 
* Pre: call of scanf() function
* Post: ignores the following characters till return
*/
void flushInputBuffer()
{
    char junk;
    /* A loop that cleans the input buffer till '\n' character being entered*/
    do
    {
        scanf("%c",&junk);
    }while(junk!='\n');
}
