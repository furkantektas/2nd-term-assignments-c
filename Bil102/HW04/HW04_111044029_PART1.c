/*############################################################################*/
/* HW04_111044029_PART1.c                                                     */
/* ----------------------                                                     */
/* Created on 14.04.2012 by Furkan Tektas                                     */
/* ­­­­­                                                                           */
/* Description                                                                */
/* ----------------------                                                     */
/* This program calculates the area between points. The coordinates of the    */
/* points are provided by input file and seperated by space. Each line        */
/* contains two coordinates(one point).  Array holds the coordinates and      */
/* area is saved to the output file.                                          */
/*                                                                            */
/*############################################################################*/


/*############################################################################*/
/*                                  Includes                                  */
/*############################################################################*/
#include <stdio.h>
#include <math.h>

/*############################################################################*/
/*                                   Macros                                   */
/*############################################################################*/
#define DEBUG 0
#define INPFILE   "p1_sample_input.txt"
#define OUTPFILE  "p1_sample_output.txt"
#define MAXNUMPOINTS 20 /* maximum number of points */

/*­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­----------------------------------------------------------------------------*/
/*                            Function Prototypes                             */
/*­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­----------------------------------------------------------------------------*/

/* Read coordinates from a given input file
 * inputFile - Contains coordinates of the points, seperated by space 
 * coordinates[][2] - Coordinate holder array. 
 * arraySize - Num of maximum points(2x of coordinates) */
int GetCorners(FILE * inputFile, double coordinates[][2], int arraySize);

/* Calculates the area between given points.
 * coordinates[][2] - Point holder array. Should not be changed.
 * arraySize - Num of maximum points
 * */
double PolygonArea(const double coordinates[][2], int arraySize);

/* Writes coordinates and the area between them to the a given output file 
 * coordinates[][2] - Coordinate holder array. 
 * arraySize - Num of maximum points */
void OutputCorners(FILE * outputFile, const double coordinates[][2], 
                   int arraySize);

/* Prints output file to the screen
 * outputFile - Output file which contains the array contents and the area */
void
DumpOutputFile(FILE * outputFile);

int
main(void)
{
    FILE *inp,
         *outp;

    double coordinates[MAXNUMPOINTS][2]; /* degree of polynomial */
           
    int arraySize = MAXNUMPOINTS; /* initial array size is the MAXNUMPOINTS */

    inp = fopen(INPFILE,"r");
    outp = fopen(OUTPFILE,"w");
    
    /* Notice that the actual array size is the half of the number of the 
     * coordinates stored in the array. Other coordinates are 0. */
    arraySize = GetCorners(inp, coordinates, arraySize) / 2;
    
    if(DEBUG)
        printf("Array size = %d\n", arraySize);
        
    OutputCorners(outp, (const double (*)[2])&coordinates, arraySize);

    /* Prompting user */
    printf("Everything's gone normal, results are saved to the ");
    printf(OUTPFILE);
    printf(".\n\n");
    
    fclose(inp);
    fclose(outp);

    /* Dumping the output file */
    outp = fopen(OUTPFILE,"r");
    DumpOutputFile(outp);
    fclose(outp);
    
    return 1;
}

/*----------------------------------------------------------------------------­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­*/
/*                            Function Implementations                        */
/*----------------------------------------------------------------------------­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­*/

int
GetCorners(FILE * inputFile, double coordinates[][2], int arraySize)
{
    int i = 0, /* counter */
        NumOfCoordinates = 0, /* Number of coordinates stored in the array */
        status = 0; /* EOF checker */

    for(i=0; i<arraySize;++i) /* set coordinate values to zero */
    {
        coordinates[i][0] = 0;
        coordinates[i][1] = 0;
    }

    i = 0;

    do
    {
        status =  fscanf(inputFile, "%lf%lf", &coordinates[i][0], 
                        &coordinates[i][1]);
        if(DEBUG)
            printf("*****Status: %d Point %d: (%.2f,%.2f)*****\n", status, i,
                   coordinates[i][0], coordinates[i][1]);

        /* if everything's ok,status should be 2,then increase NumOfCoordinates*/
        if(status == 2)
            NumOfCoordinates += 2;

        ++i;
    
    /* repeat the steps above until reach to the EOF or maximum arraySize */
    }while((status != EOF) && (i < arraySize));

    if(DEBUG)
    {
        printf("*****All Points*****\n");
        for(i=0; i < arraySize; ++i)
            printf("\t%.2f %.2f\n", coordinates[i][0], coordinates[i][1]);
        printf("\nPoint Number = %d\n\n", NumOfCoordinates);
    }/* IF(DEBUG) */

    return NumOfCoordinates;
}

double
PolygonArea(const double coordinates[][2], int arraySize)
{
    int i;
    double area = 0;
    
    for(i=0; i < arraySize - 2; ++i)
        area += (coordinates[i][0] + coordinates[i+1][0]) *
                (coordinates[i][1] - coordinates[i+1][1]);
    
    if(DEBUG)
        printf("\n*****Area=%.2f*****\n", 0.5 * fabs(area));

    return 0.5 * fabs(area);
}

void
OutputCorners(FILE * outputFile, const double coordinates[][2], int arraySize)
{
    int i;
    double totalArea; /* area between points */
    
    /* coordinates array is constant, no need to type cast */
    totalArea = PolygonArea(coordinates, arraySize);

    /* Header for output file */
    fprintf(outputFile, "Array Contents\n");
    fprintf(outputFile, "--------------\n");

    for(i=0; i < arraySize; ++i)
        fprintf(outputFile, "[%d][0] = %.2f   [%d][1] = %.2f\n",
                i, coordinates[i][0], i, coordinates[i][1]);
    
    fprintf(outputFile, "\nTotal Area = %.2f\n", totalArea);
}
void
DumpOutputFile(FILE * outputFile)
{
    char ch;
    while(fscanf(outputFile, "%c", &ch) != EOF)
        printf("%c",ch);
    
}
/*############################################################################*/
/*                         End of HW04_111044029_PART1.c                      */
/*############################################################################*/
