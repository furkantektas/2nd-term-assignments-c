/*############################################################################*/
/* HW04_111044029_PART2.c                                                     */
/* ----------------------                                                     */
/* Created on 14.04.2012 by Furkan Tektas                                     */
/* ­­­­­                                                                           */
/* Description                                                                */
/* ----------------------                                                     */
/* This program gets two polynomial from INPFILE1 and INPFILE2 files and can  */
/* multiply or sum them. Users can evaluate the value of them.                */
/*                                                                            */
/*############################################################################*/


/*############################################################################*/
/*                                  Includes                                  */
/*############################################################################*/
#include <stdio.h>
#include <math.h>

/*############################################################################*/
/*                                   Macros                                   */
/*############################################################################*/
#define DEBUG 0
#define INPFILE1  "p2_sample_input1.txt"
#define INPFILE2  "p2_sample_input2.txt"
#define MAXDEGREE 8

/* Displays the menu and gets an appropriate operation from user */
void DisplayMenu(char *operation);

/* Read coefficients from a given input file */
void GetPoly(FILE * inputFile, double coeff[], int * degreep);

/* Evaluate a polynomial at a given value of x */
double Evaluate(const double coeff[], int degree, double x);

/* result = poly1 + poly2 */
double AddPoly(const double poly1[], const double poly2[], double result[],
int degreePoly1, int degreePoly2, int *degreeResult);

/* result = poly1 * poly2 */
double MultiplyPoly(const double poly1[], const double poly2[], double result[], int
degreePoly1, int degreePoly2, int *degreeResult);

/* Prints polynomial in proper format */
void PrintPoly(const double poly[], int degree);

/* Set all coeffients to zero */
double SetToZero(double coeff[], int degree);

/* Reverses the given array */
void ArrayReverse(double array[], int degree);

/* Prints plus sign if number is positive */
void PrintSign(double number);


int
main(void)
{
    FILE *inp1, *inp2;
    double coeff1[MAXDEGREE + 1],     /* coefficients of polynomial 1 */
           coeff2[MAXDEGREE + 1],     /* coefficients of polynomial 2 */
           result[2 * MAXDEGREE + 1], /* coefficients of result polynomial */
           inputX;                    /* x to evaluate the polynomial */
    int degree1, degree2, degreeResult; /* degrees of the polynomials */
    char operation = 'f';             /* user option to multiply or sum */
    
    /* Opening files */
    inp1 = fopen(INPFILE1,"r");
    inp2 = fopen(INPFILE2,"r");
    
    /* Defining Polynomials */
    GetPoly(inp1, coeff1, &degree1);
    GetPoly(inp2, coeff2, &degree2);
    
    /* Displaying Polynomials */
    printf("Poly 1:\t");
    PrintPoly(coeff1, degree1);
    
    printf("Poly 2:\t");
    PrintPoly(coeff2, degree2);
    
    /* Displaying Menu */
    DisplayMenu(&operation);
    
    /* Getting x value */
    printf("Enter the x:");
    scanf("%lf", &inputX);
    
    /* Processing user input */
    if(operation == 's' || operation == 'S') /* add poly */
        AddPoly(coeff1, coeff2, result, degree1, degree2, &degreeResult);
    else /* multiply poly */
        MultiplyPoly(coeff1, coeff2, result, degree1, degree2, &degreeResult);
    
    /* Printing result array */
    printf("Result:\t");
    PrintPoly(result, degreeResult);
    
    /* Printing the evaluated value. */
    printf("Result is : %.2f\n", Evaluate(result, degreeResult, inputX));
    
    return 1;
}

void
DisplayMenu(char *operation)
{
    char garbage; /* garbage char for \n or chars bigger than 1 byte  */
    /* get operation until user enters s or m */
    while ( (*operation != 'm') && (*operation != 'M') &&
            (*operation != 's') && (*operation != 'S') )
    {
        printf("Choose an operation:\n");
        printf("\t[s] Sum:\n");
        printf("\t[m] Multiply:\n");
        scanf(" %c%c", operation, &garbage);
    }
}

void
GetPoly(FILE *inputFile, double coeff[], int * degreep)
{
    int i;
    *degreep = 0;

    if(DEBUG)
        printf("GetPoly function is initialized.\n");

    /* Setting all coefficients to zero */
    SetToZero(coeff, *degreep);

    for(i=0; i < MAXDEGREE; ++i)/* polynomial can be at most degree 8 */
    {
        if(fscanf(inputFile, "%lf", &coeff[i]) == 1)
            ++*degreep;
    }
    ArrayReverse(coeff, *degreep);
    
    if(DEBUG)
    {
        printf("degreep = %d\n", *degreep);
        printf("*****Coeffs*****\n");
        for(i=0; i < *degreep; ++i)
            printf("\t%.2f", coeff[i]);
        printf("\n");
        
        PrintPoly(coeff, *degreep);
    }/* IF(DEBUG) */
    
    
}

double
Evaluate(const double coeff[], int degree, double x)
{
    int i;
    double result = 0;
    
    for(i=0; i<degree; ++i)
        result += coeff[i] * pow(x,i);
    
    if(DEBUG)
        printf("*****Result = %.2f*****\n", result);
        
    return result;
}

double
AddPoly(const double poly1[], const double poly2[], double result[],
int degreePoly1, int degreePoly2, int *degreeResult)
{
    int i;

    if(degreePoly1 > degreePoly2)
        *degreeResult = degreePoly1;
    else
        *degreeResult = degreePoly2;

    for(i = *degreeResult; i >= 0; --i)
        result[i] = poly1[i] + poly2[i];

    return 1;
}

double
MultiplyPoly(const double poly1[], const double poly2[], double result[],
int degreePoly1, int degreePoly2, int *degreeResult)
{
    int i, j;

    *degreeResult = degreePoly1 + degreePoly2 -1;

    SetToZero(result, *degreeResult);
    
    for(i=degreePoly1-1; i >= 0; --i)
    {
        for(j=degreePoly2-1; j>=0; --j)
            result[i+j] += poly1[i] * poly2[j];
    }
    if(DEBUG)
        PrintPoly(result, *degreeResult);
    return 1;
}

void
PrintPoly(const double poly[], int degree)
{
    int i;
    
    if(DEBUG)
    {
        printf("PrintPoly function is initialized.\n");
        printf("degree = %d\n", degree);
    }
    for(i=degree-1; i>=0; --i)
    {
        /* do not print anything if coeff is 0 */
        if(!( (poly[i] > -0.05) && (poly[i] < 0.05) ) ) /* not 0 */
        {
            if(i < degree-1) /* dont print the sign if its the first coeff */
                PrintSign(poly[i]); /* Print the sign if number is positive */
            
            /* do not print if coeff is either 1 or -1 */
            if(
                (!( (poly[i] > 0.95) && (poly[i] < 1.05) ) && /* not 1 */
                !( (poly[i] < -0.95) && (poly[i] > -1.05) ) /* not -1 */ )
              )
                    printf("%.2f", poly[i]);

            /* do not print the power if power is smaller than 1 */
            if(i > 1)
                printf("x^%d ", i);
            else if(i == 1)
                printf("x ");
        }/* if not 0 */
    }
    printf("\n");
}
double
SetToZero(double coeff[], int degree)
{
    int i;
    for(i = 0; i < degree; ++i)
        coeff[i] = 0;
    return 1;
}

void
ArrayReverse(double array[], int degree)
{
    double temp;
    int i;
    for(i=0; i < degree; ++i)
    {
        temp = array[i];
        array[i] = array[degree-1];
        array[degree-1] = temp;
        --degree;
    }
}

void
PrintSign(double number)
{
    if(number > 0)
        printf("+");
}
/*############################################################################*/
/*                         End of HW04_111044029_PART2.c                      */
/*############################################################################*/
