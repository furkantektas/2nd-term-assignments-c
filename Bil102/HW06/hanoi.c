#include <stdio.h>

void
tower(char from_peg,  /* input - characters naming                 */
      char to_peg,    /*         the problem's                     */
      char aux_peg,   /*         three pegs                        */
      int  n)         /* input - number of disks to move           */
{
	printf("from_peg = %c\tto_peg = %c\taux_peg = %c\tn = %d\n",
		from_peg, to_peg,aux_peg,n);
      if (n == 1) {
            /*printf("Move disk 1 from peg %c to peg %c\n", from_peg, to_peg);*/
      } else {
            printf("\ntower(%c, %c, %c, %d);\n",from_peg,aux_peg,to_peg,n - 1);
            tower(from_peg, aux_peg, to_peg, n - 1);
            /*printf("Move disk %d from peg %c to peg %c\n", n, from_peg, to_peg);*/
            printf("\ntower(%c, %c, %c, %d);\n",aux_peg,to_peg,from_peg,n - 1);
            tower(aux_peg, to_peg, from_peg, n - 1);
      }
}

int main(void) {
	tower('A', 'B', 'C', 4);
	return 0;
}
/*
 *  Displays instructions for moving n disks from from_peg to to_peg using
 *  aux_peg as an auxiliary.  Disks are numbered 1 to n (smallest to 
 *  largest). Instructions call for moving one disk at a time and never 
 *  require placing a larger disk on top of a smaller one.
 */

