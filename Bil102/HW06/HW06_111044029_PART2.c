/*############################################################################*/
/* HW06_111044029_PART2.c                                                     */
/* ----------------------                                                     */
/* Created on 09.05.2012 by Furkan Tektas                                     */
/* ­­­­­                                                                           */
/* Description                                                                */
/* ----------------------                                                     */
/* This program generates a random maze and tries to find a path from         */
/* starting point to the finishing point. Wall density of the maze can be     */
/* changed by WALLDENSITY and starting-finishing points can also be changed   */
/* by macros. For the shortest path, use down and right directions.           */
/*                                                                            */
/*############################################################################*/


/*############################################################################*/
/*                                  Includes                                  */
/*############################################################################*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

/*############################################################################*/
/*                                   Macros                                   */
/*############################################################################*/
#define DEBUG  0
#define WIDTH  8
#define HEIGHT 8
#define PATHMARKER 'O'
#define STARTINGPTX 0
#define STARTINGPTY 1
#define FINISHINGPTX 7
#define FINISHINGPTY 7
#define MAZEWALL 'X'
#define MAZEPATH ' '
#define WALLDENSITY 0.2

/* Path directions. */
typedef enum {
	top,
	left,
	bottom,
	right
	} direction_t;

/*­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­----------------------------------------------------------------------------*/
/*                            Function Prototypes                             */
/*­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­----------------------------------------------------------------------------*/

/* Finds a path recursively.
 * maze        - Maze holder matrix
 * path        - Path holder matrix (equals to the maze)
 * locX        - Location on the X axis
 * locY        - Location on the Y axis
 * direction   - Last movement's direction */
int FindPath(char maze[][HEIGHT], char path[][HEIGHT], int locX, int locY,
             direction_t direction);

/* Prints a matrix(maze in this case) */
void PrintMaze(char maze[][HEIGHT]);

/* Prints a line which longs 2*WIDTH */
void PrintLine(void);

/* Clears PATHMARKER chars from path matrix.Used in end of the each failed
 * FindPath trial to get a clear path matrix */
void ClearPath(char path[][HEIGHT]);

/* Generates a random maze to solve. Wall Density can be changed by WALLDENSITY
 * macro */
void GenerateRandomMaze(char maze[][HEIGHT], char path[][HEIGHT]);

/* Returns randomly 1 or 0 according to wall-density */
int Random(void);

int
main(void)
{
	char	maze[WIDTH][HEIGHT], 
		path[WIDTH][HEIGHT]; 
	int	locX = STARTINGPTX, 
		locY = STARTINGPTY; /* location of the maze on x-y axises */
	direction_t direction = 5;  /* no direction is set */
	
	srand(time(NULL)); /* seed the generator */
	
	/* Inıtializing the maze and path matrixes equally */
	GenerateRandomMaze(maze,path);
	
	/* Put a PATHMARKER char on starting point */
	path[locX][locY] = PATHMARKER;
	
	/* Printing the generated maze */
	printf("      MAZE      \n");
	PrintMaze(maze);

	/* Trying to find a path on the maze. */
	if(FindPath(maze, path, locX, locY, direction)) {
		/* Printing the path found. */
		printf("      PATH      \n");
		PrintMaze(path);
	}
	else {
		/* Printing the "No path found." error. */
		printf("     NO PATH     \n");
		PrintMaze(path);
		printf("No path found!\n");
	}
	
	if(DEBUG) {
		/* Print the manipulated matrix */
		printf("    NEW MAZE     \n");
		PrintMaze(maze);
	}
	
    return 1;
}

/*----------------------------------------------------------------------------­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­*/
/*                            Function Implementation                         */
/*----------------------------------------------------------------------------­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­*/

int
FindPath(char maze[][HEIGHT], char path[][HEIGHT], int locX, int locY,
         direction_t direction)
{
	if(DEBUG) {
		PrintMaze(maze);
		PrintLine();
		PrintMaze(path);

		/* Continue step by step */
		printf("Press Return to move\n");
		getchar();

		/* Print the last direction */
		printf("Direction: ");
		switch(direction) {
			case left:
				printf("left\n");
				break;
			case top:
				printf("top\n");
				break;
			case right:
				printf("right\n");
				break;
			case bottom:
				printf("bottom\n");
				break;
		}
	}
	
	/* maze contains a wall on the starting point */
	if(maze[STARTINGPTX][STARTINGPTY] == MAZEWALL)
		return 0;
	/* maze contains a wall on the finishing point */
	else if(maze[FINISHINGPTX][FINISHINGPTY] == MAZEWALL)
		return 0;
	/* not reached the finishing point yet*/
	else if(locX != FINISHINGPTX || locY != FINISHINGPTY) {

		/* trying the right movement */
		if(locY < WIDTH-1 && direction != left &&
		   maze[locX][locY+1] == MAZEPATH &&
		   path[locX][locY+1] != PATHMARKER) {

			++locY;
			path[locX][locY] = PATHMARKER;

			return FindPath(maze, path, locX, locY, right);
		}
		 /* trying the down movement */
		else if(locX < HEIGHT-1 && direction != top && 
		        maze[locX+1][locY] == MAZEPATH &&
		        path[locX+1][locY] != PATHMARKER) {

			++locX;
			path[locX][locY] = PATHMARKER;

			return FindPath(maze, path, locX, locY, bottom);
		}
		/* trying the left movement */
		else if(locY > 0 && direction != right &&
		        maze[locX][locY-1] == MAZEPATH &&
		        path[locX][locY-1] != PATHMARKER) {

			--locY;
			path[locX][locY] = PATHMARKER;

			return FindPath(maze, path, locX, locY, left);
		}
		/* trying the top movement */
		else if(locX > 0 && direction != bottom &&
		        maze[locX-1][locY] == MAZEPATH &&
		        path[locX-1][locY] != PATHMARKER) {

			--locX;
			path[locX][locY] = PATHMARKER;

			return FindPath(maze, path, locX, locY, top);
		}
		/* cannot move anywhere else. Create a wall at this location
		 * and not come here back in the future then restart finding
		 * a path from starting point */
		else
		{
			/* create a wall to not try again */
			maze[locX][locY] = MAZEWALL; 
			ClearPath(path);
			return FindPath(maze,path,STARTINGPTX,STARTINGPTY, direction);
		}
	}
	/* Reached to the finishing point */
	else
		return 1;
}

void
PrintMaze(char maze[][HEIGHT])
{
	int i,j;

	PrintLine();

	for(i=0; i<HEIGHT;++i) {
		for(j=0; j<WIDTH;++j)
			printf("%c ",maze[i][j]);
		printf("\n");
	}

	PrintLine();
}

void
PrintLine(void)
{
	int i;
	for(i=0;i<WIDTH*2;++i)
		printf("=");
	printf("\n");
}
void
GenerateRandomMaze(char maze[][HEIGHT], char path[][HEIGHT])
{
	int i,j;
	for(i=0;i<WIDTH;++i) {
		for(j=0;j<HEIGHT;++j) {
			
			if(Random()) {
				maze[i][j] = MAZEWALL;
				path[i][j] = MAZEWALL;
			}
			else {
				maze[i][j] = MAZEPATH;
				path[i][j] = MAZEPATH;
			}
		}
	}
	/* dont create a wall on the starting point */
	maze[STARTINGPTX][STARTINGPTY] = MAZEPATH;
	path[STARTINGPTX][STARTINGPTY] = MAZEPATH;
}

void
ClearPath(char path[][HEIGHT])
{
	int i,j;
	for(i=0;i<WIDTH;++i)
		for(j=0;j<HEIGHT;++j)
			if(path[i][j] == PATHMARKER)
				path[i][j] = MAZEPATH;
	
	/* mark starting point again */
	path[STARTINGPTX][STARTINGPTY] = PATHMARKER;
}

int
Random(void)
{
	return  ( rand() % 10 < WALLDENSITY*10);
}

/*############################################################################*/
/*                         End of HW06_111044029_PART2.c                      */
/*############################################################################*/
