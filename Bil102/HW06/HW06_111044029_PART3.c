/*############################################################################*/
/* HW06_111044029_PART3.                                                      */
/* ----------------------                                                     */
/* Created on 09.05.2012 by Furkan Tektas                                     */
/* ­­­­­                                                                           */
/* Description                                                                */
/* ----------------------                                                     */
/* Finds roots of the equations                                               */
/*       g(x) = 0   and      h(x) = 0                                         */
/* on a specified interval [x_left, x_right] using the bisection method.      */
/*                                                                            */
/*############################################################################*/


/*############################################################################*/
/*                                  Includes                                  */
/*############################################################################*/
#include <stdio.h> 
#include <math.h> 

#define FALSE 0
#define TRUE 1

/* 
 *  Implements the bisection method for finding a root of a function f.
 *  Finds a root if signs of fp(x_left) and fp(x_right) are different. 
 */ 
double bisect(double x_left, double x_right, double epsilon,
              double f(double farg));

/* Finds root recursively without error checking (as mentioned in Q5) 
 * 
 * Pre: Should be a root between x_left and x_right                   */
double FindRoot(double x_left, double x_right, double epsilon,
                double f(double farg));

/*  Functions for which roots are sought */ 
double g(double x);
double h(double x);


int 
main(void) 
{ 
	double x_left, x_right, /* left, right endpoints of interval  */ 
	       epsilon,      /* error tolerance  */ 
	       root; 
 
	/*  Get endpoints and error tolerance from user     */ 
	printf("\nEnter interval endpoints> "); 
	scanf("%lf%lf", &x_left, &x_right); 
	printf("\nEnter tolerance> "); 
	scanf("%lf", &epsilon); 
 
	/*  Use bisect function to look for roots of g and h */ 
	printf("\n\nFunction g"); 
	root = bisect(x_left, x_right, epsilon, g); 
	printf("\n   g(%.7f) = %e\n", root, g(root)); 
 
	printf("\n\nFunction h"); 
	root = bisect(x_left, x_right, epsilon, h); 
	printf("\n   h(%.7f) = %e\n", root, h(root)); 

	return (0); 
} 

/* 
 *  Implements the bisection method for finding a root of a function f.
 *  Finds a root if signs of fp(x_left) and fp(x_right) are different. 
 */ 
double 
bisect(double x_left,          /* input  - endpoints of interval in */ 
       double x_right,         /* which to look for a root          */ 
       double epsilon,         /* input  - error tolerance          */ 
       double f(double farg))  /* input  - the function             */ 
{
	/*  If there is a root, it is the midpoint of [x_left, x_right] */ 
	return FindRoot(x_left,x_right,epsilon,f); 
} 
 
/*  Functions for which roots are sought */ 
 
/*    3     2 
 *  5x  - 2x  + 3 
 */ 
double 
g(double x) 
{ 
	return (5 * pow(x, 3.0) - 2 * pow(x, 2.0) + 3); 
} 
 
/*   4     2 
 *  x  - 3x  - 8 
 */ 
double 
h(double x) 
{ 
	return (pow(x, 4.0) - 3 * pow(x, 2.0) - 8); 
} 

/* Finds root recursively without error checking (as mentioned in Q5) 
 * 
 * Pre: Should be a root between x_left and x_right                   */
double
FindRoot(double x_left,        /* input  - endpoints of interval in  */ 
         double x_right,       /* which to look for a root           */
         double epsilon,       /* input  - error tolerance           */ 
         double f(double farg)) /* input  - the function              */
{
	double x_mid,    /* midpoint of interval */ 
	       f_left, /* f(x_left)            */ 
	       f_mid;  /* f(x_mid)             */ 
	int    root_found = FALSE;

	/* Computes function values at initial endpoints of interval  */ 
	f_left = f(x_left);

	/* Computes midpoint and function value at midpoint */
	x_mid = (x_left + x_right) / 2.0;
	f_mid = f(x_mid);

	if (fabs(x_right - x_left) < epsilon  ||  root_found) {
		printf("\nRoot found at x = %.7f, midpoint of [%.7f, %.7f]",
		       x_mid, x_left, x_right); 
		return x_mid;
	}
	else {
		if (f_mid == 0.0)  {
			/* Here's the root*/ 
			root_found = TRUE; 
		} else if (f_left * f_mid < 0.0) {
			/* Root in [x_left,x_mid]*/ 
			x_right = x_mid;
		} else {
			/* Root in [x_mid,x_right]*/
			x_left = x_mid;
			f_left = f_mid;
		} 
		printf("\nNew interval is [%.7f, %.7f]", 
		       x_left, x_right); 
		return FindRoot(x_left, x_right, epsilon, f);
	}
}
/*############################################################################*/
/*                   	  End of HW06_111044029_PART3.c                      */
/*############################################################################*/
