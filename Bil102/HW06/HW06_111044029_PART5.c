/*############################################################################*/
/* HW06_111044029_PART1.c                                                     */
/* ----------------------                                                     */
/* Created on 04.06.2012 by Furkan Tektas                                     */
/* ­­­­­                                                                           */
/* Description                                                                */
/* ----------------------                                                     */
/* This program calculates the area between points. The coordinates of the    */
/* points are provided by input file and seperated by space. Each line        */
/* contains two coordinates(one point).  Array holds the coordinates and      */
/* area is saved to the output file.                                          */
/*                                                                            */
/*############################################################################*/


/*############################################################################*/
/*                                  Includes                                  */
/*############################################################################*/
#include <stdio.h>

/*­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­----------------------------------------------------------------------------*/
/*                            Function Prototype                              */
/*­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­----------------------------------------------------------------------------*/

/* Read coordinates from a given input file
 * inputFile - Contains coordinates of the points, seperated by space 
 * coordinates[][2] - Coordinate holder array. 
 * arraySize - Num of maximum points(2x of coordinates) */
int CalculateFunction(int n,int k, int turn);
int iterative(int n, int k);

int
main(void)
{
    int n,k;
    
    /* Prompting user for n & k */
    printf("Enter n and k seperated by space => ");
    scanf("%d%d", &n, &k);

    printf("f(%d,%d) = %d\n", n, k, CalculateFunction(n,k,0));
    printf("f(%d,%d) = %d\n", n, k, iterative(n,k));

    return 1;
}

/*----------------------------------------------------------------------------­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­*/
/*                            Function Implementation                         */
/*----------------------------------------------------------------------------­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­*/
int CalculateFunction(int n,int k, int turn)
{
    int i;
    for(i=0;i<turn;++i)
        printf("\t");
    printf("%d - %d", n, k);
    if(n >= 0 && k == 0)
        printf(" = 1");
    printf("\n");
    
    if(n > 0 && k > 0)
        return CalculateFunction(n-1,k-1, turn + 1) + CalculateFunction(n-1,k, turn + 1);
    else if(n >= 0 && k == 0)
        return 1;
    else /*if(n == 0 && k >= 0) */
        return 0;
}

int iterative(int n, int k){
    int	result = 0,
	nCur,kCur;
	
	nCur = n;
	kCur = k;
	
        while(n-1 <= nCur && k-1 <= kCur) {
		while(nCur-1 >= 0 && kCur-1 >= 0) {
			if(kCur == 0)
				++result;
			
		}
		--n;
		--k;

	}
    return result;
}
/*############################################################################*/
/*                         End of HW05_111044029_PART1.c                      */
/*############################################################################*/
