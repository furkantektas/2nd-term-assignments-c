/*############################################################################*/
/* PR01_111044029.c                                                           */
/* ----------------------                                                     */
/* Created on 26.03.2012 by Furkan Tektas                                     */
/* ­­­­­                                                                           */
/* Description                                                                */
/* ----------------------                                                     */
/* This program simulates an exchange game which users can can get help from  */
/* advisors or decide themselves. Advisors are like real people, so they can  */
/* make wrong predictions, mistakenly. Users can ask for help from advisors   */
/* ,repeatedly                                                                */
/*                                                                            */
/* Pre:                                                                       */
/* ­­­­­-----                                                                      */
/* ExchangeRates.txt in format below:                                         */
/*                                                                            */
/*  Day	Euro	Dollar	Pound                                                 */
/*  1	2.326	1.764	2.7802                                                */
/*                                                                            */
/* Notes                                                                      */
/* ­­­­­-----                                                                      */
/* You can play n-1 day, where n is the last daay of ExchangeRates.txt        */
/*                                                                            */
/*############################################################################*/


/*############################################################################*/
/*                                  Includes                                  */
/*############################################################################*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/*############################################################################*/
/*                                   Macros                                   */
/*############################################################################*/


#define COMMISSIONRATE 0.0004 /* Commission rate for for each buying          */

#define INITIALMONEY 10000 /* Initial money in TL */

/* Possibility of advisors' right prediction                                  */
/* Note: rand()%100 returns [0-99] so possibility = possibility -1            */
#define POSSIBILITYA 74
#define POSSIBILITYB 49
#define POSSIBILITYC 39

/* Fee intervals for advisors, usage written below.                           */
#define FEEINTERVALA 12
#define FEEINTERVALB 10
#define FEEINTERVALC 34

/* Lower bounds of fees of the advisors                                       */
/* Usage: MINFEEFORX + rand()%FEEINTERVALX                                    */
#define MINFEEFORA 8
#define MINFEEFORB 5
#define MINFEEFORC 6

/* Currency Macros                                                            */
#define TL     't'
#define DOLLAR 'd'
#define EURO   'e'
#define POUND  'p'

/* Conversion Macros' Strings                                                 */
#define TOTL       "-->TL"
#define TODOLLAR   "-->DOLLAR"
#define TOEURO     "-->EURO"
#define TOPOUND    "-->POUND"

/* Abbrevation strings of currencies                                          */
#define TLSTR      "TL"
#define DOLLARSTR  "DOLLAR"
#define EUROSTR    "EURO"
#define POUNDSTR   "POUND"


/* Inout and Output files                                                     */
#define INPUTFILE  "ExchangeRates.txt"
#define OUTPUTFILE "InvestmentReport.txt"

/* Change 1 to debug mode                                                     */
#define DEBUG 0


/*­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­----------------------------------------------------------------------------*/
/*                            Function Prototypes                             */
/*­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­----------------------------------------------------------------------------*/

/*
 * input : advisor
 * output: fee
 * Use macros to define minimum fee and fee interval for each advisor and
 * get an random integer from GetRandomInt function between advisor's fee
 * interval and adds it to minimum fee and return the sum.
 */
int GetFee(char advisor);

/*
 * input : mod
 * output: random integer
 * Generates random integer between the interval [0-mod]
 */
int GetRandomInt(int mod);

/*
 * input : day
 *         inp : input file
 *         money
 *         currencyCode : Currency Code from macro (TL-$-€-£)
 * Displays the day, user money and the daily currency rates.
 */
void DisplayCurrencyRates(int day, FILE *inp, double money, char currencyCode);

/*
 * input : inp : input file
 * output: day
 * Returns the day by reading from input file.
 */
int GetDay(FILE *inp);

/*
 * input : money
 *       : currency currency rate to multiply with 
 * output: multiplied money and currency rate ( 1/currency rate if currency < 1) 
 */
 
double ConvertCurrency(double money, double currency);
/*
 * input : money
 *         currencyCode : Currency Code from macro (TL-$-€-£)
 *         day
 *         inp : input file
 * output: Turkish Lira equivalent of the given currency
 */
 
double CalculateTlEquivalent(double money, char currencyCode, int day, FILE *inp);
/*
 * input : inp : input file
 *         day
 * output: day
 * Closes the file pointer and re-opens to move cursor to the begining of the 
 * input file and skips line(s) until cursor reaches the desired day.
 */
 
int FindDay(FILE *inp, int day);
/*
 * input : inp : input file
 * Skip lines by scaning chars until finding "\n"
 */
 
void SkipLine(FILE *inp);
/*
 * input : currencyCode : current money's currency code (from macro)
 *         feeA, feeB, feeC : Fees for advisors
 * Displays the investment and advisor options to the user. Advisor's fees are
 * calculated by GetFee function.
 */
void DisplayOptions(char currencyCode, int feeA, int feeB, int feeC);

/*
 * input : money
 *         currencyCode : Currency Code of user's current money (TL-$-€-£)
 *         inp : input file
 *         day
 *         userChoice : user's response to DisplayOptions function
 * output: new money
 * First, converts money to tl equivalent by CalculateTlEquivalent function.
 * Then, converts tl quivalent of the money to the desired currency
 */

double ProcessUserChoice(double money, char currencyCode, FILE *inp, int day, 
                         char userChoice);
/*
 * input : inp : input file
 *         day : current day
 * output: dollar parity
 * Finds the given day by FindDay function, then returns the dollar parity by 
 * GetCurrencyRate function.
 */

double GetDollar(FILE *inp, int day);
/*
 * input : inp : input file
 *         day : current day
 * output: euro parity
 * Finds the given day by FindDay function, then returns the euro parity by 
 * GetCurrencyRate function.
 */

double GetEuro(FILE *inp, int day);
/*
 * input : inp : input file
 *         day : current day
 * output: pound parity
 * Finds the given day by FindDay function, then returns the pound parity by 
 * GetCurrencyRate function.
 */

double GetPound(FILE *inp, int day);
/*
 * input : day
 *         oldMoneyCode : Money's currency code that converted from
 *         newMoneyCode : Money's new currency code
 *         tlEquivalent : TL equivalent of the current money
 *         profit : daily profit (former day's money - current day's money)
 *         file : output file
 * output: fprintf functions result
 * Writes daily report to the output file.
 */

int WriteDailyReport(int day, int oldMoneyCode, int newMoneyCode, double money,
                     double tlEquivalent, double profit, FILE *file);
/*
 * input: advisor : advisor's letter
 *        day
 *        inp : input file
 *        currentMoneyCode : for "keep yput money" message.
 * output: currency code that was given as advice.
 * Displays an appropriate advice to the user by GiveAdvice function.
 */
char DisplayAdvice(char advisor,int day, FILE *inp, char currentMoneyCode);

/*
 * Reads output file char by char and prints to the screen until cursor reaches
 * the end of file.
 */
void DisplayInvestmentReport(void);

/*############################################################################*/
/*  int main()                                                                */
/*  ----------                                                                */  
/*  Return                                                                    */
/*  ----------                                                                */
/*      +1 on success                                                         */
/*      -1 on quit                                                            */
/*############################################################################*/

int
main(void)
{
    FILE *inp,                  /* input file */
         *outp;                 /* output file */
    int day, followingDay,      /* number of current day */
        feeA, feeB, feeC;       /* fees for advisors */

    double currentMoney = INITIALMONEY,
        newMoney,
        tlEquivalent,
        profit;

    char userChoice = TL,
         tempCurrencyCode,
         temp;

    /* Opening IO files */
    inp = fopen(INPUTFILE,"r");
    outp = fopen(OUTPUTFILE,"w");
    
    /* Writing Headers */
    fprintf(outp, "Day     Exchange       Money        ");
    fprintf(outp, "TL Equivalent   Profit\n");
    
    SkipLine(inp);

    day = FindDay(inp, 1);
    /* Getting information from input file and display it */
    do
    {
        if(DEBUG)
            printf("*****main basi getday = %d*****\n", day);
            
        tempCurrencyCode = userChoice;
        
        feeA = GetFee('a');
        feeB = GetFee('b');
        feeC = GetFee('c');
        
        DisplayCurrencyRates(day, inp, currentMoney, userChoice);
        DisplayOptions(userChoice, feeA, feeB, feeC); /* userChoice = currencyCode */
        scanf(" %c%c", &userChoice, &temp);
        if(userChoice == 'q' || userChoice == 'Q')
            return -1;
        else if(userChoice == 'k' || userChoice == 'K')
        {
            /* keep money */
            newMoney = currentMoney;
            /* keep currency code */
            userChoice = tempCurrencyCode;
        } /* else if */
        else if(userChoice == 'a')
        {
            newMoney = -1;
            DisplayAdvice('a', day, inp, tempCurrencyCode);
            currentMoney -= feeA;
        }
        else if(userChoice == 'b')
        {
            newMoney = -1;
            DisplayAdvice('b', day, inp, tempCurrencyCode);
            currentMoney -= feeB;
        }
        else if(userChoice == 'c')
        {
            newMoney = -1;
            DisplayAdvice('c', day, inp, tempCurrencyCode);
            currentMoney -= feeC;
        }
        else
        {
            newMoney = ProcessUserChoice(currentMoney, tempCurrencyCode,
                                         inp, day, userChoice); 
        }/* else */
        if(newMoney > 0) /* Everything is fine. Append changes to the report. */
        {
            
            tlEquivalent = CalculateTlEquivalent(newMoney, userChoice, day, inp);
            currentMoney = CalculateTlEquivalent(currentMoney, tempCurrencyCode, day - 1, inp);
            profit = tlEquivalent - currentMoney;
            WriteDailyReport(day, tempCurrencyCode, userChoice, newMoney, tlEquivalent, profit, outp);
            
        }
        else/* (newMoney < 0) means error */
        {
            --day;
            /* ProcessUserChoice returned *1 due to illegal currency code */
            /* entered, restore the money */
            newMoney = currentMoney;
            /* illegal currency code entered, restore the previous one */
            userChoice = tempCurrencyCode; 
        }/* else */    
        currentMoney = newMoney;
        day = FindDay(inp, day + 1);
        followingDay = FindDay(inp, day + 1);

    }while( (day != EOF) && ( followingDay != EOF) );
    
    fclose(inp);
    fclose(outp);
    DisplayInvestmentReport();
    return 1;
}

/*----------------------------------------------------------------------------­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­*/
/*                            Function Implementations                        */
/*----------------------------------------------------------------------------­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­*/

/*
 * Gets the char of advisor in any case and returns advisor's fee according to
 * his/her fee interval. */
int
GetFee(char advisor)
{
    int minFee,      /* minimum fee */
        feeInterval; /* interval of fee, (max fee - min fee) */

    switch(advisor)
    {
        case 'a':
        case 'A':
            minFee = MINFEEFORA;
            feeInterval = FEEINTERVALA;
            break;
        case 'b':
        case 'B':
            minFee = MINFEEFORB;
            feeInterval = FEEINTERVALB;
            break;
        case 'c':
        case 'C':
            minFee = MINFEEFORC;
            feeInterval = FEEINTERVALC;
            break;
    }
    return GetRandomInt(feeInterval) + minFee;
    
}

/* Generates random integer with given mod */
int
GetRandomInt(int mod)
{
    srand (time ( NULL ) ); /* seed the generator */
    return rand() % mod;
}

/* Get the number of days, if fscanf reaches the EOF, returns EOF, otherwise
 * returns the day number */
int
GetDay(FILE *inp)
{
    int day,
        temp; /* for EOF check */
    temp = fscanf(inp, "%d", &day);
    if(temp == EOF)
        return EOF;
    else
        return day;
}

/* Gets currency rates from input file */
double
GetCurrencyRate(FILE *inp)
{
    double currencyRate;
    fscanf(inp, "%lf", &currencyRate);
    return currencyRate;
}

/* Displays day and currency rates */
void
DisplayCurrencyRates(int day, FILE *inp, double money, char currencyCode)
{
    double euro, dollar, pound, tl = 1; /* currency rates */
    
    euro   = GetEuro(inp, day);
    dollar = GetDollar(inp, day);
    pound  = GetPound(inp, day);
    
    switch(currencyCode)
    {
        case EURO:
            dollar /= euro;
            pound /= euro;
            tl /= euro;
            break;
        case DOLLAR:
            euro /= dollar;
            pound /= dollar;
            tl /= dollar;
            break;
        case POUND:
            dollar /= pound;
            euro /= pound;
            tl /= pound;
            break;
    }
    
    
    printf("Day: %d\n", day);
    printf("\tYour Money: %.2f", money);
    
    /* display currency type */
    switch(currencyCode)
    {
        case TL:
            printf(TLSTR);
            printf("\n\t");
            /* Display Currency Rates */
            printf("Dollar: %-7.4f   Euro: %-7.4f   Pound: %-7.4f\n", 
                    dollar, euro, pound);
            break;
        case DOLLAR:
            printf(DOLLARSTR);
            printf("\n\t");
            printf("Turkish Lira: %-7.4f   Euro: %-7.4f   Pound: %-7.4f\n", 
                    tl, euro, pound);
            break;
        case EURO:
            printf(EUROSTR);
            printf("\n\t");
            printf("Dollar: %-7.4f   Turkish Lira: %-7.4f   Pound: %-7.4f\n", 
                    dollar, tl, pound);
            break;
        case POUND:
            printf(POUNDSTR);
            printf("\n\t");
            printf("Dollar: %-7.4f   Euro: %-7.4f   Turkish Lira: %-7.4f\n", 
                    dollar, euro, tl);
            break;
    }/* switch */
    
    printf("---------------------------------------------------------------\n");
    
}

/* Converts currency to another with given currency rate */
double
ConvertCurrency(double money, double currencyRate)
{
    if(currencyRate > 1)
        currencyRate = 1.0 / currencyRate;
    return ( money * currencyRate ) * ( 1 - COMMISSIONRATE);
}

void
SkipLine(FILE *inp)
{
    char temp;
    /* Skipping Line */
    do
      fscanf(inp, "%c", &temp);
    while (temp != '\n');
}

/* This funtion wastes cpu resources because of closing and opening file.
 * Yet we cannot use pointers and arrays, yet. So, it's a negligible fault.
 */
int
FindDay(FILE *inp, int day)
{
    int tmpDay;
    if(day < 1)
        return -1;
    fclose(inp);
    inp = fopen(INPUTFILE,"r");
    do
    {
        SkipLine(inp);
        tmpDay = GetDay(inp);
        if(tmpDay == EOF)
            return EOF;
    }
    while(tmpDay != day);
    
    if(DEBUG)
        printf("*****FindDay: %d*****\n", day);

    return day;
}


int
WriteDailyReport(int day, int oldMoneyCode, int newMoneyCode, double money,
                 double tlEquivalent, double profit, FILE *file)
{
    printf("\n----------------------------------------------------");
    
    printf("\n\tDay              : %-2d", day);
    printf("\n\tExchange Details : %c->%c", oldMoneyCode, newMoneyCode);
    printf("\n\tMoney            : %.4f", money);
    printf("\n\tTL Eq            : %.4f", tlEquivalent);
    printf("\n\tProfit           : %.4f\n", profit);

    printf("----------------------------------------------------\n\n\n");
    
    return fprintf(file, "%-7d %c->%-11c %-12.4f %-15.4f %-13.4f \n", day, 
                   oldMoneyCode, newMoneyCode, money, tlEquivalent, profit);
}

void
DisplayOptions(char currencyCode, int feeA, int feeB, int feeC)
{
    printf("What do you want?\n");
    printf("\t[k]Keep the money\n");
    if(currencyCode != DOLLAR)
        printf("\t[%c]Convert to Dollar\n", DOLLAR);
    if(currencyCode != EURO)
        printf("\t[%c]Convert to Euro\n", EURO);
    if(currencyCode != POUND)
        printf("\t[%c]Convert to Pound\n", POUND);
    if(currencyCode != TL)
        printf("\t[%c]Convert to Turkish Lira\n", TL);
    printf("\t[a]Consult Advisor 'A' [%d TL]\n", feeA);
    printf("\t[b]Consult Advisor 'B' [%d TL]\n", feeB);
    printf("\t[c]Consult Advisor 'C' [%d TL]\n", feeC);
    printf("\t[q]Quit\n");
}

double
ProcessUserChoice(double money, char currencyCode,
                  FILE *inp, int day, char userChoice)
{
    if(DEBUG)
        printf("*****currencyCode = %c, userChoice = %c*****\n", currencyCode, userChoice);

    /* Convert money to TL */
    money = CalculateTlEquivalent(money, currencyCode, day, inp);
    
    if(DEBUG)
        printf("Money in TL: %f\n", money);
    
    switch(userChoice)
    {
        case DOLLAR:
            if(DEBUG)
                printf("currency : %.4f", GetDollar(inp, day));
            return ConvertCurrency(money, GetDollar(inp, day));
        case EURO:
            if(DEBUG)
                printf("currency : %.4f", GetEuro(inp, day));
            return ConvertCurrency(money, GetEuro(inp, day));
        case POUND:
            if(DEBUG)
                printf("currency : %.4f", GetPound(inp, day));
            return ConvertCurrency(money, GetPound(inp, day));
        case TL:
            return money; /* here the money is tl equivalent of currency */
                
        default:
            printf("*************Illegal Choice! Try Again!************* \n");
            return -1.0;
    }/* switch */
}

double
CalculateTlEquivalent(double money, char currencyCode, int day, FILE *inp)
{
    switch(currencyCode)
    {
        case DOLLAR:
            money *= GetDollar(inp, day);
            break;
        case EURO:
            money *= GetEuro(inp, day);
            break;
        case POUND:
            money *= GetPound(inp, day);
            break;
        /* Conversion of TL is needless */
    }/* switch */
    return money;
    
}

double
GetEuro(FILE *inp, int day)
{
    if(day != FindDay(inp, day) )
        return -1;
    return GetCurrencyRate(inp); /* currency = euro */
}

double
GetDollar(FILE *inp, int day)
{
    if(day != FindDay(inp, day) )
        return -1;
    GetCurrencyRate(inp);        /* currency = euro */
    return GetCurrencyRate(inp); /* currency = dollar   */
}

double
GetPound(FILE *inp, int day)
{
    if(day != FindDay(inp, day) )
        return -1;
    GetCurrencyRate(inp);        /* currency = euro */
    GetCurrencyRate(inp);        /* currency = dollar   */
    return GetCurrencyRate(inp); /* currency = pound  */
}

char
InvestmentRecommendation(int day, FILE *inp)
{
    double dollarProfitRate,
           euroProfitRate,
           poundProfitRate;
    dollarProfitRate = GetDollar(inp, day + 1) / GetDollar(inp, day);
    euroProfitRate = GetEuro(inp, day + 1) / GetEuro(inp, day);
    poundProfitRate = GetPound(inp, day + 1) / GetPound(inp, day);
    
    if(dollarProfitRate < 0 && euroProfitRate < 0 && poundProfitRate < 0)
        return TL;
    
    else if(dollarProfitRate > euroProfitRate)
    {
        if(dollarProfitRate > poundProfitRate)
            return DOLLAR;
        else
            return POUND;
    }
    else if(euroProfitRate > poundProfitRate)
        return EURO;
    else
        return POUND;
}

char
GiveAdvice(char advisor,int day, FILE *inp, char currentMoneyCode)
{
   char bestCurrency, /* Good advice */
        randCurrency; /* fake advice */
   int  randInt;
   
   bestCurrency = InvestmentRecommendation(day, inp); 
   randInt = GetRandomInt(100);
   
   switch(GetRandomInt(4))
   {
        case 0:
            randCurrency = DOLLAR;
            break;
        case 1:
            randCurrency = EURO;
            break;
        case 2:
            randCurrency = POUND;
            break;
        case 3:
            randCurrency = TL;
            break;
    }/* switch */
    
    switch(advisor)
    {
        case 'a':
        case 'A':
            if(randInt < POSSIBILITYA)
                return bestCurrency;
            else
                return randCurrency;
            break;
        case 'b':
        case 'B':
            if(randInt < POSSIBILITYB)
                return bestCurrency;
            else
                return randCurrency;
            break;
        default: /* default = c */
            if(randInt < POSSIBILITYC)
                return bestCurrency;
            else
                return randCurrency;
            break;
    }/* switch */
}

char
DisplayAdvice(char advisor,int day, FILE *inp, char currentMoneyCode)
{
    char advice;
    
    advice = GiveAdvice(advisor, day, inp, currentMoneyCode);
    
    printf("\t----------------------------------------------------\n");
    
    if(currentMoneyCode == advice)
        printf("\t                  Keep your money!\n");
    else
    {
        
        switch(advice)
        {
            case DOLLAR:
                printf("\t         Dollar helps you make more profit!\n");
                
            case EURO:
                printf("\t          Euro helps you make more profit!\n");
                
            case POUND:
                printf("\t         Pound helps you make more profit!\n");
                
            case TL:
                printf("\t      Turkish Lira helps you make more profit!\n");
                
        } /* switch */
        
    }/* else */
    
    printf("\t----------------------------------------------------\n");
    return advice;
    
}

void
DisplayInvestmentReport(void)
{
    FILE *file;
    char ch;
    int eof;
    
    file = fopen(OUTPUTFILE, "r");
    
    do{
        eof = fscanf(file,"%c",&ch);
        printf("%c",ch);        
    }
    while(eof != EOF);
    
    fclose(file);
}

/*############################################################################*/
/*                             End of PR01_111044029.c                        */
/*############################################################################*/
