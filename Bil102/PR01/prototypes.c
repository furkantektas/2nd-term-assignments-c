/*############################################################################*/
/*                                   Macros                                   */
/*############################################################################*/

#define COMMISSIONRATE 0.0004 /* Commission rate for for each buying          */

#define INITIALMONEY 10000 /* Initial money in TL */

/* Possibility of advisors' right prediction                                  */
/* Note: rand()%100 returns [0-99] so possibility = possibility -1            */
#define POSSIBILITYA 74
#define POSSIBILITYB 49
#define POSSIBILITYC 39

/* Fee intervals for advisors, usage written below.                           */
#define FEEINTERVALA 12
#define FEEINTERVALB 10
#define FEEINTERVALC 34

/* Lower bounds of fees of the advisors                                       */
/* Usage: MINFEEFORX + rand()%FEEINTERVALX                                    */
#define MINFEEFORA 8
#define MINFEEFORB 5
#define MINFEEFORC 6

/* Currency Macros                                                            */
#define TL     't'
#define DOLLAR 'd'
#define EURO   'e'
#define POUND  'p'

/* Conversion Macros' Strings */
#define TOTL       "-->TL"
#define TODOLLAR   "-->DOLLAR"
#define TOEURO     "-->EURO"
#define TOPOUND    "-->POUND"

#define INPUTFILE  "ExchangeRates.txt"
#define OUTPUTFILE "InvestmentReport.txt"
#define TEMPFILE   "TempFile.txt"

/*­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­----------------------------------------------------------------------------*/
/*                            Function Prototypes                             */
/*­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­----------------------------------------------------------------------------*/

/*
 * input : advisor
 * output: fee
 * Use macros to define minimum fee and fee interval for each advisor and
 * get an random integer from GetRandomInt function between advisor's fee
 * interval and adds it to minimum fee and return the sum.
 */
int GetFee(char advisor);
/*
 * input : mod
 * output: random integer
 * Generates random integer between the interval [0-mod]
 */
int GetRandomInt(int mod);

/*
 * input : day
 *         inp : input file
 *         money
 *         currencyCode : Currency Code from macro (TL-$-€-£)
 * Displays the day, user money and the daily currency rates.
 */
void DisplayCurrencyRates(int day, FILE *inp, double money, char currencyCode);
/*
 * input : inp : input file
 * output: day
 * Returns the day by reading from input file.
 */
int GetDay(FILE *inp);

/*
 * input : money
 *       : currency currency rate to multiply with 
 * output: multiplied money and currency rate ( 1/currency rate if currency < 1) 
 */
double ConvertCurrency(double money, double currency);
/*
 * input : money
 *         currencyCode : Currency Code from macro (TL-$-€-£)
 *         day
 *         inp : input file
 * output: Turkish Lira equivalent of the given currency
 */
double CalculateTlEquivalent(double money, char currencyCode, int day, FILE *inp);

/*
 * input : inp : input file
 *         day
 * output: day
 * Closes the file pointer and re-opens to move cursor to the begining of the 
 * input file and skips line(s) until cursor reaches the desired day.
 */
int FindDay(FILE *inp, int day);
/*
 * input : inp : input file
 * Skip lines by scaning chars until finding "\n"
 */
void SkipLine(FILE *inp);
/*
 * input : currencyCode : current money's currency code (from macro)
 * Displays the investment and advisor options to the user. Advisor's fees are
 * calculated by GetFee function.
 */
void DisplayOptions(char currencyCode);

/*
 * input : money
 *         currencyCode : Currency Code of user's current money (TL-$-€-£)
 *         inp : input file
 *         day
 *         userChoice : user's response to DisplayOptions function
 * output: new money
 * First, converts money to tl equivalent by CalculateTlEquivalent function.
 * Then, converts tl quivalent of the money to the desired currency
 */
double ProcessUserChoice(double money, char currencyCode, FILE *inp, int day, 
                         char userChoice);
/*
 * input : inp : input file
 *         day : current day
 * output: dollar parity
 * Finds the given day by FindDay function, then returns the dollar parity by 
 * GetCurrencyRate function.
 */
double GetDollar(FILE *inp, int day);
/*
 * input : inp : input file
 *         day : current day
 * output: euro parity
 * Finds the given day by FindDay function, then returns the euro parity by 
 * GetCurrencyRate function.
 */
double GetEuro(FILE *inp, int day);
/*
 * input : inp : input file
 *         day : current day
 * output: pound parity
 * Finds the given day by FindDay function, then returns the pound parity by 
 * GetCurrencyRate function.
 */
double GetPound(FILE *inp, int day);

/*
 * input : day
 *         oldMoneyCode : Money's currency code that converted from
 *         newMoneyCode : Money's new currency code
 *         tlEquivalent : TL equivalent of the current money
 *         profit : daily profit (former day's money - current day's money)
 *         file : output file
 * output: fprintf functions result
 * Writes daily report to the output file.
 */
int WriteDailyReport(int day, int oldMoneyCode, int newMoneyCode, double money,
                     double tlEquivalent, double profit, FILE *file);
                     
/*
 * input: advisor : advisor's letter
 *        day
 *        inp : input file
 *        currentMoneyCode : for "keep yput money" message.
 * output: currency code that was given as advice.
 * Displays an appropriate advice to the user by GiveAdvice function.
 */
char DisplayAdvice(char advisor,int day, FILE *inp, char currentMoneyCode);

/*
 * Reads output file char by char and prints to the screen until cursor reaches
 * the end of file.
 */
void DisplayInvestmentReport(void);
