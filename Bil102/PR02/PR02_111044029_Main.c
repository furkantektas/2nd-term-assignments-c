/*PR02_111044029_Main.c

 111044029 - Furkan Tektas
 Last Modified: 13.06.2012 
 
 BASIC POLYCLINIC AUTOMATION */
#include "PR02_111044029_Polyclinic.c"

int
main(void)
{
    cale_t           curDay;
    humans_t         humans;
    clinics_t        clinics;
    reservations_t   reservations;
    
    #ifndef TESTMODE
            outputFile = stdout;
            inputFile = stdin;
    #else
            outputFile = fopen(OUTPUTFILE,"w");
            inputFile = fopen(INPUTFILE,"r");
    #endif

    ClearScreen();
    /* Check the prerequisites and fill the minimum required data */	
    InitializeData(&humans, &clinics, &reservations,&curDay);

    if(TRACE)
            fprintf(outputFile,"****Continuing to main() with data\n");
    PolyclinicAutomation(&humans, &clinics, &reservations, &curDay);
    
    /* Saving the session's data to database files */
    SaveDataToFiles(&humans, &clinics, &reservations, &curDay);

    TxtOutput(&clinics,&humans,&reservations);

    fclose(outputFile);
    fclose(inputFile);

    return 0;
}
