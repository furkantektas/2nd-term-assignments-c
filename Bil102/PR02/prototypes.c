/*
  111044029 Furkan Tektas
  Project 2 - Function Prototypes */


/* Daha onceki kullanimlarda olusturulmus dosyalari kontrol eder. Eger dossyalar
   mevcutsa ve iclerinde bilgi varsa bunlari ilgili struct'lara import eder.*/
void InitializeData(humans_t *humans, clinics_t *clinics, reservations_t *reservations);

/* Tum verileri kendi dosyalarina kaydeder. */
void SaveDataToFiles(humans_t *humans,clinics_t *clinics, reservations_t *reservations);

/* Menuler */
/* Genel Menuyu gosterir. Genel Menu 4 adet alt menuye baglidir. Bu menuleri
   gosteren fonksiyonlarin prototype'lari asagidadir*/
char DisplayMainMenu(void);

/* Asagidaki menulerin tamaminda gosterilecek olan kismi ekrana yazar */
void DisplayCommonMenu(void);

/* Detay menusunu gosterir. ID'si girilen clinic/doctor/patient/reservation icin
   tum bilgileri gosterme menusu */
char DisplayDetailMenu(void);

/* Varolan clinic/doctor/patient/reservation girdisini guncelleme menusu */
char DisplayUpdateMenu(void);

/* clinic/doctor/patient/reservation olusturma menusu */
char DisplayCreateMenu(void); 

/* Tum clinic/doctor/patient/reservation listesini gosterme menusu */
char DisplayListMenu(void);

/* Kullaniciya gonderilen mesaji gostererek kullanicidan integer alip 
   return eder */
int GetUserInputInt(char *message);

/* Kullanicidan char alip uppercase olarak return eder */
char GetUserInputChar(void);

/* Daha once kaydedilmis olan dosyadan verileri okur. Okudukca numOfElements'i
   arttirir. */
int GetDataFromFile(FILE *file, void *data, int sizeOfData, int *numOfElements);

/* Hasta Bolumu */
/* Id'yi arttirip ModifyPatient() fonksiyonu yardimiyla yeni hasta ekler. */
void AddPatient(humans_t *humans); 

/* Id'si girilen hastanin bilgilerini ModifyPatient() fonksiyonu yardimiyla
   gunceller (id haric) */
void UpdatePatient(human_t *patient);

/* AddPatient() ve UpdatePatient() fonksiyonlari tarafindan cagrilir.
   Kullanicidan alinacak verileri input/output parametre olarak verilen 
   patient'e yazar */
void ModifyPatient(human_t *patient);

/* Doktor Bolumu */
/* Id'yi arttirip ModifyDoctor() fonksiyonu yardimiyla yeni doktor ekler. 
   clinics verisi doktorun clinicId'sini almak icin kullanilir. */
void AddDoctor(humans_t *humans, clinics_t *clinics);

/* Id'si girilen doktorun bilgilerini ModifyDoctor() fonksiyonu yardimiyla
   gunceller (id haric) 
   clinics verisi doktorun clinicId'sini almak icin kullanilir. */
void UpdateDoctor(human_t *doctor, clinics_t *clinics);

/* AddDoctor() ve UpdateDoctor() fonksiyonlari tarafindan cagrilir.
   Kullanicidan alinacak verileri input/output parametre olarak verilen 
   doctor'a yazar.
   clinics verisi doktorun clinicId'sini almak icin kullanilir.  */
void ModifyDoctor(human_t *doctor, clinics_t *clinics);


/* Rezervasyon Menusu */
/* ControlClinicCapacity() fonksiyonuyla kullanicidan alinan gun ve klinikte, 
   o gun icin yer var mi diye kontrol eder. Eger varsa id'si girilen hastaya
   istenilen gun ve klinikte yeni bir rezervasyon olusturur. */
void AddReservation(reservations_t *reservations, humans_t *humans, clinics_t *clinics);

/* Rezervasyon eklerken verilen gun ve verilen klinikte o gun icin yer var mi
   diye kontrol eder. Bunun icin  GetNumOfReservationsOnDay() ve 
   GetNumOfClinicDoctors() fonksiyonlarindan yararlanir. Klinigin bir hastaya
   bakma suresini, klinikteki doktor sayisi ve klinigin toplam calisma saatini
   kullanarak klinik kapasitesi hesaplar. */
int ControlClinicCapacity(int day, clin_t *clinic, reservations_t *reservations, humans_t *humans);

/* Verilen klinik icin verilen gundeki randevu sayisini return eder. */
int GetNumOfReservationsOnDay(int clinicId, int day, reservations_t *reservations);

/* Id'si verilen klinige bagli doktor sayisini return eder */
int GetNumOfClinicDoctors(int clinicId, humans_t *humans);


/* Human Menusu */
/* Doktor ve Hasta icin ortak olan bilgileri kullanicidan alir ve return eder */
human_t AddHuman();

/* Adresi verilen human_t struct'ini yeniden doldurur */
void UpdateHuman(human_t *human);


/*Klinik menusu */
/* idClinics'i bir arttirarak yeni bir klinik ekler */
void AddClinic(clinics_t *clinics);

/* Rezervasyonlarin tamamini listeler. 
   PrintReservationLine() fonksiyonunu cagirir.*/
void PrintReservations(reservations_t *reservations, clinics_t *clinics, humans_t *humans);

/* Satir olarak rezervasyon bilgilerini basar. clinics ve humans bilgileri ile
   id'si bilinen hasta adini ve id'si bilinen klinigin adini gerekli 
   fonksiyonlari cagirarak ekrana basar. */
void PrintReservationLine(reser_t *reservation, clinics_t *clinics, humans_t *humans);

/* Tum hastalari listeler.
   PrintPatientLine() fonksiyonunu cagirir. */
void PrintPatients(humans_t *humans);

/* Tek bir hastanin tum bilgilerini ekrana basar */
void PrintPatient(human_t *patient);

/* Tek satir halinde hastanin genel bilgilerini ekrana yazdirir.
   PrintPatients() fonksiyonu tarafindan cagrilir */
void PrintPatientLine(human_t *patient);


/* human_t struct'indaki name stringini yazdirir. 
   id'si bilinen insanin adini yazdirmak icin PrintReservationLine() fonksiyonu
   gibi fonksiyonlarda kullanilir.*/
void PrintHumanName(human_t *human);

/* Verilen klinige bagli doktorlari listeler. PrintDoctorLine() fonksiyonunu
   cagirir */
void PrintDoctorsOfClinics(clin_t *clinic,humans_t *humans);

/* Tum doktorlari listeler.
   PrintDoctorLine() fonksiyonunu cagirir. */
void PrintDoctors(humans_t *humans, clinics_t *clinics); 

/* Tek bir doktorun tum bilgilerini ekrana basar */
void PrintDoctor(human_t *doctor, clinics_t *clinics);

/* Tek satir halinde doktorun genel bilgilerini ekrana yazdirir.
   PrintDoctors() fonksiyonu tarafindan cagrilir */
void PrintDoctorLine(human_t *doctor, clin_t *clinic);

/* Hasta ve doktor icin ortak olan  ozellikleri ekrana yazdirir*/
void PrintHuman(human_t *human);

/* Hasta ve doktor listelerinde ortak olan alanlari ekrana yazdirir*/
void PrintHumanLine(human_t *human);

/* Tum klinikleri listeler
   PrintClinicLine() fonksiyonunu kullanir. */
void PrintClinics(clinics_t * clinics);

/* Tek satir halinde klinik bilgilerini ekrana basar
   PrintClinics() tarafindan cagrilir */
void PrintClinicLine(clin_t *clinic);

/* Verilen klinigin tum bilgilerini ve klinige bagli olan doktorlarin listesini
   yazdirir.*/
void PrintClinic(clin_t *clinic, humans_t *humans);

/* Verilen klinigin adini yazdirir. Doktor bilgileri ekrana yazdirilirken 
   kullanilir*/
void PrintClinicName(clin_t *clinic);

/* Ekrana ortalanmis ve alti-ustu tirelerle cevrelenmis sekilde gonderilen
   mesaji ekrana basar. Kullaniciyi bilgilendirmek icin kullaniilir.*/
void PrintFlashMessage(char *message);

/* Ekrani temizleyip, Ekranin en ustune ortalanmis olarak verilen stringi yazar
   Altina da PrintLine() fonksiyonu ile cizgi ceker. Bulunulan menunun adinin
   gosterilmesi ve arayuz icin kullanilir. */
void PrintHeader(char *header);

/* Ekrana bir satir boyunca(80 karakter) tire basarak cizgi cizer */
void PrintLine(void);

/* Eger TRACE mode'u aktif degil ise ekrani temizler */
void ClearScreen(void);

/* Kullanicidan deger alindiktan sonra '\n' karakterine kadar olan tum 
   karakterleri yoksayar */
void flushInputBuffer();
