ID   NAME                     TEL            ACTIVE         CLINIC              
--------------------------------------------------------------------------------
1    Mehmet Oz                567123123      Yes            Neurology      
3    Doctor One               123456789      Yes            Neurology      
4    Doctor Two               1234566        Yes            Pediatry       
5    John Doe                 12345678       Yes            In. Medicine   
6    Jane Doe                 123123123      Yes            Surgery        
7    Doctor Two               123455         No             Dermatology    
8    Dentist One              12312312       Yes            Dental C.      
9    Radiologist              1231132        Yes            Radiology      
23   Doctor Z                 --             Yes            Dermatology    

ID   NAME                     TEL            AGE       OCCUPATION               
--------------------------------------------------------------------------------
2    John Doe                 12312312       12        Patient                  
10   Patient One              12312312       78        Teacher                  
11   Patient 2                123123123      45        Police                   
12   Patient 3                12312312       30        Engineer                 
13   Another Pat.             123123         13        Student                  
14   Baby P.                  --             1         baby :)                  
15   Child P.                 1231231        12        Student                  
16   Patient A                123123         20        Student                  
17   Patient B                123123         25        Engineer                 
18   Patiente C               343434         24        Student                  
19   Harun                    125126512      34        Scientist                
20   Patient F                1612612        21        --                       
21   Murat                    12312312       25        Doctor                   
22   Mustafa                  12712612       30        --                       
