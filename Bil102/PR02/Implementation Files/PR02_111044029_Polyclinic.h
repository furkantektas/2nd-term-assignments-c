/* PR02_111044029_Polyclinic.h 
 
 111044029 - Furkan Tektas
 Last Modified: 13.06.2012
 */
#ifndef POLYCLINIC_H_INCLUDED
#define POLYCLINIC_H_INCLUDED

#define CLINFILE	"clinics.bin"
#define CALEFILE	"calendar.bin"
#define RESEFILE	"reservations.bin"
#define HUMANFILE	"human.bin"

#define HUMANTXTFILE	"human.txt"
#define CLINTXTFILE	"clinics.txt"
#define RESETXTFILE	"reservations.txt"

/* Uncomment the line below to activating TEST MODE.
   input.txt consists of all of the required information for the first run.
   All of the *.bin files should be removed before TESTMODE */
/*#define TESTMODE*/

/* Change 0 to 1 for activating the trace mode */
#define TRACE 0

#define OUTPUTFILE 	"output.txt"
#define INPUTFILE  	"input.txt"

#define MAXNUMOFCLINICS 	25
#define MAXNUMOFHUMANS 		80
#define MAXNUMOFRESERVATIONS 	250
#define MAXNUMOFCALENDAR 	250*80*5 /* RESERVATIONS*PATIENTS*5 */

#define ILLEGALCHOICE "Illegal Choice! Try Again!" /*Illegal menu entry error*/
#define NEWENTRY -1 /*Numeric value for update functions to determine creation*/

#define FALSE 0
#define TRUE  1

#define NOTEXAMINED  		0
#define EXAMINED     		1
#define CANCELED     		2

#define SMALLSTRSIZE 		15
#define MEDIUMSTRSIZE 		50
#define LONGSTRSIZE 		255

typedef enum{
	doctorEnum, patientEnum
} humanTypes_t;

typedef struct {
	int	id;
        double  hourOfStarting,
		hourOfFinishing,
		examinePeriod;
	char	name[SMALLSTRSIZE];
} clin_t; /* clinics */

typedef struct {
	int	/*id,*/
		active,
		clinicId;
} doct_t; /* doctor */

typedef struct {
	int	id,
		age;
	char	occupation[SMALLSTRSIZE],
		medicalHist[LONGSTRSIZE];
} pati_t; /* patient */

typedef union {
	doct_t	doctor;
	pati_t	patient;
} hTyp_t; /* humanType */

typedef struct {
	int	id;
	humanTypes_t	hType;
	char	name[SMALLSTRSIZE],
		address[SMALLSTRSIZE],
		extraInfo[LONGSTRSIZE],
		tel[SMALLSTRSIZE], /* int is not enough to hold it */
		gender;
	hTyp_t	type;
} human_t; /* human */

typedef struct {
	int	day;
} cale_t; /* calendar - time */

typedef struct {
	int	id,
		patientId,
		doctorId,
		clinicId,
                status;
	cale_t	time;
	char	doctorNotes[LONGSTRSIZE];
} reser_t; /* reservations */

typedef struct {
    human_t arr[MAXNUMOFHUMANS];
    int idHumans;
} humans_t;

typedef struct {
    clin_t arr[MAXNUMOFCLINICS];
    int idClinics;
} clinics_t;

typedef struct {
    reser_t arr[MAXNUMOFRESERVATIONS];
    int idReservations;
} reservations_t;

FILE *outputFile,
     *inputFile;
        
/* main function for Polyclinic.c */
int PolyclinicAutomation(humans_t *humans, clinics_t *clinics, 
                         reservations_t *reservations, cale_t *curDay);

/* Generates txt output data at the end of the execution */
void TxtOutput(clinics_t *clinics,humans_t *humans,reservations_t *reservations);

/* Loads binary data from last session */
void InitializeData(humans_t *humans,clinics_t *clinics,reservations_t *reservations,cale_t *calendar);

/* Saves all of the data to binary files at the end of the execution */
void SaveDataToFiles(humans_t *humans,clinics_t *clinics,reservations_t *reservations,cale_t *calendar);

/* Menu functions - All of the functions below display menus according to
                    their names. DisplayCommonMenu() displays the common items
                    for each Display*() functions. */
    char DisplayDeleteMenu(cale_t *calendar);
    char DisplayExploreMenu(cale_t *calendar);
    char DisplayUpdateMenu(cale_t *calendar);
    char DisplayCreateMenu(cale_t *calendar);
    char DisplayListMenu(cale_t *calendar);
    void DisplayCommonMenu(void);
    char DisplayMainMenu(cale_t *calendar);

/* Gets a char from inputFile and returns it */
char GetUserInputChar(void);

/* Gets an integer from inputFile and returns it.
   Notifies user by displaying prompt. */
int GetUserInputInt(char *message);


/* Reads binary file and fills the data array.
 * It is called by InitializeData() function */
int GetDataFromFile(FILE *file,void *data,int sizeOfData,int *numOfElements);

/* Human Functions */
    /* Fills an human_t structure and returns it. (Calls UpdateHuman())*/
    human_t AddHuman();
    /* Refills the given human_t structure. */
    void UpdateHuman(human_t *human);

/* Patient Functions*/
    /* By using ModifyPatient(), calls the UpdateHuman() and adds the additional
     * patient_t structure's information (increments the idHumans) */
    void AddPatient(humans_t *humans);
    /* By using ModifyPatient(), calls the UpdateHuman() and adds the additional
     * patient_t structure's information */
    void UpdatePatient(human_t *patient);
    /* Calls UpdateHuman() function and fills the adds the additional patient_t
     * structure's information */
    void ModifyPatient(human_t *patient);

/* Doctor Functions*/
    /* By using ModifyDoctor(), calls the UpdateHuman() and adds the additional
     * human_t structure's information (increments the idHumans) */
    void AddDoctor(humans_t *humans,clinics_t *clinics);
    /* By using ModifyDoctor(), calls the UpdateHuman() and adds the additional
     * patient_t structure's information */
    void UpdateDoctor(human_t *doctor,clinics_t *clinics);
    /* Changes the doctor_t structure's active flag to 0*/
    void DeleteDoctor(human_t *doctor);
    /* Calls UpdateHuman() function and fills the adds the additional doctor_t
     * structure's information */
    void ModifyDoctor(human_t *doctor,clinics_t *clinics);

/* Reservation Functions */
    /* Adds a reservation if clinic has enough capacity */
    void AddReservation(reservations_t *reservations,humans_t *humans,clinics_t *clinics,cale_t *calendar);
    /* Cancels a reservation */
    void CancelReservation(reser_t *reservation);
    /* Cancels all of the reservations between day1 and day2 */
    void CancelReservations(reservations_t *reservations,int day1,int day2);
    /* Examines a reservation */
    void ExamineReservation(reser_t *reservation,clinics_t *clinics,humans_t *humans);
    /* Makes the time->day's examination and returns 1 if all of the reservation
       is examined. */
    int ExamineDay(cale_t *time,reservations_t *reservations,clinics_t *clinics,humans_t *humans);
    /* Returns the number of reservation -which clinicId is clinicId- on day */
    int GetNumOfReservationsOnDay(int clinicId,int day,reservations_t *reservations);

/* Returns an available doctors id for reservation */
int AssignDoctorForReservation(reservations_t *reservations,int day,int clinicId,humans_t *humans);

/* Checks the availability of the clinic for a reservation and returns 1 if
   clinic has a room for a reservation, 0 for no more room for a reservation */
int ControlClinicCapacity(int day,clin_t *clinic,reservations_t *reservations,humans_t *humans);

/* Returns the capacity of the clinic */
int GetClinicCapacity(clin_t *clinic,humans_t *humans);

/* Returns the number of doctors on a specific clinic */
int GetNumOfClinicDoctors(int clinicId,humans_t *humans);

/* Adds a new clinic */
void AddClinic(clinics_t *clinics);

/* Updates the information of clinic */
void UpdateClinic(clin_t *clinic);

/* Fills the clin_t struct's fields. Called by AddClinic() and UpdateClinic() */
void ModifyClinic(clin_t *clinic);

/* By using PrintFilteredReservations() prints the reservation after the day */
void PrintFutureReservations(reservations_t *reservations,clinics_t *clinics,humans_t *humans,cale_t *calendar);

/* By using PrintFilteredReservations() prints all of the reservations except
 * the canceled ones. */
void PrintReservations(reservations_t *reservations,clinics_t *clinics,humans_t *humans);

/* By using PrintFilteredReservations() prints all of the reservations without
 * any filtering.*/
void PrintAllReservations(reservations_t *reservations,clinics_t *clinics,humans_t *humans);

/* Prints reservations by filtering. Filtering can be customized according to
   reservations' status and day */
void PrintFilteredReservations(reservations_t *reservations,clinics_t *clinics,humans_t *humans,cale_t *calendar,int showCanceled);

/* Prints a reservations */
void PrintReservation(reser_t *reservation,clinics_t *clinics,humans_t *humans);

/* Prints a summary of reservation as a row */
void PrintReservationLine(reser_t *reservation,clinics_t *clinics,humans_t *humans);

/* Prints all of the patients as a table */
void PrintPatients(humans_t *humans);

/* Prints a specific patient's information */
void PrintPatient(human_t *patient);

/* Prints a summary of patient as a row */
void PrintPatientLine(human_t *patient);

/* Prints the name field of a human_t structure */
void PrintHumanName(human_t *human);

/* Prints only the available doctors */
void PrintActiveDoctors(humans_t *humans,clinics_t *clinics);

/* Prints all of the doctors */
void PrintAllDoctors(humans_t *humans,clinics_t *clinics);

/* Prints doctors by filtering. This function is called by PrintActiveDoctors()
   and PrintAllDoctors(). */
void PrintDoctors(humans_t *humans,clinics_t *clinics,int displayPassiveDocs);

/* Prints a doctor */
void PrintDoctor(human_t *doctor,clinics_t *clinics);

/* Prints a summary of doctor as a row */
void PrintDoctorLine(human_t *doctor,clin_t *clinic);

/* Prints the doctors of a specific clincic */
void PrintDoctorsOfClinics(clin_t *clinic,humans_t *humans);

/* Prints the common information of doctor and patient 
 * (human_t structure's informations) */
void PrintHuman(human_t *human);

/* Prints a summary of human as a row. Called by PrintDoctorLine() and 
 * PrintPatientLine() */
void PrintHumanLine(human_t *human);

/* Prints all of the clinics */
void PrintClinics(clinics_t *clinics);

/* Prints a summary of clinic as a row */
void PrintClinicLine(clin_t *clinic);

/* Prints a clinic */
void PrintClinic(clin_t *clinic,humans_t *humans);

/* Prints the name field of the clin_t structure */
void PrintClinicName(clin_t *clinic);

/* Displays a flash message for prompting user */
void PrintFlashMessage(char *message);

/* Displays a header on the top of the screen by calling PrintHeaderWithDay() */
void PrintHeader(char *header);

/* Displays a header on the top of the screen and the day information */
void PrintHeaderWithDay(char *header,int day);

/* Prints a 80 character long horizontal line on the screen */
void PrintLine(void);

/* Clears the screen if the TRACE macro is zero (For *nix like systems)*/
void ClearScreen(void);

/* Gets a string from inputFile and replaces the leading \n character to NULL */
void GetString(char *str,int length,FILE *file);

/* Ignores all of the characters until encountering '\n' character */
void flushInputBuffer();

#endif /* POLYCLINIC_H_INCLUDED */
