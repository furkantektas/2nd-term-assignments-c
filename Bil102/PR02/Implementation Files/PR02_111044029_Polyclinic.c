/* PR02_111044029_Polyclinic.c 
 
 111044029 - Furkan Tektas
 Last Modified: 13.06.2012 */
#ifndef POLYCLINIC_C_INCLUDED
#define POLYCLINIC_C_INCLUDED
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "PR02_111044029_Polyclinic.h"

int
PolyclinicAutomation(humans_t *humans, clinics_t *clinics, 
					 reservations_t *reservations, cale_t *curDay)
{
	int 	tmpInt,
		todaysExamination = 0;
	char 	tmpChar,
		userInput;

	/* Displaying Menu */
	do {
		userInput = DisplayMainMenu(curDay);
		ClearScreen();
		switch(userInput) {
		case 'C': /* Create Menu */
			do{
				userInput = DisplayCreateMenu(curDay);
				ClearScreen();
				switch(userInput) {
					case 'D':
						AddDoctor(humans, clinics);
						break;
					case 'P':
						AddPatient(humans);
						break;
					case 'R':
						AddReservation(reservations,
								   humans, 
								   clinics,
															   curDay);
						break;
					case 'C':
						AddClinic(clinics);
						break;
					case 'M':
					case 'Q':
						break;
					default:
						fprintf(outputFile,"%s\n",ILLEGALCHOICE);
				}/* switch Create Menu */
			} while(userInput != 'M' && userInput != 'Q');
			break;
		case 'L': /* List Menu */
			do {
				userInput = DisplayListMenu(curDay);
				ClearScreen();
				switch(userInput) {
					case 'D':
												/* Printing both active and passive doctors */
						PrintAllDoctors(humans, clinics);
						break;
					case 'P':
						PrintPatients(humans);
						break;
					case 'R':
						PrintReservations(reservations, clinics, humans);
						break;
					case 'C':
						PrintClinics(clinics);
						break;
					case 'M':
					case 'Q':
						break;
					default:
						fprintf(outputFile,"%s\n",ILLEGALCHOICE);
				}/* switch List Menu */
			} while(userInput != 'M' && userInput != 'Q');
			break;
		case 'U': /* Update Menu */
			do {
				userInput = DisplayUpdateMenu(curDay);
				ClearScreen();
				switch(userInput) {
					case 'D':
						PrintActiveDoctors(humans,clinics);
						UpdateDoctor(&humans->arr[GetUserInputInt("ID")], clinics);
						break;
					case 'P':
						PrintPatients(humans);
						UpdatePatient(&humans->arr[GetUserInputInt("ID")]);
						break;
					case 'C':
												PrintClinics(clinics);
												UpdateClinic(&clinics->arr[GetUserInputInt("ID")]);
						break;
					case 'M':
					case 'Q':
						break;
					default:
						fprintf(outputFile,"%s\n",ILLEGALCHOICE);
				}/* switch Update Menu */
			} while(userInput != 'M' && userInput != 'Q');
			break;
		case 'D': /* Delete Menu */
			do {
				userInput = DisplayDeleteMenu(curDay);
				ClearScreen();
				switch(userInput) {
					case 'D':
						PrintHeader("Enter the id of the doctor to remove");
												PrintActiveDoctors(humans,clinics);
												tmpInt = GetUserInputInt("ID");
												DeleteDoctor(&humans->arr[tmpInt]);
						break;
					case 'R':
						PrintFutureReservations(reservations,clinics,humans,curDay);
						CancelReservation(&reservations->arr[GetUserInputInt("ID")]);
						break;
					case 'M':
					case 'Q':
						break;
					default:
						fprintf(outputFile,"%s\n",ILLEGALCHOICE);
				}/* switch Delete Menu */
			} while(userInput != 'M' && userInput != 'Q');
			case 'E': /* Explore Menu */
						do {
								userInput = DisplayExploreMenu(curDay);
								ClearScreen();
								switch(userInput) {
										case 'D':
												PrintAllDoctors(humans,clinics);
												PrintDoctor(&humans->arr[GetUserInputInt("ID")], clinics);
												break;
										case 'P':
												PrintPatients(humans);
												PrintPatient(&humans->arr[GetUserInputInt("ID")]);
												break;
										case 'C':
												PrintClinics(clinics);
												PrintClinic(&clinics->arr[GetUserInputInt("ID")], humans);
												break;
										case 'R':
												PrintAllReservations(reservations,clinics,humans);
												PrintReservation(&reservations->arr[GetUserInputInt("ID")], clinics, humans);
												break;
										case 'M':
										case 'Q':
												break;
										default:
												fprintf(outputFile,"%s\n",ILLEGALCHOICE);
								}/* switch Explore Menu */
						} while(userInput != 'M' && userInput != 'Q');
			break;
			case 'N': /* Next Day */
							if(!todaysExamination) {
								fprintf(outputFile,"You haven't complete today's examinations.\n"
										"All of uncompleted reservations will be "
										"canceled. \nAre you sure to continue?"
										" [Y/N] => ");
								do {
									tmpChar = toupper(GetUserInputChar());
								} while(tmpChar != 'Y' && tmpChar != 'N');
								
								if(tmpChar == 'Y') {
									CancelReservations(reservations,curDay->day,
													   curDay->day+1);
									todaysExamination = 0;
									++(curDay->day);
								}
							}
							else {
								todaysExamination = 0;
								++(curDay->day);
							}
							
			break;
			case 'T': /* Examine Today's Reservations */
							if(todaysExamination) {
								fprintf(outputFile,"Hooray! You have completed today's examinations.\n"
										"You can change the date on main menu below.\n");
							}
							else {
								todaysExamination = ExamineDay(curDay,reservations,clinics,humans);
							}
			break;
			case 'G': /* Go to day */
							fprintf(outputFile,"If you jump another day, all of the waiting "
									"reservations will be\ncanceled between today "
									"and the day you will jump to.\n"
									"Are you sure to continue?"
									" [Y/N] => ");
							do {
								tmpChar = toupper(GetUserInputChar());
							} while(tmpChar != 'Y' && tmpChar != 'N');

							if(tmpChar == 'Y') {
								do {
									fprintf(outputFile,"Please enter a day bigger than "
											"%d to jump => ", curDay->day);
									fscanf(inputFile,"%d", &tmpInt);
								} while(tmpInt <= curDay->day);

								CancelReservations(reservations,curDay->day,
													tmpInt);
								todaysExamination = 0;
								curDay->day = tmpInt;
								ClearScreen();
							}
			break;
						
					case 'Q':
						break;
		default: /* Illegal Choice Error */
			fprintf(outputFile,"%s\n",ILLEGALCHOICE);
		}
	} while(userInput != 'Q');

		if(TRACE) {
			PrintHeader("PROGRAM STATS BEFORE TERMINATION");
			fprintf(outputFile,"Current Day : %d\n", curDay->day);
			fprintf(outputFile,"#humans	 : %d\n",humans->idHumans);
			fprintf(outputFile,"#clinics	: %d\n",clinics->idClinics);
			fprintf(outputFile,"#reservs	: %d\n",reservations->idReservations);
		}
	
	return 0;
}


/*
* Clean the input buffer till encounter with return character 
* Pre: call of fscanf(inputFile,) function
* Post: ignores the following characters till return
*/
void flushInputBuffer()
{
	#ifndef TESTMODE
	char junk;
	/* A loop that cleans the input buffer till '\n' character being entered*/
	
	do
	{
		junk = fgetc(inputFile);
	}while(junk!='\n');
	#endif
}

void
GetString(char *str,int length,FILE *file)
{
	fgets(str,length,file);
	if(strlen(str) < length)
		length = strlen(str);
	str[length-1] = '\0';
}

void
ClearScreen(void) 
{
	if(!TRACE) /* disable screen cleaning on trace mode */
		system("clear");
}

/* Prints a line */
void
PrintLine(void)
{
	int i;
	for(i=0;i<80;++i)
		putc('-', outputFile);
	putc('\n', outputFile);
}

/*Prints header with day */
void
PrintHeaderWithDay(char *header, int day)
{
	int	lineLength,
		i;
		
	
		lineLength = (80 - strlen(header))/2;
		
		/* if day is not -1, print the day */
		if(day != -1) {
			
				fprintf(outputFile,"Day: %-5d",day);
				lineLength -= 10;
		}
		
		for(i=0;i<lineLength;++i)
		putc(' ',outputFile);

	fprintf(outputFile,"%s",header);

	/* Uncomment if you want to press another char instead of space.
	for(i=0;i<lineLength;++i)
		putc(' ',outputFile);
	*/
	putc('\n',outputFile);

	PrintLine();
}


/* Prints only the header */
void
PrintHeader(char *header)
{
	PrintHeaderWithDay(header, -1);
}

void
PrintFlashMessage(char *message)
{
	int	lineLength,
		i;
	lineLength = strlen(message) + 4;

	for(i=0;i<(80-lineLength)/2;++i)
		putc(' ',outputFile);

	for(i=0;i<lineLength;++i)
		putc('-',outputFile);
	putc('\n',outputFile);

	for(i=0;i<(80-lineLength)/2;++i)
		putc(' ',outputFile);
	fprintf(outputFile,"  %s  ",message);
	putc('\n',outputFile);

	for(i=0;i<(80-lineLength)/2;++i)
		putc(' ',outputFile);

	for(i=0;i<lineLength;++i)
		putc('-',outputFile);
	putc('\n',outputFile);
}

/* Prints a clinic name */
void
PrintClinicName(clin_t *clinic)
{
	fprintf(outputFile,"%-15s",clinic->name);
}

void
PrintClinic(clin_t *clinic, humans_t *humans)
{
	ClearScreen();
	PrintHeader("CLINIC DETAILS");
	fprintf(outputFile,"%40s : %-10d\n","ID",clinic->id);
	fprintf(outputFile,"%40s : %-25s\n","Name",clinic->name);
	fprintf(outputFile,"%40s : %-15.2f\n","Examine Period",clinic->examinePeriod);
	fprintf(outputFile,"%40s : %-15.2f\n","Starting Hour",clinic->hourOfStarting);
	fprintf(outputFile,"%40s : %-15.2f\n","Finishing Hour",clinic->hourOfFinishing);
	
	fprintf(outputFile,"\n\n");
	PrintHeader("CLINIC DOCTORS");
	PrintDoctorsOfClinics(clinic, humans);
}
/* Print a clinic in line */
void
PrintClinicLine(clin_t *clinic)
{
		fprintf(outputFile,"%-10d",clinic->id);
		fprintf(outputFile,"%-25s",clinic->name);
		fprintf(outputFile,"%-15.2f",clinic->examinePeriod);
		fprintf(outputFile,"%-15.2f",clinic->hourOfStarting);
		fprintf(outputFile,"%-15.2f",clinic->hourOfFinishing);
		putc('\n',outputFile);
}

/* Print all clinics in a table view */
void
PrintClinics(clinics_t * clinics)
{
	int i;
	fprintf(outputFile,"%-10s","ID");
	fprintf(outputFile,"%-25s","NAME");
	fprintf(outputFile,"%-15s","EXAMINE PERIOD");
	fprintf(outputFile,"%-15s","STARTING HOUR");
	fprintf(outputFile,"%-15s","FINISHING HOUR");
	putc('\n',outputFile);

	PrintLine();

	for(i=1;i<=clinics->idClinics;i++)
		PrintClinicLine(&clinics->arr[i]);
}

void
PrintHumanLine(human_t *human)
{
	fprintf(outputFile,"%-5d",human->id);
	fprintf(outputFile,"%-25s",human->name);
	fprintf(outputFile,"%-15s",human->tel);

}

void
PrintHuman(human_t *human)
{
	fprintf(outputFile,"%40s : %d\n","Id",human->id);
	fprintf(outputFile,"%40s : %c\n","Gender",human->gender);
	fprintf(outputFile,"%40s : %s\n","Name",human->name);
	fprintf(outputFile,"%40s : %s\n","Tel",human->tel);
	fprintf(outputFile,"%40s : %s\n","Extra Info",human->extraInfo);
}

void
PrintDoctorLine(human_t *doctor, clin_t *clinic)
{
		PrintHumanLine(doctor);

		if(doctor->type.doctor.active)
				fprintf(outputFile,"%-15s","Yes");
		else
				fprintf(outputFile,"%-15s","No");

		PrintClinicName(clinic);

		putc('\n',outputFile);
}

void
PrintDoctor(human_t *doctor, clinics_t *clinics)
{
	ClearScreen();
	PrintHeader("VIEW DOCTOR DETAILS");
	PrintHuman(doctor);

	fprintf(outputFile,"%40s : ","Active");
	if(doctor->type.doctor.active)
		fprintf(outputFile,"%-10s","Yes");
	else
		fprintf(outputFile,"%-10s","No");

	putc('\n',outputFile);
	fprintf(outputFile,"%40s : ","Clinic");
	PrintClinicName(&clinics->arr[doctor->type.doctor.clinicId]);
	fprintf(outputFile,"\n\n");
}

void
PrintDoctors(humans_t *humans, clinics_t *clinics, int displayPassiveDocs)
{
	int i;

	fprintf(outputFile,"%-5s", "ID");
	fprintf(outputFile,"%-25s", "NAME");
	fprintf(outputFile,"%-15s", "TEL");
	fprintf(outputFile,"%-15s", "ACTIVE");
	fprintf(outputFile,"%-20s", "CLINIC");
	putc('\n',outputFile);

	PrintLine();

	for(i=1;i<=humans->idHumans;i++) {
		/* Print only the doctors (filtered by activation status) */
		if(humans->arr[i].hType == doctorEnum && 
				   (humans->arr[i].type.doctor.active || displayPassiveDocs))
			PrintDoctorLine(&humans->arr[i],&clinics->arr[humans->arr[i].type.doctor.clinicId]);
	}
}

/* Prints both active and passive doctors. */
void
PrintAllDoctors(humans_t *humans, clinics_t *clinics)
{
	PrintDoctors(humans,clinics,1);
}

/* Prints only the available doctors. */
void
PrintActiveDoctors(humans_t *humans, clinics_t *clinics)
{
	PrintDoctors(humans,clinics,0);
}

void
PrintDoctorsOfClinics(clin_t *clinic,humans_t *humans)
{
	int	i,
		numOfDoctors = 0;
	fprintf(outputFile,"%-5s", "ID");
	fprintf(outputFile,"%-25s", "NAME");
	fprintf(outputFile,"%-15s", "TEL");
	fprintf(outputFile,"%-15s", "ACTIVE");
	fprintf(outputFile,"%-20s", "CLINIC");
	putc('\n',outputFile);

	PrintLine();

	for(i=1;i<=humans->idHumans;i++) {
		/* Print only the doctors */
		if(humans->arr[i].hType == doctorEnum &&
		   humans->arr[i].type.doctor.clinicId == clinic->id) {
			PrintDoctorLine(&humans->arr[i],clinic);
			++numOfDoctors;
		}
	}

	if(numOfDoctors == 0)
		fprintf(outputFile,"No doctor found\n");
}



void
PrintPatientLine(human_t *patient)
{
	PrintHumanLine(patient);
	fprintf(outputFile,"%-10d",patient->type.patient.age);
	fprintf(outputFile,"%-25s\n",patient->type.patient.occupation);
}

void
PrintHumanName(human_t *human)
{
	fprintf(outputFile,"%-15s",human->name);
}

void
PrintPatient(human_t *patient)
{
		ClearScreen();
		PrintHuman(patient);
		fprintf(outputFile,"%40s : %d\n","Age",patient->type.patient.age);
		fprintf(outputFile,"%40s : %s\n","Occupation",patient->type.patient.occupation);
		fprintf(outputFile,"%40s : %s\n","Medical History",patient->type.patient.medicalHist);
}

void
PrintPatients(humans_t *humans)
{
	int i;

	fprintf(outputFile,"%-5s", "ID");
	fprintf(outputFile,"%-25s", "NAME");
	fprintf(outputFile,"%-15s", "TEL");
	fprintf(outputFile,"%-10s", "AGE");
	fprintf(outputFile,"%-25s", "OCCUPATION");
	putc('\n',outputFile);

	PrintLine();

	for(i=1;i<=humans->idHumans;i++) {
		/* Print only the doctors */
		if(humans->arr[i].hType == patientEnum)
			PrintPatientLine(&humans->arr[i]);
	}
}


void
PrintReservationLine(reser_t *reservation, clinics_t *clinics, humans_t *humans)
{
	fprintf(outputFile,"%-5d",reservation->id);
	PrintClinicName(&clinics->arr[reservation->clinicId]);
	fprintf(outputFile,"%5c",' '); /* for alignment */
	PrintHumanName(&humans->arr[reservation->doctorId]);
	fprintf(outputFile,"%5c",' '); /* for alignment */
	PrintHumanName(&humans->arr[reservation->patientId]);
	fprintf(outputFile,"%5c",' '); /* for alignment */
	fprintf(outputFile,"%-5d",reservation->time.day);
	
	if(reservation->status == CANCELED)
		fprintf(outputFile,"%-10s\n","Canceled");
	else if(reservation->status == EXAMINED)
		fprintf(outputFile,"%-10s\n","Examined");
	else if(reservation->status == NOTEXAMINED)
		fprintf(outputFile,"%-10s\n","Waiting");
}

void
PrintReservation(reser_t *reservation, clinics_t *clinics, humans_t *humans)
{
	fprintf(outputFile,"%38s : %-39d\n", "ID",reservation->id);
	fprintf(outputFile,"%38s : ", "CLINIC");

	PrintClinicName(&clinics->arr[reservation->clinicId]);
	putc('\n',outputFile);
		
	fprintf(outputFile,"%38s : ", "DOCTOR");
	PrintHumanName(&humans->arr[reservation->doctorId]);
	putc('\n',outputFile);

	fprintf(outputFile,"%38s : ", "PATIENT");
	PrintHumanName(&humans->arr[reservation->patientId]);
	putc('\n',outputFile);
		
	fprintf(outputFile,"%38s : %-39d\n", "DAY",reservation->time.day);

	fprintf(outputFile,"%38s : ", "STATUS");
	if(reservation->status == CANCELED)
		fprintf(outputFile,"%-10s\n","Canceled");
	else if(reservation->status == EXAMINED)
		fprintf(outputFile,"%-10s\n","Examined");
	else if(reservation->status == NOTEXAMINED)
		fprintf(outputFile,"%-10s\n","Waiting");
	
	if(reservation->status == EXAMINED)
		fprintf(outputFile,"%38s : %-39s\n", "DOCTOR'S NOTE",reservation->doctorNotes);		
}

/* Print all of the reservations. If day is zero prints all of the reservations.
   If day is not zero, prints the future reservations */
void
PrintFilteredReservations(reservations_t *reservations, clinics_t *clinics,
						  humans_t *humans, cale_t *calendar, int showCanceled)
{
	int i;

	fprintf(outputFile,"%-5s", "ID");
	fprintf(outputFile,"%-20s", "CLINIC");
	fprintf(outputFile,"%-20s", "DOCTOR");
	fprintf(outputFile,"%-20s", "PATIENT");
	fprintf(outputFile,"%-5s", "DAY");
	fprintf(outputFile,"%-10s", "STATUS");
	putc('\n',outputFile);

	PrintLine();
		
	for(i=1;i<=reservations->idReservations;i++)
		/*Filtering results by date and status */
		if((showCanceled || reservations->arr[i].status != CANCELED) &&
		   (reservations->arr[i].time.day >= calendar->day))
			PrintReservationLine(&reservations->arr[i],clinics,humans);
}

/* Prints all of the reservations*/
void
PrintAllReservations(reservations_t *reservations, clinics_t *clinics, humans_t *humans)
{
	cale_t calendar = {0};
	PrintFilteredReservations(reservations,clinics,humans,&calendar,TRUE);
}

/* Prints all of the reservations except canceled ones.*/
void
PrintReservations(reservations_t *reservations, clinics_t *clinics, humans_t *humans)
{
	cale_t calendar = {0};
	PrintFilteredReservations(reservations,clinics,humans,&calendar,FALSE);
}

/* Prints all of the non canceled reservations which is later than calendar->day */
void
PrintFutureReservations(reservations_t *reservations, clinics_t *clinics,
						humans_t *humans, cale_t *calendar)
{
	PrintFilteredReservations(reservations,clinics,humans,calendar,FALSE);
}



/* Add new clinic */
void
AddClinic(clinics_t *clinics)
{
	clin_t clin;
	
	++(clinics->idClinics);
	clin.id = clinics->idClinics;
	PrintHeader("Adding a New Clinic");
	ModifyClinic(&clin);
	clinics->arr[clinics->idClinics] = clin;
}

void
UpdateClinic(clin_t *clinic)
{
	PrintHeader("Updating the Clinic");
	ModifyClinic(clinic);
}

void
ModifyClinic(clin_t *clinic)
{	
	fprintf(outputFile,"%40s", "Name => ");
	GetString(clinic->name,SMALLSTRSIZE-1,inputFile);
	
	fprintf(outputFile,"%40s", "Examine Period (eg: 0.2 hour) => ");
	fscanf(inputFile,"%lf", &clinic->examinePeriod);

	do {
		fprintf(outputFile,"%40s", "Starting Hour => ");
		fscanf(inputFile,"%lf", &clinic->hourOfStarting);

		fprintf(outputFile,"%40s", "Finishing hour => ");
		fscanf(inputFile,"%lf", &clinic->hourOfFinishing);
		flushInputBuffer();
	
		/* Finishing hour must be after than starting hour */
		if(clinic->hourOfFinishing < clinic->hourOfStarting)
			PrintFlashMessage("Finishing hour should be after than Starting"
							   " hour");
	} while(clinic->hourOfFinishing <= clinic->hourOfStarting);
}

/* Update new human */
void
UpdateHuman(human_t *human)
{
	if(TRACE)
		fprintf(outputFile,"****Initializing function: UpdateHuman()\n");	
	
	fprintf(outputFile,"%40s", "Name => ");
	GetString(human->name,SMALLSTRSIZE-1,inputFile);

	do { /* get gender info until valid char is entered */
		fprintf(outputFile,"%40s", "Gender(F/M) => ");
		human->gender = toupper(fgetc(inputFile));
		flushInputBuffer();
	} while(human->gender != 'F' && human->gender != 'M');

	fprintf(outputFile,"%40s", "Address => ");
	GetString(human->address,SMALLSTRSIZE-1,inputFile);

	fprintf(outputFile,"%40s", "Tel => ");
	GetString(human->tel,SMALLSTRSIZE-1,inputFile);

	fprintf(outputFile,"%40s", "Extra Info => ");
	GetString(human->extraInfo,LONGSTRSIZE-1,inputFile);
	
	if(TRACE)
		fprintf(outputFile,"****Terminating function: UpdateHuman()\n");	
}

/* Add new human */
human_t
AddHuman()
{
	human_t human;
	if(TRACE)
		fprintf(outputFile,"****Initializing function: AddHuman()\n");	
	
	UpdateHuman(&human);
	
	if(TRACE)
		fprintf(outputFile,"****Terminating function: AddHuman()\n");
	
	return human;
}

/* Returns the number of ACTIVE doctors on specific clinic */
int
GetNumOfClinicDoctors(int clinicId, humans_t *humans)
{
	int	i,
		numOfDoctors = 0;

	if(TRACE)
		fprintf(outputFile,"GetNumOfClinicDoctors\t -  initialized.\n");

	for(i=1;i<=humans->idHumans;++i)
		if(humans->arr[i].hType == doctorEnum &&
		   humans->arr[i].type.doctor.clinicId == clinicId &&
		   humans->arr[i].type.doctor.active)
			++numOfDoctors;

	if(TRACE)
		fprintf(outputFile,"GetNumOfClinicDoctors\t -  terminated with %d.\n", 
	                numOfDoctors);

	return numOfDoctors;
}

/* Returns the number of reservations of the clinic on the day */
int
GetNumOfReservationsOnDay(int clinicId, int day, reservations_t *reservations)
{
	int	i,
		numOfReservations = 0;

	if(TRACE)
		fprintf(outputFile,"GetNumOfReservationsOnDay\t -  initialized.\n");

	for(i=0;i<reservations->idReservations;++i)
		if(reservations->arr[i].time.day == day &&
		   reservations->arr[i].clinicId == clinicId)
			++numOfReservations;

	if(TRACE)
		fprintf(outputFile,"GetNumOfReservationsOnDay\t - terminated.\n");

	return numOfReservations;
}

/* Returns the capacity of clinic on specific day */
int
GetClinicCapacity(clin_t *clinic, humans_t *humans)
{
		int	numOfDoctors,
			totalWorkingHours,
			capacity;
		
	numOfDoctors = GetNumOfClinicDoctors(clinic->id, humans);
	if(numOfDoctors < 1) {
		PrintFlashMessage("No doctor found in clinic");
		return 0;
	}
		
	totalWorkingHours = clinic->hourOfFinishing - clinic->hourOfStarting;
	capacity = (totalWorkingHours*numOfDoctors)/clinic->examinePeriod;
		
	if(TRACE)
		fprintf(outputFile,"Clinic Id=%d Capacity=%d Num Of Doctors=%d\n", clinic->id, capacity, numOfDoctors);
		
	return capacity;
}

/* Control clinic's capacity for a specific day and clinic. 
   Returns 1 if clinic has a room for a reservation
   0 for no room*/
int
ControlClinicCapacity(int day, clin_t *clinic, reservations_t *reservations, humans_t *humans)
{
	int	clinicCapacity,
		numOfReservations,
		status;

	if(TRACE)
		fprintf(outputFile,"ControlClinicCapacity \t -  initialized.\n");

	clinicCapacity = GetClinicCapacity(clinic,humans);
	numOfReservations = GetNumOfReservationsOnDay(clinic->id, day, reservations);
	status = (1 < (clinicCapacity - numOfReservations));

	if(TRACE)
		fprintf(outputFile,"ControlClinicCapacity \t -  terminated with"
				   "status code %d\n",status);

	return status;
}


/* returns an human id as a doctor id.
   id is selected according to number of doctors reservations on day
 */
int
AssignDoctorForReservation(reservations_t *reservations, int day, int clinicId, humans_t *humans)
{
	int	i,j, /*counters*/
		availableDocId = 0, /* Most available doctor's human.id */
		tempReservNumHolderPrev = 0, /* temporary reservation counter */
		tempReservNumHolderCurr = 0, /* temporary reservation counter */
		docId;

	/* scanning humans for doctors */
	for(i=1;i<=humans->idHumans;++i) {
		/* filtering doctors of preferred clinic */
		if(humans->arr[i].type.doctor.clinicId == clinicId) {
			docId = humans->arr[i].id;
			/*resetting temp counter */
			tempReservNumHolderCurr = 0;
		   
		   	/* scanning doctors */
			for(j=0;j<reservations->idReservations;++j) {
				if(reservations->arr[j].time.day == day &&
				   reservations->arr[j].clinicId == clinicId &&
				   reservations->arr[j].doctorId == docId) 
					++tempReservNumHolderCurr;
			   
			  	/* current doctor has less reservation */
				if(tempReservNumHolderCurr <= tempReservNumHolderPrev)
					availableDocId = docId;
			   
			  	 tempReservNumHolderPrev = tempReservNumHolderCurr;
			} /* 2nd for */
		} /* if */
	} /*1st for */
	return availableDocId;  
}

void
AddReservation(reservations_t *reservations, humans_t *humans, clinics_t *clinics,
			   cale_t *calendar)
{
	reser_t	reserv;

	ClearScreen();
	
	PrintHeader("ADDING RESERVATION");

	++(reservations->idReservations);
	reserv.id = reservations->idReservations;
	
	putc('\n',outputFile);
	PrintFlashMessage("SELECT CLINIC");
	PrintClinics(clinics);
	reserv.clinicId = GetUserInputInt("ID");

	do {
		do {
			putc('\n',outputFile);
			PrintFlashMessage("SELECT DATE");
			fprintf(outputFile,"%40s : %d\n","Current Day", calendar->day);
			reserv.time.day = GetUserInputInt("day");

			/* Check whether the day is past or not */
			if(reserv.time.day < calendar->day)
				PrintFlashMessage("You cannot select a previous day!");
		}while((reserv.time.day < calendar->day));

		/* check whether the day is full or not */
		if(!ControlClinicCapacity(reserv.time.day, &clinics->arr[reserv.clinicId],reservations,humans)) {
			PrintFlashMessage("This day is full. Please select"
							  " another day.");
		} 
	} while(!ControlClinicCapacity(reserv.time.day, &clinics->arr[reserv.clinicId],reservations,humans));
	
	reserv.doctorId = AssignDoctorForReservation(reservations, reserv.time.day, reserv.clinicId, humans);
		
	putc('\n',outputFile);
	PrintFlashMessage("SELECT PATIENT");
	PrintPatients(humans);
	reserv.patientId = GetUserInputInt("ID");
	reserv.status = NOTEXAMINED;
	reservations->arr[reservations->idReservations] = reserv;
}

void
ExamineReservation(reser_t *reservation, clinics_t *clinics, humans_t *humans)
{
	/* filtering examined and canceled reservations */
	if(reservation->status == NOTEXAMINED) {
		PrintReservation(reservation,clinics,humans);
		fprintf(outputFile,"%40s", "Enter the doctor notes for reservation =>");
		GetString(reservation->doctorNotes,LONGSTRSIZE-1,inputFile);
		reservation->status = EXAMINED;
	}
}

int
ExamineDay(cale_t *time, reservations_t *reservations, clinics_t *clinics, humans_t *humans)
{
	int i;
	
	for(i=1; i<=reservations->idReservations;++i)
		if(reservations->arr[i].time.day == time->day)
			ExamineReservation(&reservations->arr[i],clinics,humans);
	return 1;
}

/*Changes the reservation status to canceled*/
void
CancelReservation(reser_t *reservation)
{
	reservation->status = CANCELED;
}

/* Cancels all waiting reservations between day1 and day2 */
void
CancelReservations(reservations_t *reservations, int day1, int day2)
{
	int i;
	
	for(i=1;i<=reservations->idReservations;++i) {
		if(reservations->arr[i].time.day >= day1 &&
		   reservations->arr[i].time.day < day2 &&
		   reservations->arr[i].status == NOTEXAMINED)
				reservations->arr[i].status = CANCELED;
	}
}

/* Add new doctor */
void
ModifyDoctor(human_t *doctor, clinics_t *clinics)
{
	UpdateHuman(doctor);

	doctor->hType = doctorEnum;
	doctor->type.doctor.active = 1;

	putc('\n',outputFile);
	PrintClinics(clinics);
	fprintf(outputFile,"\n\nPlease enter the clinic id for the doctor => ");
	fscanf(inputFile,"%d", &doctor->type.doctor.clinicId);
	flushInputBuffer();
}

void
UpdateDoctor(human_t *doctor, clinics_t *clinics)
{
	ClearScreen();
	PrintHeader("Updating a the Doctor");
	ModifyDoctor(doctor, clinics);
}

/* Add new doctor */
void
AddDoctor(humans_t *humans, clinics_t *clinics)
{
	ClearScreen();
	PrintHeader("Adding a New Doctor");
	++(humans->idHumans);
	humans->arr[humans->idHumans].id = humans->idHumans;
	ModifyDoctor(&humans->arr[humans->idHumans], clinics);
}

/* Changes the active flag to 0 of the doctor */
void
DeleteDoctor(human_t *doctor)
{
	doctor->type.doctor.active = 0;
}

/* Add new doctor */
void
ModifyPatient(human_t *patient)
{
	UpdateHuman(patient);

	patient->hType = patientEnum;
	fprintf(outputFile,"%40s","Age => ");
	fscanf(inputFile,"%d", &patient->type.patient.age);
	flushInputBuffer();

	fprintf(outputFile,"%40s", "Occupation =>");
	GetString(patient->type.patient.occupation,SMALLSTRSIZE-1,inputFile);

	fprintf(outputFile,"%40s", "Medical History =>");
	GetString(patient->type.patient.medicalHist,LONGSTRSIZE-1,inputFile);
	putc('\n',outputFile);	
}

void
UpdatePatient(human_t *patient)
{
	ClearScreen();
	PrintHeader("Updating a the Patient");
	ModifyPatient(patient);
}

/* Add new doctor */
void
AddPatient(humans_t *humans)
{
	ClearScreen();
	PrintHeader("Adding a New Patient");
	++(humans->idHumans);
	humans->arr[humans->idHumans].id = humans->idHumans;
	ModifyPatient(&humans->arr[humans->idHumans]);
}

int
GetDataFromFile(FILE *file, void *data, int sizeOfData, int *numOfElements)
{
	int	status = 0;
	
	*numOfElements = 0;

	if(file == NULL) {
		if(TRACE)
			fprintf(outputFile,"Couldn't open file\n");
		return 0;
	}

	do
	{
		++(*numOfElements); /* all Ids start from 1 */

		/* a bit of math to calculate the memory location
		 * (thanks to Erdogan Sevilgen :)*/ 
		status = fread((char *)data + (*numOfElements)*sizeOfData, 
						sizeOfData,
						1,
						file);
	} while(status == 1);

	--(*numOfElements);
	return 1;
}


char
GetUserInputChar(void)
{
	char userInput;
	userInput = toupper(fgetc(inputFile));
	flushInputBuffer();
	return userInput;
}

int
GetUserInputInt(char *message)
{
	int userInput;
	fprintf(outputFile,"Enter %s => ", message);
	fscanf(inputFile,"%d",&userInput);
	flushInputBuffer();
	return userInput;
}

char
DisplayMainMenu(cale_t *calendar)
{
	char userInput;

	PrintHeaderWithDay("CHOOSE AN OPERATION",calendar->day);
	fprintf(outputFile,"%34c%-45s%c",' ',"[C] CREATE",'\n');
	fprintf(outputFile,"%34c%-45s%c",' ',"[L] LIST",'\n');
	fprintf(outputFile,"%34c%-45s%c",' ',"[U] UPDATE",'\n');
	fprintf(outputFile,"%34c%-45s%c",' ',"[D] DELETE",'\n');
	fprintf(outputFile,"%34c%-45s%c",' ',"[E] EXPLORE",'\n');
	fprintf(outputFile,"%34c%-45s%c",' ',"[N] GO TO NEXT DAY",'\n');
	fprintf(outputFile,"%34c%-45s%c",' ',"[T] EXAMINE TODAY'S RESERVATIONS",'\n');
	fprintf(outputFile,"%34c%-45s%c",' ',"[G] GO TO DAY",'\n');
	fprintf(outputFile,"%34c%-45s%c",' ',"[Q] QUIT",'\n');

	userInput = GetUserInputChar();
	return userInput;
}



void
DisplayCommonMenu(void)
{
	fprintf(outputFile,"%34c%-45s%c",' ',"[C] Clinic",'\n');
	fprintf(outputFile,"%34c%-45s%c",' ',"[D] Doctor",'\n');
	fprintf(outputFile,"%34c%-45s%c",' ',"[P] Patient",'\n');
	fprintf(outputFile,"%34c%-45s%c",' ',"[R] Reservation",'\n');
	fprintf(outputFile,"%34c%-45s%c",' ',"[M] Return to Main Menu",'\n');
	fprintf(outputFile,"%34c%-45s%c",' ',"[Q] Quit",'\n');
}

char
DisplayListMenu(cale_t *calendar)
{
	PrintHeaderWithDay("LIST",calendar->day);
	DisplayCommonMenu();
	return GetUserInputChar();
}

char
DisplayCreateMenu(cale_t *calendar)
{
	PrintHeaderWithDay("CREATE",calendar->day);
	DisplayCommonMenu();
	return GetUserInputChar();
}

char
DisplayUpdateMenu(cale_t *calendar)
{
	PrintHeaderWithDay("UPDATE",calendar->day);
	fprintf(outputFile,"%34c%-45s%c",' ',"[C] Clinic",'\n');
	fprintf(outputFile,"%34c%-45s%c",' ',"[D] Doctor",'\n');
	fprintf(outputFile,"%34c%-45s%c",' ',"[P] Patient",'\n');
	fprintf(outputFile,"%34c%-45s%c",' ',"[M] Return to Main Menu",'\n');
	fprintf(outputFile,"%34c%-45s%c",' ',"[Q] Quit",'\n');
	return GetUserInputChar();
}

char
DisplayExploreMenu(cale_t *calendar)
{
	PrintHeaderWithDay("EXPLORE",calendar->day);
	DisplayCommonMenu();
	return GetUserInputChar();
}

char
DisplayDeleteMenu(cale_t *calendar)
{
	PrintHeaderWithDay("DELETE MENU",calendar->day);
	fprintf(outputFile,"%29s%-50s%c","","[R] Cancel Reservation",'\n');
	fprintf(outputFile,"%29s%-50s%c","","[D] Delete Doctor",'\n');
	fprintf(outputFile,"%29s%-50s%c","","[M] Return to Main Menu",'\n');
	fprintf(outputFile,"%29s%-50s%c","","[Q] Quit",'\n');
	return GetUserInputChar();
}

void
SaveDataToFiles(humans_t *humans,
		clinics_t *clinics,
		reservations_t *reservations,
		cale_t *calendar)
{
	int status;
	FILE	*reserFile,
		*clinFile,
		*humanFile,
		*caleFile;

	/* opening binary files in writable mode */
	reserFile = fopen(RESEFILE,"wb");
	clinFile  = fopen(CLINFILE,"wb");
	humanFile = fopen(HUMANFILE,"wb");
	caleFile  = fopen(CALEFILE,"wb");

	status = fwrite(calendar, sizeof(cale_t), 1, caleFile);
	if(TRACE)
		if(status == 1)
			PrintFlashMessage("Calendar is saved to file");
		else
			PrintFlashMessage("Calendar cannot be saved.");

	status = fwrite(&humans->arr[1], sizeof(human_t), humans->idHumans, humanFile);
	if(TRACE)
		if(status == humans->idHumans)
			PrintFlashMessage("Humans are saved to file");
		else
			PrintFlashMessage("Humans cannot be saved.");
	
	status = fwrite(&clinics->arr[1], sizeof(clin_t), clinics->idClinics, clinFile);
	if(TRACE)
		if(status == clinics->idClinics)
			PrintFlashMessage("Clinics are saved to file");
		else
			PrintFlashMessage("Clinics cannot be saved.");
	
	status = fwrite(&reservations->arr[1], sizeof(reser_t), reservations->idReservations, reserFile);
	if(TRACE)
		if(status == reservations->idReservations)
			PrintFlashMessage("Reservations are saved to file");
		else
			PrintFlashMessage("Reservations cannot be saved.");

	/* closing database files */
	fclose(reserFile);
	fclose(clinFile);
	fclose(humanFile);
	fclose(caleFile);
}

void
InitializeData(humans_t *humans, clinics_t *clinics, 
               reservations_t *reservations, cale_t *calendar)
{
	int	i,
		doctorNum = 0, patientNum = 0;
	FILE	*reserFile,
		*clinFile,
		*humanFile,
		*caleFile;
	
	humans->idHumans = 0;
	clinics->idClinics = 0;
	reservations->idReservations = 0;
	calendar->day = 1;
		
	if(TRACE)
		fprintf(outputFile,"****Initializing function: InitializeData()\n");	
	
		/* Getting calendar of last session */
	caleFile  = fopen(CALEFILE,"rb");
	if(caleFile != NULL) {
		fread(calendar,sizeof(cale_t),1,caleFile);
		fclose(caleFile);
	}
	else
		PrintFlashMessage("Could not open Calendar file.");
		
		
		/* Reading the clinics from the last session*/
	clinFile  = fopen(CLINFILE,"rb");
	if(clinFile != NULL) {
		GetDataFromFile(clinFile,clinics->arr,sizeof(clin_t),&clinics->idClinics);
		fclose(clinFile);
	}
	else
		PrintFlashMessage("Could not open Clinics file.");
	if(TRACE)
		fprintf(outputFile,"%d clinic(s) read from file\n", clinics->idClinics);
	/* No clinic found. Adding a new one */
	if(clinics->idClinics < 1) {
		AddClinic(clinics);
		ClearScreen();
	}
	else if(TRACE)
		PrintFlashMessage("Clinics are successfully imported!");

	
	/* Reading the reservations from the last session*/
	reserFile = fopen(RESEFILE,"rb");
	if(reserFile != NULL) {
		GetDataFromFile(reserFile,reservations->arr, sizeof(reser_t),&reservations->idReservations);
		fclose(reserFile);
	}
	else
		PrintFlashMessage("Could not open Reservations file.");
	if(TRACE)
		fprintf(outputFile,"%d reservation(s) read from file\n", reservations->idReservations);
	/* No reservation found. Adding a new one */
	if(reservations->idReservations > 0 && TRACE)
		PrintFlashMessage("Reservations are successfully imported!");

	
	/* Reading the humans from the last session*/
	humanFile = fopen(HUMANFILE,"rb");
	if(humanFile != NULL) {
		GetDataFromFile(humanFile,humans->arr,sizeof(human_t),&humans->idHumans);
		fclose(humanFile);
	}
	else
		PrintFlashMessage("Could not open Human file.");
	if(TRACE)
		fprintf(outputFile,"%d human(s) read from file\n", humans->idHumans);
	/* No human found. Adding a doctor and a patient */
	if(humans->idHumans < 1) {
		AddDoctor(humans, clinics);
		ClearScreen();
		AddPatient(humans);
		ClearScreen();
	}
	else {
		/* Looking for doctors and patients */
		for(i=1;i<=humans->idHumans;++i) {
			/* If current human is doctor, increase doctorNum */
			if(humans->arr[i].hType == doctorEnum)
				++doctorNum;
			/* If current human is patient, increase patientNum */
			else if(humans->arr[i].hType == patientEnum)
				++patientNum;
		}
		
		/* No doctor found. Adding a doctor */
		if(doctorNum < 1) {
			AddDoctor(humans, clinics);
			ClearScreen();
		}
		else if(TRACE)
			PrintFlashMessage("Doctors are successfully imported!");
		
		/* No patient found. Adding a patient */
		if(patientNum < 1) {
			AddPatient(humans);
			ClearScreen();
		}
		else if(TRACE)
			PrintFlashMessage("Patients are successfully imported!");
	}
	
	if(TRACE)
		fprintf(outputFile,"****Terminating function: InitializeData()\n");
}

void
TxtOutput(clinics_t *clinics, humans_t *humans, reservations_t *reservations)
{
	FILE *tempOutput;
	
	tempOutput = outputFile;
	
	outputFile = fopen(CLINTXTFILE,"w");
	PrintClinics(clinics);
	fclose(outputFile);

	outputFile = fopen(HUMANTXTFILE,"w");
	PrintAllDoctors(humans,clinics);
	fputc('\n',outputFile);
	PrintPatients(humans);
	fclose(outputFile);
	
	outputFile = fopen(RESETXTFILE,"w");
	PrintAllReservations(reservations,clinics,humans);
	fclose(outputFile);
	
	outputFile = tempOutput;
}
#endif
