/*############################################################################*/
/* HW01_111044029_PART_2.c                                                    */
/* ----------------------                                                     */
/* Created on 03.03.2012 by Furkan Tektas                                     */
/*                                                                            */
/* Notes                                                                      */
/* ­­­­­-----                                                                      */
/* This program needs input.txt file consists of 5 lines, each of which has   */
/* 3 different double values and will create output.txt                       */
/* verbose.                                                                   */
/* ­­­­­                                                                           */
/* Description                                                                */
/* ----------------------                                                     */
/* Calculates the total cost of house with given tax rate,                    */
/* fuel costs and initial costs in input.txt for five years                   */
/* and prints the calculated five years costs with input variables            */
/*                                                                            */
/*############################################################################*/


/*############################################################################*/
/*                                  Includes                                  */
/*############################################################################*/
#include <stdio.h>
#include <math.h>

/*­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­----------------------------------------------------------------------------*/
/*                            Function Prototypes                             */
/*­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­----------------------------------------------------------------------------*/
/*############################################################################*/
/*                                                                            */
/* void CalculateTotalCost(double  initialCost, double  taxRate,              */
/* double  fuelCostPerYear)                                                   */
/* ­­­­­­­­­­­                                                                           */
/*                                                                            */
/*      initialCost   ­   5 year term initial cost                             */
/*      taxRate   ­       tax rate for initial cost per year                   */
/*      fuelCostPerYear ­ fuel cost with taxes per year (tax-free)             */
/*                                                                            */
/* Return                                                                     */
/* ­­­­­­                                                                           */
/*     (double) total Cost calculation                                        */
/*                                                                            */
/* Description                                                                */
/* ­­­­­­­­­­­                                                                           */
/*     This function takes the fuel cost for year and initial cost            */
/*     for five years and calculates the total cost by adding tax             */
/*     for initial cost. Then returns the sum of them.                        */
/*                                                                            */
/*############################################################################*/
double CalculateTotalCost(double  initialCost, double  taxRate, 
                          double  fuelCostPerYear);

/*############################################################################*/
/*                                                                            */
/* void WriteToFile(FILE *outputFile, double  initialCost,                    */
/* double  taxRate, double  fuelCostPerYear, double totalCost)                */
/* ­­­­­­­­­­­                                                                           */
/*                                                                            */
/*      *outputFile   ­   output file pointer                                  */
/*      initialCost      5 year term initial cost                             */
/*      taxRate   ­       tax rate for initial cost per year                   */
/*      fuelCostPerYear ­ fuel cost with taxes per year (tax-free)             */
/*      totalCost        total cost with tax                                  */
/*                                                                            */
/* Return                                                                     */
/* ­­­­­­                                                                           */
/*     Nothing.                                                               */
/*                                                                            */
/* Description                                                                */
/* ­­­­­­­­­­­                                                                           */
/*     This function takes the pointer of the output file,                    */
/*     fuel cost for year,  initial cost and total cost for five              */
/*     years and append them to the output file.                              */
/*                                                                            */
/*############################################################################*/
void WriteToFile(FILE *outputFile, double  initialCost, double  taxRate, 
                 double  fuelCostPerYear, double totalCost);

/*############################################################################*/
/*  int main()                                                                */
/*  ----------                                                                */  
/*  Return                                                                    */
/*  ----------                                                                */
/*       0 on success                                                         */
/*############################################################################*/

int
main(void)
{
    double  initialCost,      /* initial cost for five years */
            taxRate,          /* tax rate for five years term */
            fuelCostPerYear,  /* fuel cost per year */
            totalCost;        /* five years total cost of house */

    FILE  *inputFile,         /* input data */
          *outputFile;        /* output data */


    /* Defining input and output files */
    inputFile = fopen("input.txt","r");
    outputFile = fopen("output.txt","w");

    /* Writing headers to the output file */
    fprintf(outputFile,
            "Initial House Cost     Annual Fuel Cost     Tax Rate     Total Cost\n");


    /*######################### 1ST FIVE YEAR TERM ###########################*/
    
    /* Getting the costs and tax rate for the five year term */
    fscanf(inputFile,"%lf %lf %lf",&initialCost,&fuelCostPerYear,&taxRate);
    
    /* Calculating the total cost and assigning the corresponding variable */
    totalCost = CalculateTotalCost(initialCost, fuelCostPerYear, taxRate);

    /* Appending all values to the output file. */
    WriteToFile(outputFile, initialCost, fuelCostPerYear, taxRate, totalCost);


    /*######################### 2ND FIVE YEAR TERM ###########################*/
    
    /* Getting the costs and tax rate for the five year term */
    fscanf(inputFile,"%lf %lf %lf",&initialCost,&fuelCostPerYear,&taxRate);
    
    /* Calculating the total cost and assigning the corresponding variable */
    totalCost = CalculateTotalCost(initialCost, fuelCostPerYear, taxRate);

    /* Appending all values to the output file. */
    WriteToFile(outputFile, initialCost, fuelCostPerYear, taxRate, totalCost);


    /*######################### 3RD FIVE YEAR TERM ###########################*/
    
    /* Getting the costs and tax rate for the five year term */
    fscanf(inputFile,"%lf %lf %lf",&initialCost,&fuelCostPerYear,&taxRate);
    
    /* Calculating the total cost and assigning the corresponding variable */
    totalCost = CalculateTotalCost(initialCost, fuelCostPerYear, taxRate);

    /* Appending all values to the output file. */
    WriteToFile(outputFile, initialCost, fuelCostPerYear, taxRate, totalCost);


    /*######################### 4TH FIVE YEAR TERM ###########################*/
    
    /* Getting the costs and tax rate for the five year term */
    fscanf(inputFile,"%lf %lf %lf",&initialCost,&fuelCostPerYear,&taxRate);
    
    /* Calculating the total cost and assigning the corresponding variable */
    totalCost = CalculateTotalCost(initialCost, fuelCostPerYear, taxRate);

    /* Appending all values to the output file. */
    WriteToFile(outputFile, initialCost, fuelCostPerYear, taxRate, totalCost);


    /*######################### 5TH FIVE YEAR TERM ###########################*/
    
    /* Getting the costs and tax rate for the five year term */
    fscanf(inputFile,"%lf %lf %lf",&initialCost,&fuelCostPerYear,&taxRate);
    
    /* Calculating the total cost and assigning the corresponding variable */
    totalCost = CalculateTotalCost(initialCost, fuelCostPerYear, taxRate);

    /* Appending all values to the output file. */
    WriteToFile(outputFile, initialCost, fuelCostPerYear, taxRate, totalCost);


    /* Closing input and output files */
    fclose(inputFile);
    fclose(outputFile);

    return 0;
}

/*----------------------------------------------------------------------------­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­*/
/*                            Function Implementations                        */
/*----------------------------------------------------------------------------­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­*/
    
/* Function CalculateTotalCost                                                */
/* ­­­­­­­­­­­---------------------------                                                */
/*     This function takes the pointer of the output file,                    */
/*     fuel cost for year,  initial cost and total cost for five              */
/*     years and append them to the output file.                              */
/*                                                                            */
double
CalculateTotalCost(double  initialCost, double  fuelCostPerYear, double  taxRate)
{
    /* Calculating and returning total cost. */
    return ( ( initialCost * 5.0 * taxRate ) + initialCost + (5.0 * fuelCostPerYear) );
}


/* Function WriteToFile                                                       */
/* ­­­­­­­­­­­---------------------------                                                */
/*     This function takes the pointer of the output file,                    */
/*     fuel cost for year,  initial cost and total cost for five              */
/*     years and append them to the output file.                              */
/*                                                                            */
void
WriteToFile(FILE *outputFile, double  initialCost, double  fuelCostPerYear, 
            double  taxRate, double totalCost)
{
    fprintf(outputFile,"%-18.0f     %-16.0f     %-8.3f     %-10.0f\n", 
            initialCost, fuelCostPerYear, taxRate, totalCost);
}

/*############################################################################*/
/*                        End of HW01_111044029_PART_2.c                      */
/*############################################################################*/
