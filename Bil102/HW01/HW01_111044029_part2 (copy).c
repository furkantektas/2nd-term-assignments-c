/*#################################################################*/
/* HW01_111044029_PART_2.c                                         */
/* ----------------------                                          */
/* Created on 23.02.2012 by Furkan Tektas                          */
/*                                                                 */
/* Description                                                     */
/* ----------------------                                          */
/* Calculates the total cost of house with given tax rate,         */
/* fuel costs and initial costs in input.txt for five years        */
/* and prints the calculated five years costs with input variables */
/*                                                                 */
/*#################################################################*/


/*#################################################################*/
/*                            Includes                             */
/*#################################################################*/
#include <stdio.h>
#include <math.h>


void calculateTotalCost(double initialCost, double fuelCostPerYear,
                        double taxRate, FILE * outputFile);

/*#################################################################*/
/*  int main()                                                     */
/*  ----------                                                     */  
/*  Return                                                         */
/*  ----------                                                     */
/*       0 on success                                              */
/*#################################################################*/

int
main(void)
{
    FILE *  inputFile,        /* input data */
         *  outputFile;       /* output data */

    double  initialCost,      /* initial cost for five years */
            taxRate,          /* tax rate for five years term */
            fuelCostPerYear;  /* fuel cost per year */

    /* Defining input and output files */
    inputFile = fopen("input.txt","r");
    outputFile = fopen("output.txt","w");
    
    /* Writing headers to the output file */
    fprintf(outputFile,"Initial House Cost     Annual Fuel Cost     Tax Rate     Total Cost\n");


    /*######################## 1ST FIVE YEAR TERM ##########################*/
    /* Getting the cost and tax rate for 1st five year term */
    fscanf(inputFile,"%lf %lf %lf",&initialCost,&fuelCostPerYear,&taxRate);

    /* Calculate the total cost and write variables to the output file */
    calculateTotalCost(initialCost,fuelCostPerYear,taxRate, outputFile);


    /*######################## 2ND FIVE YEAR TERM ##########################*/
    /* Getting the cost and tax rate for 1st five year term */
    fscanf(inputFile,"%lf %lf %lf",&initialCost,&fuelCostPerYear,&taxRate);

    /* Calculate the total cost and write variables to the output file */
    calculateTotalCost(initialCost,fuelCostPerYear,taxRate, outputFile);


    /*######################## 3RD FIVE YEAR TERM ##########################*/
    /* Getting the cost and tax rate for 1st five year term */
    fscanf(inputFile,"%lf %lf %lf",&initialCost,&fuelCostPerYear,&taxRate);

    /* Calculate the total cost and write variables to the output file */
    calculateTotalCost(initialCost,fuelCostPerYear,taxRate, outputFile);


    /*######################## 4TH FIVE YEAR TERM ##########################*/
    /* Getting the cost and tax rate for 1st five year term */
    fscanf(inputFile,"%lf %lf %lf",&initialCost,&fuelCostPerYear,&taxRate);

    /* Calculate the total cost and write variables to the output file */
    calculateTotalCost(initialCost,fuelCostPerYear,taxRate, outputFile);


    /*######################## 5TH FIVE YEAR TERM ##########################*/
    /* Getting the cost and tax rate for 1st five year term */
    fscanf(inputFile,"%lf %lf %lf",&initialCost,&fuelCostPerYear,&taxRate);

    /* Calculate the total cost and write variables to the output file */
    calculateTotalCost(initialCost,fuelCostPerYear,taxRate, outputFile);

    /* Closing input and output files */
    fclose(inputFile);
    fclose(outputFile);

    return 0;
}

/*######################################################################*/
/*  void calculateTotalCost(double initialCost, double fuelCostPerYear, */
/*                          double taxRate, FILE * outputFile)          */
/*  ----------                                                          */
/*  Return                                                              */
/*  ----------                                                          */
/*       0 on success                                                   */
/*######################################################################*/

void
calculateTotalCost(double initialCost, double fuelCostPerYear, 
                   double taxRate, FILE * outputFile)
{
    double totalCost; /* five years total cost of house */

    /* Calculating totalCost.                              */
    /* Note: 1.0 + taxRate is the sum of cost and its tax  */
    /*totalCost = initialCost * (1.0 + taxRate) + 
                ((fuelCostPerYear * 5.0) * (1.0 + taxRate));*/
    totalCost = ( initialCost * 5.0 * taxRate ) + initialCost + (5.0 * fuelCostPerYear);

    /* Printing the input values and result to the output file. */
    fprintf(outputFile,"%18.0f     %16.0f     %8.3f     %10.0f\n", 
            initialCost, fuelCostPerYear, taxRate, totalCost);
}
