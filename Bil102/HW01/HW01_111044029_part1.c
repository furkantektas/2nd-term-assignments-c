/*############################################################################*/
/* HW01_111044029_PART_1.c                                                    */
/* ----------------------                                                     */
/* Created on 03.03.2012 by Furkan Tektas                                     */
/*                                                                            */
/* Description                                                                */
/* ----------------------                                                     */
/* Calculates the given function in hw01.pdf with user                        */
/* input values.                                                              */
/*                                                                            */
/*############################################################################*/


/*############################################################################*/
/*                            Includes                                        */
/*############################################################################*/
#include <stdio.h>
#include <math.h>


/*############################################################################*/
/*  int main()                                                                */
/*  ----------                                                                */  
/*  Return                                                                    */
/*  ----------                                                                */
/*       0 on success                                                         */
/*############################################################################*/

int
main(void)
{

    double  inputZ, /* user given input */
            valueN, /* second variable of function */
            result; /* calculated value of the function */

    /* Prompt and get inputZ value */
    printf("Enter the z: \t");
    scanf("%lf",&inputZ);

    /* Assign value 1 to the valueN */
    valueN = 1.0;
    /* assign the calculated value of the formula for user given z and valueN */
    result = sqrt(inputZ * ( ( 1.0 -( (valueN + 1.0) * pow(inputZ,valueN) )
                  + valueN * pow(inputZ, (valueN + 1.0) ) )
                  / pow( (1.0 -inputZ), 2.0 ) )
                  + exp(-inputZ * valueN) );
    /* Print the result */
    printf("\tf(%.0f,%.0f)\t= %-11f\n", inputZ,valueN,result);


    /* Assign value 2 to the valueN */
    valueN = 2.0;
    /* assign the calculated value of the formula for user given z and valueN */
    result = sqrt(inputZ * ( ( 1.0 -( (valueN + 1.0) * pow(inputZ,valueN) )
                  + valueN * pow(inputZ, (valueN + 1.0) ) )
                  / pow( (1.0 -inputZ), 2.0 ) )
                  + exp(-inputZ * valueN) );
    /* Print the result */
    printf("\tf(%.0f,%.0f)\t= %-11f\n", inputZ,valueN,result);


    /* Assign value 3 to the valueN */
    valueN = 3.0;
    /* assign the calculated value of the formula for user given z and valueN */
    result = sqrt(inputZ * ( ( 1.0 -( (valueN + 1.0) * pow(inputZ,valueN) )
                  + valueN * pow(inputZ, (valueN + 1.0) ) )
                  / pow( (1.0 -inputZ), 2.0 ) )
                  + exp(-inputZ * valueN) );
    /* Print the result */
    printf("\tf(%.0f,%.0f)\t= %-11f\n", inputZ,valueN,result);


    /* Assign value 4 to the valueN */
    valueN = 4.0;
    /* assign the calculated value of the formula for user given z and valueN */
    result = sqrt(inputZ * ( ( 1.0 -( (valueN + 1.0) * pow(inputZ,valueN) )
                  + valueN * pow(inputZ, (valueN + 1.0) ) )
                  / pow( (1.0 -inputZ), 2.0 ) )
                  + exp(-inputZ * valueN) );
    /* Print the result */
    printf("\tf(%.0f,%.0f)\t= %-11f\n", inputZ,valueN,result);


    /* Assign value 5 to the valueN */
    valueN = 5.0;
    /* assign the calculated value of the formula for user given z and valueN */
    result = sqrt(inputZ * ( ( 1.0 -( (valueN + 1.0) * pow(inputZ,valueN) )
                  + valueN * pow(inputZ, (valueN + 1.0) ) )
                  / pow( (1.0 -inputZ), 2.0 ) )
                  + exp(-inputZ * valueN) );
    /* Print the result */
    printf("\tf(%.0f,%.0f)\t= %-11f\n", inputZ,valueN,result);


    /* Assign value 6 to the valueN */
    valueN = 6.0;
    /* assign the calculated value of the formula for user given z and valueN */
    result = sqrt(inputZ * ( ( 1.0 -( (valueN + 1.0) * pow(inputZ,valueN) )
                  + valueN * pow(inputZ, (valueN + 1.0) ) )
                  / pow( (1.0 -inputZ), 2.0 ) )
                  + exp(-inputZ * valueN) );
    /* Print the result */
    printf("\tf(%.0f,%.0f)\t= %-11f\n", inputZ,valueN,result);


    /* Assign value 7 to the valueN */
    valueN = 7.0;
    /* assign the calculated value of the formula for user given z and valueN */
    result = sqrt(inputZ * ( ( 1.0 -( (valueN + 1.0) * pow(inputZ,valueN) )
                  + valueN * pow(inputZ, (valueN + 1.0) ) )
                  / pow( (1.0 -inputZ), 2.0 ) )
                  + exp(-inputZ * valueN) );
    /* Print the result */
    printf("\tf(%.0f,%.0f)\t= %-11f\n", inputZ,valueN,result);


    /* Assign value 8 to the valueN */
    valueN = 8.0;
    /* assign the calculated value of the formula for user given z and valueN */
    result = sqrt(inputZ * ( ( 1.0 -( (valueN + 1.0) * pow(inputZ,valueN) )
                  + valueN * pow(inputZ, (valueN + 1.0) ) )
                  / pow( (1.0 -inputZ), 2.0 ) )
                  + exp(-inputZ * valueN) );
    /* Print the result */
    printf("\tf(%.0f,%.0f)\t= %-11f\n", inputZ,valueN,result);


    /* Assign value 9 to the valueN */
    valueN = 9.0;
    /* assign the calculated value of the formula for user given z and valueN */
    result = sqrt(inputZ * ( ( 1.0 -( (valueN + 1.0) * pow(inputZ,valueN) )
                  + valueN * pow(inputZ, (valueN + 1.0) ) )
                  / pow( (1.0 -inputZ), 2.0 ) )
                  + exp(-inputZ * valueN) );
    /* Print the result */
    printf("\tf(%.0f,%.0f)\t= %-11f\n", inputZ,valueN,result);


    /* Assign value 10 to the valueN */
    valueN = 10.0;
    /* assign the calculated value of the formula for user given z and valueN */
    result = sqrt(inputZ * ( ( 1.0 -( (valueN + 1.0) * pow(inputZ,valueN) )
                  + valueN * pow(inputZ, (valueN + 1.0) ) )
                  / pow( (1.0 -inputZ), 2.0 ) )
                  + exp(-inputZ * valueN) );
    /* Print the result */
    printf("\tf(%.0f,%.0f)\t= %-11f\n", inputZ,valueN,result);

    return(0);
}

/*############################################################################*/
/*                        End of HW01_111044029_PART_1.c                      */
/*############################################################################*/
