/*#################################################################*/
/* HW01_111044029_PART_2.c                                         */
/* ----------------------                                          */
/* Created on 23.02.2012 by Furkan Tektas                          */
/*                                                                 */
/* Description                                                     */
/* ----------------------                                          */
/* Calculates the total cost of house with given tax rate,         */
/* fuel costs and initial costs in input.txt for five years        */
/* and prints the calculated five years costs with input variables */
/*                                                                 */
/*#################################################################*/


/*#################################################################*/
/*                            Includes                             */
/*#################################################################*/
#include <stdio.h>
#include <math.h>


void calculateTotalCost(FILE * inputFile, FILE * outputFile);

/*#################################################################*/
/*  int main()                                                     */
/*  ----------                                                     */  
/*  Return                                                         */
/*  ----------                                                     */
/*       0 on success                                              */
/*#################################################################*/

int
main(void)
{
    FILE *  inputFile,        /* input data */
         *  outputFile;       /* output data */


    /* Defining input and output files */
    inputFile = fopen("input.txt","r");
    outputFile = fopen("output.txt","w");
    
    /* Writing headers to the output file */
    fprintf(outputFile,"Initial House Cost     Annual Fuel Cost     Tax Rate     Total Cost\n");


    /*######################## 1ST FIVE YEAR TERM ##########################*/  
    /* Calling the function which get input variables from input,       */
    /* calculates the total cost and write variables to the output file */
    calculateTotalCost(inputFile, outputFile);


    /*######################## 2ND FIVE YEAR TERM ##########################*/
    /* Calling the function which get input variables from input,
    /* calculates the total cost and write variables to the output file */
    calculateTotalCost(inputFile, outputFile);


    /*######################## 3RD FIVE YEAR TERM ##########################*/
    /* Calling the function which get input variables from input,
    /* calculates the total cost and write variables to the output file */
    calculateTotalCost(inputFile, outputFile);


    /*######################## 4TH FIVE YEAR TERM ##########################*/
    /* Calling the function which get input variables from input,
    /* calculates the total cost and write variables to the output file */
    calculateTotalCost(inputFile, outputFile);


    /*######################## 5TH FIVE YEAR TERM ##########################*/
    /* Calling the function which get input variables from input,
    /* calculates the total cost and write variables to the output file */
    calculateTotalCost(inputFile, outputFile);
    
    /* Closing input and output files */
    fclose(inputFile);
    fclose(outputFile);

    return 0;
}

/*######################################################################*/
/*  void calculateTotalCost(FILE * inputFile, FILE * outputFile)        */
/*  ----------                                                          */
/*  Return                                                              */
/*  ----------                                                          */
/*       nothing                                                        */
/*######################################################################*/

void
calculateTotalCost(FILE * inputFile, FILE * outputFile)
{
    double  initialCost,      /* initial cost for five years */
            taxRate,          /* tax rate for five years term */
            fuelCostPerYear,  /* fuel cost per year */
            totalCost;        /* five years total cost of house */

    /* Getting the cost and tax rate for the five year term */
    fscanf(inputFile,"%lf %lf %lf",&initialCost,&fuelCostPerYear,&taxRate);

    /* Calculating totalCost. */
    totalCost = ( initialCost * 5.0 * taxRate ) + initialCost + (5.0 * fuelCostPerYear);

    /* Printing the input values and result to the output file. */
    fprintf(outputFile,"%18.0f     %16.0f     %8.3f     %10.0f\n", 
            initialCost, fuelCostPerYear, taxRate, totalCost);
}
