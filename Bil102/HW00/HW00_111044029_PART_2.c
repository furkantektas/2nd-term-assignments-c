/*################################################################*/
/* HW00_111044029_PART_2.c                                        */
/* ----------------------                                         */
/* Created on 23.02.2012 by Furkan Tektas                         */
/*                                                                */
/* Description                                                    */
/* ----------------------                                         */
/* Calculates the area of a rectangle by given edge lengths.      */
/*                                                                */
/*################################################################*/


/*################################################################*/
/*                            Includes                            */
/*################################################################*/
#include <stdio.h>


/*################################################################*/
/*  int main()                                                    */
/*  ----------                                                    */  
/*  Return                                                        */
/*  ----------                                                    */
/*       0 on success                                             */
/*################################################################*/

int
main(void)
{

	int shortEdgeLength, /* short edge length of the rectangle */
	    longEdgeLength,  /* long edge length of the rectangle */
	    area;            /* area of the rectangle */

	/* Get short edge length */
	printf("\tShort edge of the rectangle:\t");
	scanf("%d",&shortEdgeLength);

	/* Get long edge length */
	printf("\tLong edge of the rectangle:\t");
	scanf("%d",&longEdgeLength);

	/* Calculate the area */
	area = shortEdgeLength * longEdgeLength;

	/* Printing the result */
	printf("\tArea of the %dx%d rectangle is %d\n", shortEdgeLength, longEdgeLength, area);

	return(0);
}
