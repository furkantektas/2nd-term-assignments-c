/*################################################################*/
/* HW00_08104101_PART_1.c
*/
/* ----------------------
*/
/* Created on 22.02.2012 by Ulas Vural
*/
/*
*/
/* Description
*/
/* ----------------------
*/
/* This implementation computes the area of a square
*/
/* for a given edge length.
*/
/*
*/
/* Notes
*/
/* ---------------------
*/
/* This is a sample code.
*/
/*
*/
/* References
*/
/* ---------------------
*/
/* (If any)
*/
/*
*/
/*################################################################*/
/*################################################################*/
/*
Includes
*/
/*################################################################*/
#include <stdio.h>
/*################################################################*/
/* int main()
*/
/* ----------
*/
/* Return
*/
/* ----------
*/
/*
0 on success
*/
/*################################################################*/
int main()
{
	int edgeLength; /* edge length of square */
	int areaOfSquare; /* area of the square */

	/* Get the edge length */
	printf("Edge length: ");
	scanf("%d", &edgeLength);

	areaOfSquare = edgeLength * edgeLength;	/* Compute the area */

	/* Display the area of the square */
	printf("The area of the square is %d.\n", areaOfSquare);

	return 0;
}
