#include<stdio.h>
#include<stdlib.h>
#include<math.h>

#define DEBUG 0

typedef double * Col;

/* Function Prototypes*/
double ** MatrAlloc(int n);
Col       ColAlloc(int n);
double *  RowAlloc(int n);
void      MatrFree(double ** M, int n);
void      PartialPivot(double ** A, Col S,Col B, int n);
void      ForwEliminate(double ** A,Col B,int n);
Col       BackSubstitute(double ** A, Col B, int n);
Col       ScaleFactor(double ** A,int n);
void      Gauss(double ** A, Col B, int n);
void      SwapRows(double * *r1, double **r2);
void      PrintMatrix(double ** M, int n, char * name);
void      PrintCol(Col C, int n, char *name);
void      PrintRow(double * R, int n, char *name);

int
main(int argc, char *argv[])
{
	FILE *file;
	int n,i,j;
	double ** A;
	Col B;
	
	if(argc < 2) {
		printf("\n2 input parameters are expected. Please try again. \n");
		exit(1);
	}

	file = fopen(argv[1],"r");

	if(file == NULL) {
		printf("\nOpen the input file failed: %s\n",argv[1]);
		exit(1);
	}
	fscanf(file,"%i",&n);

	if(DEBUG)
		printf("\nDimension = %i\n",n);

	A = MatrAlloc(n);
	for( i = 1; i <= n; ++i)
		for(j = 1; j <= n; ++j)
			fscanf(file,"%lf", &A[i][j]);

	B = ColAlloc(n);	

	for(j = 1; j <= n; ++j)
		fscanf(file,"%lf",&B[j]);
	fclose(file);


	PrintMatrix(A,n,"A");
	PrintCol(B,n,"B");

	Gauss(A,B,n);

	MatrFree(A,n);
	free(B + 1);

	printf("\nCalculation completed. Terminating.\n");
	return 0;
}
void
PrintMatrix(double ** M, int n, char * name)
{
	int i,j;
	printf("\n[%s] = ",name);
	printf("\n\n");
	for(i = 1; i <= n; i++) {
		for(j = 1; j <= n; ++j)
			printf("%6.1f ",M[i][j]);
		printf("\n");
	}
}
void
PrintCol(Col C, int n, char * name)
{
	int j;
	printf("\n[%s] = ",name);
	printf("\n\n");
	for(j = 1; j <= n; ++j)
		printf("%6.1f\n",C[j]);
}
void
PrintRow(double * R, int n, char * name)
{
	int i;
	printf("\n[%s] = ",name);
	for(i = 1; i <= n; ++i)
		printf("%6.1f ",R[i]);
	printf("\n");
}
double **
MatrAlloc(int n)
{
	double ** A;
	int i;
	A = malloc(n * sizeof(double *));
	if(!A) {
		printf("\nError : Could not allocate memory for matrix\n");
		exit(1);
	}		
	--A;

	for(i = 1; i <= n; ++i) {
		A[i] = malloc(n * sizeof(double));
		if(!A[i]) {
			printf("\nError : Could not allocate memory for matrix\n");
			exit(1);
		}
		--A[i];
	}
	return A;
}
void
MatrFree(double ** M, int n)
{
	int i;
	for(i = 1; i <= n; ++i)
		free(M[i] + 1);
	free(M + 1);
}
Col
ColAlloc(int n)
{
	Col B;

	B = malloc(n * sizeof(double));

	if(!B) {
		printf("\nMemory allocation for column failed.\n");
		exit(1);
	}
	--B;
	return B;
}
double *
RowAlloc(int n)
{
	double * B;
	B = malloc(n * sizeof(double));
	if(!B) {
		printf("\nMemory allocation for row failed.\n");
		exit(1);
	}
	--B;
	return B;
}
Col
ScaleFactor(double ** A, int n)
{
	int i,j;
	Col S ;
	S = ColAlloc(n);

	for(i = 1; i <= n; ++i) {
		S[i] = A[i][1];
		for(j = 2; j <= n; ++j) {
			if(S[i] < fabs(A[i][j]))
				S[i] = fabs(A[i][j]);
		}
	}
	return S;
}

void
PartialPivot(double ** A, Col S,Col B, int n)
{
	int i,j;
	double temp;
	for(j = 1; j <= n; ++j) {
		for(i = j + 1; i <= n; ++i) {
			if(S[i] == 0) {
				if(B[i] == 0)
					printf("\nNo unique solution!");
				else 
					printf("\nSystem is inconsistent");
				exit(1);
			}
			if(fabs(A[i][j]/S[i]) > fabs(A[j][j]/S[j])) {
				SwapRows(&A[i],&A[j]);
				temp = B[i];
				B[i] = B[j];
				B[j] = temp;
			}
		}

		if(A[j][j] == 0) {
			printf("\nSingular System Detected\n");
			exit(1);
		}
	}
}

void
SwapRows(double * *r1, double **r2)
{
	double * temp;
	temp = *r1;
	*r1 = *r2;
	*r2 = temp;
}

void
ForwEliminate(double ** A,Col B,int n)
{
	int i,j,k;
	double m;

	for(k = 1; k <= n-1; ++k) {
		for(i = k + 1; i <= n; ++i) {
			m =  A[i][k] / A[k][k];
			for(j = k + 1; j <= n; ++j) {
				A[i][j] -= m * A[k][j];
				if(i == j && A[i][j] == 0) {
					printf("\nSingular system detected");
					exit(1);
				}
			}
			B[i] -= m * B[k];
		}
	}
}

Col
BackSubstitute(double ** A, Col B, int n)
{
	int i,j;
	double sum;
	Col X = ColAlloc(n);
	X[n] = B[n]/A[n][n];
	for(i = n - 1; i >= 1; --i) {
		sum = 0;
		for(j = i + 1; j <= n; ++j)
			sum += A[i][j] * X[j];
		X[i] = (B[i] - sum) / A[i][i];
	}
	return X;
}

void
Gauss(double ** A, Col B, int n)
{
	Col S, X;
	S = ScaleFactor(A,n);
	PartialPivot(A,S,B,n);
	ForwEliminate(A,B,n);
	X = BackSubstitute(A,B,n);
	PrintCol(X,n,"X");
	
	free(S + 1);
	free(X + 1);
}
