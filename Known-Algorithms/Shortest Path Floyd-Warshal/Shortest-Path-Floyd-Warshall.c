/*############################################################################*/
/* Shortest-Path-Floyd-Warshall.c                                             */
/* ----------------------                                                     */
/* Created on 22.04.2012 by Furkan Tektas                                     */
/* ­­­­­                                                                           */
/* Description                                                                */
/* ----------------------                                                     */
/* This program gets the points and the distance between them from a plain    */
/* text file and finds the shortest path between two points by Floyd-Warshall */
/* algorithm.                                                                 */
/*                                                                            */
/* Sources                                                                    */
/* -------                                                                    */
/* http://en.wikipedia.org/wiki/Floyd%E2%80%93Warshall_algorithm              */
/*                                                                            */
/*############################################################################*/

/*############################################################################*/
/*                                  Includes                                  */
/*############################################################################*/
#include<stdio.h>
#include<stdlib.h>

/*############################################################################*/
/*                                   Macros                                   */
/*############################################################################*/
#define FILENAME "input.txt" 
#define NUMCOLUMNS 3 /* num of columns in the file eg: pt1 pt2 distance */
#define DEBUG 1 /* 1 for print the matrixes,0 to hide */

/*­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­----------------------------------------------------------------------------*/
/*                            Function Prototypes                             */
/*­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­----------------------------------------------------------------------------*/

/* Depends on Floyd-Warshall Algorithm. Finds the shortest path between two
 * points.
 * 
 * inputs: 
 *          matrix : distance holder matrix
 *          matrixSize : dimension of square matrix
 *          pt1 - pt2: two points to find the shortest path between them
 * output:
 *          int - shotest path length between pt1 and pt2
 * 
 */
int FindMinDistance(int **arr, int arrSize, int pt1, int pt2);

/* Finds the smaller of two numbers.
 * 
 * inputs:
 *          x-y: numbers to be compared
 * output:
 *          minimum of x-y
 */
int min(int x, int y);

/* Prints a square NxN matrix
 * 
 * inputs:
 *          matrix: matrix to be printed
 *          matrixSize: dimension of square matrix
 */
void PrintMatrix(int **matrix, int matrixSize);

/*############################################################################*/
/*  int main()                                                                */
/*  ----------                                                                */  
/*  Return                                                                    */
/*  ----------                                                                */
/*      0 on success                                                          */
/*############################################################################*/
int
main()
{
    FILE *inp;                /* file which consist of points and distances */
    int numPoints, numRows,   /* number of points and number of the rows in the file*/
        **matrix,             /* we need pointnumberxpointnumber square matrix*/
        i,j,                  /* counters for counter-controlled loops */
        status = 0,           /* for fscanf */
        pt1,pt2,              /* temporary point holders */
        dist;                 /* temporary distance holder */
        

    inp = fopen(FILENAME, "r");
    /* get rows and columns for memory allocating  */
    fscanf(inp, "%d%d", &numPoints, &numRows);

    /* define and fill with 9999 the matrix */
    matrix = (int**)malloc(numPoints * sizeof(int*));
    for (i=0; i < numPoints; i++)
    {
      matrix[i] = (int*)malloc(numPoints * sizeof(int));
        for(j=0; j<numPoints;++j)
            matrix[i][j] = 9999;
    }

    /* getting points */
    for(i=0; status != EOF && i< numRows;++i)
    {
        status = fscanf(inp, "%d %d %d", &pt1, &pt2, &dist);
        matrix[pt1-1][pt2-1] = dist;
        matrix[pt2-1][pt1-1] = dist;
    }

    /* if debug mode is on, print the initial matrix */
    if(DEBUG)
    {
        PrintMatrix(matrix, numPoints);
        printf("=========================================================\n");
    }

    /* getting two points from user to find the distance between them */
    printf("Enter two points seperated by space to find the distance between them =>");
    scanf("%d%d",&pt1, &pt2);
    if(DEBUG)
        printf("Points are = %d-%d\n", pt1, pt2);

    /* calculate and display the minimum distance between pt1 and pt2 */
    printf("Minimum distance between %d & %d points is %d\n",
           pt1, pt2, FindMinDistance(matrix, numPoints, pt1, pt2));

    return 0;
}

/*----------------------------------------------------------------------------­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­*/
/*                            Function Implementations                        */
/*----------------------------------------------------------------------------­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­*/
int
FindMinDistance(int **matrix, int matrixSize, int pt1, int pt2)
{
    int i,j,k;

    for(i=0;i<matrixSize;++i)
        for(j=0;j<matrixSize;++j)
            for(k=0;k<matrixSize;++k)
                matrix[j][k] = min(matrix[j][k], matrix[j][i] + matrix[i][k]);

    PrintMatrix(matrix, matrixSize);
    return matrix[pt1-1][pt2-1];
}

int
min(int x, int y)
{
    if(x<y)
        return x;
    else
        return y;
}

void
PrintMatrix(int **matrix, int matrixSize)
{
    int i,j;
    for(i=0; i<matrixSize;++i)
    {
        for(j=0; j<matrixSize;++j)
            printf("%d\t",matrix[i][j]);
        printf("\n");
    }
}
