/*############################################################################*/
/* Shortest-Path-Floyd-Warshall.c                                             */
/* ----------------------                                                     */
/* Created on 22.04.2012 by Tarik Altuncu                                     */
/* ­­­­­                                                                           */
/* Description                                                                */
/* ----------------------                                                     */
/* This program gets the points and the distance between them from a plain    */
/* text file and finds the shortest path between two points by Floyd-Warshall */
/* algorithm.                                                                 */
/*                                                                            */
/* Sources                                                                    */
/* -------                                                                    */
/* http://en.wikipedia.org/wiki/Floyd%E2%80%93Warshall_algorithm              */
/*                                                                            */
/*############################################################################*/

/*############################################################################*/
/*                                  Includes                                  */
/*############################################################################*/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

/*############################################################################*/
/*                                   Macros                                   */
/*############################################################################*/
#define NUMCOLUMNS 3 /* num of columns in the file eg: pt1 pt2 distance */
#define DEBUG 0 /* 1 for print the matrixes,0 to hide */

/*­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­----------------------------------------------------------------------------*/
/*                            Function Prototypes                             */
/*­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­----------------------------------------------------------------------------*/

/* Depends on Floyd-Warshall Algorithm. Finds the shortest path between two
 * points.
 * 
 * inputs: 
 *          matrix : distance holder matrix
 *          matrixSize : dimension of square matrix
 *          pt1 - pt2: two points to find the shortest path between them
 * output:
 *          int - shotest path length between pt1 and pt2
 * 
 */
int FindMinDistance(int **arr, int arrSize, int pt1, int pt2, int **nextPoint);

/* Prints a square NxN matrix
 * 
 * inputs:
 *          matrix: matrix to be printed
 *          matrixSize: dimension of square matrix
 */
void PrintMatrix(int **matrix, int matrixSize);

/*
 * Prints the path
 */
void PrintPath (int i, int j, int **nextPoint);
/*############################################################################*/
/*  int main()                                                                */
/*  ----------                                                                */  
/*  Return                                                                    */
/*  ----------                                                                */
/*      0 on success                                                          */
/*############################################################################*/
int
main(int argc, char *argv[])
{
    FILE *inp = NULL;                /* file which consist of points and distances */
    int numPoints, numRows,   /* number of points and number of the rows in the file*/
        **matrix,             /* we need pointnumberxpointnumber square matrix*/
        **nextPoint,                /* balbal */
        i,j,                  /* counters for counter-controlled loops */
        status = 0,           /* for fscanf */
        pt1,pt2,              /* temporary point holders */
        dist;                 /* temporary distance holder */
    char fileName[80];

    if(argc < 2)
    {
        printf("Please enter a valid filename.\n");    
        scanf("%s",fileName);    
    }
    else
    {
        strcpy(fileName,argv[1]);  
    }
  
    inp = fopen(fileName, "r");
    /* get rows and columns for memory allocating  */
    fscanf(inp, "%d%d", &numPoints, &numRows);

    /* define and fill with 9999 the matrix */
    matrix = (int**)malloc(numPoints * sizeof(int*));
    nextPoint = (int**)malloc(numPoints * sizeof(int*));
    for (i=0; i < numPoints; i++)
    {
      matrix[i] = (int*)malloc(numPoints * sizeof(int));
      nextPoint[i] = (int*)malloc(numPoints * sizeof(int));
        for(j=0; j<numPoints;++j)
        {
            if(i==j)
                matrix[i][j] = 0;
            else
                matrix[i][j] = 9999;
            nextPoint[i][j] = i;
        }
    }

    /* getting points */
    for(i=0; status != EOF && i< numRows;++i)
    {
        status = fscanf(inp, "%d %d %d", &pt1, &pt2, &dist);
        matrix[pt1-1][pt2-1] = dist;
        matrix[pt2-1][pt1-1] = dist;
    }

    /* if debug mode is on, print the initial matrix */
    if(DEBUG)
    {
        printf("matrix0\n");
        PrintMatrix(matrix, numPoints);
        printf("nextpoint0\n");
        PrintMatrix(nextPoint, numPoints);
    }

    /* getting two points from user to find the distance between them */
    printf("Enter two points seperated by space to find the distance between them =>");
    scanf("%d%d",&pt1, &pt2);
    if(DEBUG)
        printf("Points are = %d-%d\n", pt1, pt2);

    /* calculate and display the minimum distance between pt1 and pt2 */
    printf("Minimum distance between %d & %d points is %d\n",
           pt1, pt2, FindMinDistance(matrix, numPoints, pt1, pt2, nextPoint));
    
    printf("Shortest path:");
    PrintPath(pt1-1,pt2-1,nextPoint);
    printf("\n");

    return 0;
}

/*----------------------------------------------------------------------------­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­*/
/*                            Function Implementations                        */
/*----------------------------------------------------------------------------­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­*/
int
FindMinDistance(int **matrix, int matrixSize, int pt1, int pt2, int **nextPoint)
{
    int i,j,k;

    for(k=0;k<matrixSize;++k)
        for(i=0;i<matrixSize;++i)
            for(j=0;j<matrixSize;++j)
            {
                if((matrix[i][k] + matrix[k][j]) < matrix[i][j])
                {
                    matrix[i][j] = matrix[i][k]+matrix[k][j];
                    nextPoint[i][j] = k;
                }
            }
    if(DEBUG)
    {
        printf("matrix1\n");
            PrintMatrix(matrix, matrixSize);
        printf("nextpoint1\n");
            PrintMatrix(nextPoint, matrixSize);
    }
    return matrix[pt1-1][pt2-1];
}

void PrintPath (int i, int j, int **nextPoint) {
  if (i!=j)
    PrintPath(i,nextPoint[i][j], nextPoint);
  printf("%d",j+1);
}

void
PrintMatrix(int **matrix, int matrixSize)
{
    int i,j;
    printf("=========================================================\n");
    for(i=0; i<matrixSize;++i)
    {
        for(j=0; j<matrixSize;++j)
            printf("%d\t",matrix[i][j]);
        printf("\n");
    }
    printf("=========================================================\n");    
}
