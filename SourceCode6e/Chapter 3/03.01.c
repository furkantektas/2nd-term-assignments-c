Figure 3.1  Edited Data Requirements and Algorithm for Conversion Program
/*
 * Converts distance in miles to kilometers.
 */

#include <stdio.h>                 /* printf, scanf definitions */
#define KMS_PER_MILE 1.609         /* conversion constant */

int
main(void)
{
      double miles;	/* input - distance in miles.  	*/
      double kms; 	/* output - distance in kilometers 	*/

      /* Get the distance in miles.               	*/

      /* Convert the distance to kilometers.      	*/
         /* Distance in kilometers is 
              1.609 * distance in miles.           	*/

      /* Display the distance in kilometers.       	*/

      return (0);
}