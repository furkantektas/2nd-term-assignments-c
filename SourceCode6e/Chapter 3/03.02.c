Figure 3.2  Outline of Program Circle
/*
 * Calculates and displays the area and circumference of a circle
 */

#include <stdio.h>
#define PI 3.14159

int
main(void)
{
      double radius;    /* input - radius of a circle  */
      double area;      /* output - area of a circle   */
      double circum;    /* output - circumference      */

      /* Get the circle radius */

      /* Calculate the area */
         /* Assign PI * radius * radius to area. */

      /* Calculate the circumference */
         /* Assign 2 * PI * radius to circum. */

      /* Display the area and circumference */

      return (0);
}