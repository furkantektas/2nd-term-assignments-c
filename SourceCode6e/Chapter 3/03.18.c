Figure 3.18  Function print_rboxed and Sample Run
/*
 * Displays a real number in a box.
 */
void
print_rboxed(double rnum)
{
      printf("***********\n");
      printf("*         *\n");
      printf("* %7.2f *\n", rnum);
      printf("*         *\n");
      printf("***********\n");
}

***********
*         *
*  135.68 *
*         *
***********
