/*############################################################################*/
/* HW01_111044029_PART_2.c                                                    */
/* ----------------------                                                     */
/* Created on 03.03.2012 by Furkan Tektas                                     */
/*                                                                            */
/* Notes                                                                      */
/* ­­­­­-----                                                                      */
/* This program needs input.txt file consists of 5 lines, each of which has   */
/* 3 different double values and will create output.txt                       */
/* verbose.                                                                   */
/* ­­­­­                                                                           */
/* Description                                                                */
/* ----------------------                                                     */
/* Calculates the total cost of house with given tax rate,                    */
/* fuel costs and initial costs in input.txt for five years                   */
/* and prints the calculated five years costs with input variables            */
/*                                                                            */
/*############################################################################*/


/*############################################################################*/
/*                                  Includes                                  */
/*############################################################################*/
#include <stdio.h>
#include <math.h>

/*­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­----------------------------------------------------------------------------*/
/*                            Function Prototypes                             */
/*­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­----------------------------------------------------------------------------*/

/*############################################################################*/
/*                                                                            */
/* char GradeToLetter(double  grade)                                          */
/* ­­­­­­­­­­­                                                                           */
/*      grade ­ average grade                                                  */
/*                                                                            */
/* Notes                                                                      */
/*                                                                            */
/*     Grade      Letter Code                                                 */
/*     -------    -----------                                                 */
/*     100        A                                                           */
/*     99-80      B                                                           */
/*     79-50      C                                                           */
/*     49-20      D                                                           */
/*     19-1       E                                                           */
/*     0          F                                                           */
/*                                                                            */
/* Return                                                                     */
/* ­­­­­­                                                                           */
/*     (char) Letter Code of average grade                                    */
/*                                                                            */
/* Description                                                                */
/* ­­­­­­­­­­­                                                                           */
/*     This function takes the average grade and returns the corresponding    */
/*     letter of average grade.                                               */
/*                                                                            */
/*############################################################################*/
char GradeToLetter(double  avgGrade);


/*############################################################################*/
/*                                                                            */
/* double CalculateAverageGrade(int numOfGrades, double sumOfGrades)          */
/* ­­­­­­­­­­­                                                                           */
/*      numOfGrades ­ number of grades                                         */
/*      sumOfGrades  sum of all grades                                        */
/*                                                                            */
/* Return                                                                     */
/* ­­­­­­                                                                           */
/*     (double) Average grade of the sum of the grades                        */
/*                                                                            */
/* Description                                                                */
/* ­­­­­­­­­­­                                                                           */
/*     This function takes the number and sum of the grades then returns      */
/*     the average grade by dividing sum of the grades to number of grades    */
/*                                                                            */
/*############################################################################*/
double CalculateAverageGrade(int numOfGrades, double sumOfGrades);

/*############################################################################*/
/*  int main()                                                                */
/*  ----------                                                                */  
/*  Return                                                                    */
/*  ----------                                                                */
/*       0 on success                                                         */
/*       10 if the number of grades is not between 0 and 5                    */
/*       11 if the grades is not between 0 and 100                            */
/*       12 if the letter code is not between A-F                             */
/*############################################################################*/
int
main(void){
    int    numOfGrades,  /* number of exams will be given by user */
           tempcounter;  /* temporary counter for if conditions  */
    
    double exam1, exam2, exam3,exam4,exam5, /* exam note variables */
           sumOfGrades,  /* Sum of all grades */
           avgGrade;     /* Average grade of all grades */
    
    char   grade;        /* Corresponding letter code of average grade */
    
    /* Assign exam grades to 0 for easily calculate the sum of them */
    exam1 = 0;
    exam2 = 0;
    exam3 = 0;
    exam4 = 0;
    exam5 = 0;
    
    /* Get the number of grades will be given */
    printf("Enter the number of exams:\t");
    scanf("%d",&numOfGrades);
    
    /* Check whether the number of grades is bigger than 0 or smaller than 5 */
    if((numOfGrades > 5) || (numOfGrades < 0)){
        printf("The number of exams should be smaller than 5. Exiting..\n");
        return(10);
    }/* End of if((exam1 < 0) || (exam1 > 100)) */
    
    /* Assign the value of numberOfGrades to the tempCounter */
    tempcounter = numOfGrades;
    
    /* If tempCounter is bigger than 0, get the grade */
    if(tempcounter >= 1)
    {
        printf("Enter the first exam's grade:\t"); /* Prompt for scanf */
        scanf("%lf",&exam1); /* Get the grade */
        
        /* Check the grade whether grade is between 0-100 */
        if((exam1 < 0) || (exam1 > 100))
        {
            /* Grade is not between 0-100. Prompt and exit. */
            printf("Grade should be between 0 and 100. Exiting..\n");
            return(11); /* Exiting */
        }/* End of if((exam1 < 0) || (exam1 > 100)) */

        tempcounter = tempcounter - 1; /* Decrease tempcounter */
    }/* End of if(tempcounter >= 1) */
    
    /* If tempCounter is bigger than 0, get the grade */
    if(tempcounter >= 1)
    {
        printf("Enter the second exam's grade:\t");
        scanf("%lf",&exam2);
        
        if((exam2 < 0) || (exam2 > 100))
        {
            printf("Grade should be between 0 and 100. Exiting..\n");
            return(11); /* Exiting */
        }/* End of if((exam2 < 0) || (exam2 > 100)) */
        tempcounter = tempcounter - 1; /* Decrease tempcounter */
    }/* End of if(tempcounter >= 1) */
    
    /* If tempCounter is bigger than 0, get the grade */
    if(tempcounter >= 1)
    {
        printf("Enter the third exam's grade:\t");
        scanf("%lf",&exam3);
        
        if((exam3 < 0) || (exam3 > 100))
        {
            printf("Grade should be between 0 and 100. Exiting..\n");
            return(11); /* Exiting */
        }/* End of if((exam3 < 0) || (exam3 > 100)) */
        
        tempcounter = tempcounter - 1; /* Decrease tempcounter */
    }/* End of if(tempcounter >= 1) */
    
    /* If tempCounter is bigger than 0, get the grade */
    if(tempcounter >= 1)
    {
        printf("Enter the fourth exam's grade:\t");
        scanf("%lf",&exam4);
        
        if((exam4 < 0) || (exam4 > 100))
        {
            printf("Grade should be between 0 and 100. Exiting..\n");
            return(11); /* Exiting */
        }/* End of if((exam4 < 0) || (exam4 > 100)) */
        
        tempcounter = tempcounter - 1; /* Decrease tempcounter */
    }/* End of if(tempcounter >= 1) */
    
    /* If tempCounter is bigger than 0, get the grade */
    if(tempcounter >= 1)
    {
        printf("Enter the fifth exam's grade:\t");
        scanf("%lf",&exam5);
        
        if((exam5 < 0) || (exam5 > 100))
        {
            printf("Grade should be between 0 and 100. Exiting..\n");
            return(11); /* Exiting */
        }/* End of if((exam5 < 0) || (exam5 > 100)) */
        
        tempcounter = tempcounter - 1; /* Decrease tempcounter */
    }/* End of if(tempcounter >= 1) */
    
    sumOfGrades = exam1 + exam2 + exam3 + exam4 + exam5;
    
    avgGrade = CalculateAverageGrade(numOfGrades, sumOfGrades);
    
    grade = GradeToLetter(avgGrade);
    
    if(grade > 'F')
    {
        printf("Invalid grade. Exiting..\n");
        return(12);
    }/* End of if(grade > 'F') */
    
    printf("Grade: %c\n", grade); /* Displaying grade. */
    
    return(0);
}

/*----------------------------------------------------------------------------­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­*/
/*                            Function Implementations                        */
/*----------------------------------------------------------------------------­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­*/

/* Function GradeToLetter                                                     */
/* ­­­­­­­­­­­---------------------------                                                */
/*     This function takes the average grade and returns the corresponding    */
/*     letter of average grade.                                               */

char GradeToLetter(double avgGrade)
{
    if(avgGrade > 99.999)
        return 'A';
    if(avgGrade > 79.999)
        return 'B';
    if(avgGrade > 49.999)
        return 'C';
    if(avgGrade > 19.999)
        return 'D';
    if(avgGrade > 0.999)
        return 'E';
    if(avgGrade > -1.00)
        return 'F';
    return 'Z';
}

/* Function WriteToFile                                                       */
/* ­­­­­­­­­­­---------------------------                                                */
/*     This function takes the pointer of the output file,                    */
/*     fuel cost for year,  initial cost and total cost for five              */
/*     years and append them to the output file.                              */
/*                                                                            */

double CalculateAverageGrade(int numOfGrades, double sumOfGrades)
{
    if((numOfGrades > 0) && (numOfGrades < 6))
        return (sumOfGrades / (double)numOfGrades);
    return 120.0;
}
/*############################################################################*/
/*                        End of HW01_111044029_PART_2.c                      */
/*############################################################################*/
