/*############################################################################*/
/* LW07_111044029.c                                                           */
/* ----------------------                                                     */
/* Created on 13.04.2012 by Furkan Tektas                                     */
/* ­­­­­                                                                           */
/* Description                                                                */
/* ----------------------                                                     */
/* This program reads the x,y coordinates of points from input  file          */
/* and and finds the maximum distance between them.                           */
/*                                                                            */
/* Notes                                                                      */
/* ----------------------                                                     */
/* Input file consist an integer at the first line, coordinates are seperated */
/* by space and each line represents a point.                                 */
/*                                                                            */
/*############################################################################*/


/*############################################################################*/
/*                                  Includes                                  */
/*############################################################################*/
#include <stdio.h>
#include <math.h>

/*############################################################################*/
/*                                   Macros                                   */
/*############################################################################*/
#define FILENAME "points.txt" /* File name */
#define DEBUG 1
#define MAXSIZEOFARRAY 15

/* Read points from a given input file */
int ReadPoints(FILE * inputFile, double points[][2], int capacityOfArray);

/* Find distance of two points. p1 and p2 are arrays which contains x and y coordinates */
double FindDistance(const double p1[2], const double p2[2]);

/* Find the maximum distance of all points defined in points array */
double FindMaxDistance(double points[][2], int sizeOfArray);

int
main(void)
{
	FILE *inputFile;
	double points[MAXSIZEOFARRAY][2];

	inputFile = fopen(FILENAME,"r");
	/* Filling array with the coordinates from input file. */
	ReadPoints(inputFile, points, MAXSIZEOFARRAY);

	/* Calculating and displaying the maximum distance between points */
	printf("Max Distance is = %.2f\n",FindMaxDistance(points, MAXSIZEOFARRAY));

	/* Closing file */
	fclose(inputFile);

	return 1;	
}

int
ReadPoints(FILE * inputFile, double points[][2], int capacityOfArray)
{
    int i = 0,
        pointNumber = 0, /* Total point numbers */
        status = 0;

	fscanf(inputFile, "%d", &i); /* Skipping the first line */

    i = 0;

    do
    {
        status =  fscanf(inputFile, "%lf%lf", &points[i][0], 
                        &points[i][1]);
        if(DEBUG)
            printf("*****Status: %d Point %d: (%.2f,%.2f)*****\n", status, i,
                   points[i][0], points[i][1]);

        if(status == 2)
            pointNumber += 2;
        ++i;
    }while((status != EOF) && (capacityOfArray > i));/* read until reach to the EOF */

    if(DEBUG)
    {
        printf("*****All Points*****\n");
        for(i=0; i < capacityOfArray; ++i)
            printf("\t%.2f %.2f\n", points[i][0], points[i][1]);
        printf("\nPoint Number = %d\n\n", pointNumber);
    }/* IF(DEBUG) */

    return pointNumber;
}

double
FindDistance(const double p1[2], const double p2[2])
{
	if(DEBUG)
		printf("1st Point = (%.2f,%.2f) 2nd Point = (%.2f,%.2f)\n", p1[0],p1[1],p2[0],p2[1]);

	return sqrt( pow(p1[0]-p2[0], 2) + pow(p1[1]-p2[1], 2) );
}

double
FindMaxDistance(double points[][2], int sizeOfArray)
{
	int i,j, /* counters for loop control*/
		outmostPoints[2]; /* furthest point number holder */
	double distance = 0, /* temporary distance holder */
	       maxDistance = 0; /* max distance holder */

	for(i=0; i<sizeOfArray; ++i)
	{
		for(j=0; j<sizeOfArray; ++j)
		{
			distance = FindDistance(points[i],points[j]);
			
			if(distance > maxDistance)
			{
				maxDistance = distance;
				outmostPoints[0] = i;
				outmostPoints[1] = j;
			}/* if distance */
		}/* for j*/
	}/*for i*/

	printf("\nOutmost Points are %d and %d\n\n", outmostPoints[0], outmostPoints[1]);
	return maxDistance;
}
