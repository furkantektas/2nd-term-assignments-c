/*############################################################################*/
/* LW07_111044029.c                                                           */
/* ----------------------                                                     */
/* Created on 13.04.2012 by Furkan Tektas                                     */
/* ­­­­­                                                                           */
/* Description                                                                */
/* ----------------------                                                     */
/* This program calculates the integral of three different function.          */
/* Functions are defined below as UserFunction*                               */
/*                                                                            */
/* Notes                                                                      */
/* ----------------------                                                     */
/* You can change the initial and final points on the x axis by changing      */
/* valA and valB macros.                                                      */
/*                                                                            */
/*############################################################################*/


/*############################################################################*/
/*                                  Includes                                  */
/*############################################################################*/
#include <stdio.h>
#include <math.h>

/*############################################################################*/
/*                                   Macros                                   */
/*############################################################################*/
#define FILENAME "points.txt" /* File name */
#define DEBUG 1
#define MAXSIZEOFARRAY 15

/* Read points from a given input file */
int ReadPoints(FILE * inputFile, double points[][2], int capacityOfArray);

double FindDistance(double x1, double x2, double y1, double y2);

double FindMaxDistance(double points[][2], int sizeOfArray);

int
main(void)
{
	FILE *inputFile;
	double points[MAXSIZEOFARRAY][2];

	inputFile = fopen(FILENAME,"r");
	ReadPoints(inputFile, points, MAXSIZEOFARRAY);

	printf("Max Distance is = %.2f\n",FindMaxDistance(points, MAXSIZEOFARRAY));

	return 1;	
}

int
ReadPoints(FILE * inputFile, double points[][2], int capacityOfArray)
{
    int i = 0,
        pointNumber = 0, /* Total point numbers */
        status = 0;

	fscanf(inputFile, "%d", &i); /* skipping the first line */

    i = 0;

    do
    {
        status =  fscanf(inputFile, "%lf%lf", &points[i][0], 
                        &points[i][1]);
        if(DEBUG)
            printf("*****Status: %d Point %d: (%.2f,%.2f)*****\n", status, i,
                   points[i][0], points[i][1]);

        if(status == 2)
            pointNumber += 2;
        ++i;
    }while((status != EOF) && (capacityOfArray > i));/* read until reach to the EOF */

    if(DEBUG)
    {
        printf("*****All Points*****\n");
        for(i=0; i < capacityOfArray; ++i)
            printf("\t%.2f %.2f\n", points[i][0], points[i][1]);
        printf("\nPoint Number = %d\n\n", pointNumber);
    }/* IF(DEBUG) */

    return pointNumber;
}

double
FindDistance(double x1, double x2, double y1, double y2)
{
	if(DEBUG)
		printf("1st Point = (%.2f,%.2f) 2nd Point = (%.2f,%.2f)\n", x1,y1,x2,y2);

	return sqrt( pow(x2-x1, 2) + pow(y2-y1, 2) );
}

double
FindMaxDistance(double points[][2], int sizeOfArray)
{
	int i,j; /* counters for loop control*/
	double distance = 0, /* temporary distance holder */
	       maxDistance = 0; /* max distance holder */

	for(i=0; i<sizeOfArray; ++i)
	{
		for(j=0; j<sizeOfArray; ++j)
		{
			distance = FindDistance(points[i][0],points[j][0],points[i][1],points[j][1]);
			
			if(distance > maxDistance)
				maxDistance = distance;
		}/* for j*/
	}/*for i*/

	return maxDistance;
}
