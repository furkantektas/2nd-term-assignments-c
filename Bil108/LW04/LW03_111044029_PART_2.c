/*############################################################################*/
/* LW03_111044029.c                                                           */
/* ----------------------                                                     */
/* Created on 16.03.2012 by Furkan Tektas                                     */
/*                                                                            */
/* Description                                                                */
/* ----------------------                                                     */
/* Calculates the given function in hw02c.pdf with user                       */
/* input values within function domain range. Displays error for              */
/* inappropriate input values.                                                */
/*                                                                            */
/*############################################################################*/

/*############################################################################*/
/*                            Includes                                        */
/*############################################################################*/
#include <stdio.h>

/*­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­----------------------------------------------------------------------------*/
/*                            Function Prototypes                             */
/*­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­----------------------------------------------------------------------------*/
/*############################################################################*/
/*                                                                            */
/* void printResult(double z, double n, double result)                        */
/* ­­­­­­­­­­­                                                                           */
/*                                                                            */
/*      z, n     ­        arguments of f(z,n)                                  */
/*      result           result of f(z,n), calculated by calculateFunction()  */
/*                                                                            */
/* Description                                                                */
/* ­­­­­­­­­­­                                                                           */
/*     This function takes arguments of f(z,n) and result of the function     */
/*     then prints it with appropriate formatting.                            */
/*                                                                            */
/*############################################################################*/

int
main(void)
{
    
    
}

int
printChar(char ch, int height)
{
    if(height == 0)
    {
        printMessage(TERMINATEMESSAGE);
        return
    }
    if(height < 1)
    {
        return -1;
    }
    
}
void
printMessage(int errorCode)
{
    
    
}

/*############################################################################*/
/*                             End of LW03_111044029.c                        */
/*############################################################################*/
