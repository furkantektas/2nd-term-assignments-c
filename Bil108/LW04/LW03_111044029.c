/*############################################################################*/
/* LW03_111044029.c                                                           */
/* ----------------------                                                     */
/* Created on 16.03.2012 by Furkan Tektas                                     */
/*                                                                            */
/* Description                                                                */
/* ----------------------                                                     */
/* Calculates the given function in hw02c.pdf with user                       */
/* input values within function domain range. Displays error for              */
/* inappropriate input values.                                                */
/*                                                                            */
/*############################################################################*/

/*############################################################################*/
/*                            Includes                                        */
/*############################################################################*/
#include <stdio.h>

#define TERMINATEMESSAGE   1
#define INVALIDNUMERROR    2
#define INVALIDCHARERROR   3
#define INVALIDSHAPEERROR  4
#define INVALIDHEIGHTERROR 5


#define UPPERTRIANGLE 1
#define LOWERTRIANGLE 2
#define DIAMOND       3
/*­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­----------------------------------------------------------------------------*/
/*                            Function Prototypes                             */
/*­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­----------------------------------------------------------------------------*/
/*############################################################################*/
/*                                                                            */
/* void printResult(double z, double n, double result)                        */
/* ­­­­­­­­­­­                                                                           */
/*                                                                            */
/*      z, n     ­        arguments of f(z,n)                                  */
/*      result           result of f(z,n), calculated by calculateFunction()  */
/*                                                                            */
/* Description                                                                */
/* ­­­­­­­­­­­                                                                           */
/*     This function takes arguments of f(z,n) and result of the function     */
/*     then prints it with appropriate formatting.                            */
/*                                                                            */
/*############################################################################*/
int PrintChar(char ch, int num);
int printMessage(int errorCode);
int Part1(void);
int Part2(void);

int PrintUpperTriange(int height);
int PrintLowerTriange(int height);

int
main(void)
{
    int part1Return,
    partNumber;
    
    printf("\nSelect the part:\n\t");
    printf("[1] Part 1\n\t");
    printf("[2] Part 2\n\t");
    scanf("%d",&partNumber);
    
    switch(partNumber)
    {
        case 1:
            part1Return = Part1();
            printf("\n\nPrinted char count: %d", part1Return);
            return part1Return;
            break;
        case 2:
            return Part2();
            break;
        default:
            return printMessage(INVALIDSHAPEERROR);
    }
}

/*############################################################################*/
/*                                    PART-1                                  */
/*############################################################################*/
int
Part1(void)
{
    int num;
    char ch,
         temp;
    printf("\nEnter a character:\n\t");
    scanf(" %c%c", &ch, &temp);
    
    printf("\nEnter the number:\n\t");
    scanf("%d", &num);
    
    return PrintChar(ch, num);
}

int
PrintChar(char ch, int num)
{
    int counter, /* total printed char counter */
    i; /* for loop counter */
    counter = 0;
    
    if(num == 0)
    {
        printMessage(TERMINATEMESSAGE);
        return 0;
    }
    else if(num < 0)
    {
        printMessage(INVALIDNUMERROR);
        return -1;
    }
    else if( (ch <= '!' ) || (ch >= '~' ) ) /* Printable characters */
    {
        printMessage(INVALIDCHARERROR);
        return -1;
    }
    else {
        for(i=0;i<num;++i)
            counter += printf("%c ",ch);
        printf("\n"); /* line break */
        return counter;
    }
    
}

int
printMessage(int errorCode)
{
    switch( errorCode )
    {
        case INVALIDNUMERROR:
            printf("Number should be positive.");
            return -1;
            break;
        case INVALIDCHARERROR:
            printf("You've entered invalid char.");
            return -1;
            break;
        case INVALIDSHAPEERROR:
            printf("You did not enter a valid option.");
            return -1;
            break;
        case INVALIDHEIGHTERROR:
            printf("Height should b.");
            return -1;
            break;
        case TERMINATEMESSAGE:
            printf("Terminating..");
            return 0;
            break;            
        default:
            printf("\nSomething goes wrong in userDefinedMethod! Terminating.\n");
            return -1;
    }
    
}
/*############################################################################*/
/*                                END OF PART-1                               */
/*############################################################################*/

/*############################################################################*/
/*                                    PART-2                                  */
/*############################################################################*/
int
Part2(void)
{
    int height,
        shape,
        returnValue;
    do
    {
        printf("\nSelect a shape:\n\t");
        printf("[%d] Upper Triangle\n\t", UPPERTRIANGLE);
        printf("[%d] Lower Triangle\n\t", LOWERTRIANGLE);
        printf("[%d] Diamond\n\t", DIAMOND);
        printf("[0] Exit\n\t");
        scanf("%d", &shape);

        if(shape == 0)
            return printMessage(TERMINATEMESSAGE);
        else if( (shape < 1) && (shape > 4) )
            return printMessage(INVALIDSHAPEERROR);
        else
        {
            printf("Enter the height:");
            scanf("%d",&height);
            
            if (height < 0)
                return printMessage(INVALIDHEIGHTERROR);
            else
            {
                switch(shape)
                {
                    case UPPERTRIANGLE:
                        returnValue = PrintUpperTriange(height);
                        break;
                    case LOWERTRIANGLE:
                        returnValue = PrintLowerTriange(height);
                        break;
                    case DIAMOND:
                        if(height == 2)
                            height = 1;
                        else if( (height % 2) == 0 )
                            height = (height - 2) / 2;
                        else
                            height = (height - 1) / 2;
                        
                        PrintUpperTriange(height);
                        PrintChar('*', height+1);
                        PrintLowerTriange(height);
                        break;
                }
            }
            /*return returnValue;*/
        }
    }
    while(shape > 0);
    return returnValue;
}

int
PrintUpperTriange(int height)
{
    int counter1;
    if(height > 0)
    {
        for(counter1 = 1; counter1 <= height; ++counter1)
        {
                PrintChar('*', counter1);
        }
        return 1;
    }
    else
        return -1;
    
}
int
PrintLowerTriange(int height)
{
    int counter1;
    if(height > 0)
    {
        for(counter1 = height; counter1 > 0; --counter1)
        {
                PrintChar('*', counter1);
        }
        return 1;
    }
    else
        return -1;
    
}
/*############################################################################*/
/*                                END OF PART-2                               */
/*############################################################################*/



/*############################################################################*/
/*                             End of LW03_111044029.c                        */
/*############################################################################*/
