/*############################################################################*/
/* LW08_111044029.c                                                           */
/* ----------------------                                                     */
/* Created on 20.04.2012 by Furkan Tektas                                     */
/* ­­­­­                                                                           */
/* Description                                                                */
/* ----------------------                                                     */
/* Part 1: Gets a string from user and check whether the string is palindrome */
/* or not                                                                     */
/*                                                                            */
/* Part 2: Gets a string and a char from user and delete the occurences of    */
/* user given char.                                                           */
/*                                                                            */
/* Notes                                                                      */
/* ----------------------                                                     */
/* You can change the string size by STRSIZE macro                            */
/*                                                                            */
/*############################################################################*/


/*############################################################################*/
/*                                  Includes                                  */
/*############################################################################*/
#include <stdio.h>
#include <string.h>

/*############################################################################*/
/*                                   Macros                                   */
/*############################################################################*/
#define DEBUG 1
#define STRSIZE 80

/* returns 1 if the word is palindrome (symmetric) */
int isPalindrome(char *str);

/* strlen equivalent */
int GetStrLength(char *str);

/* Delete charToDel occurrences from str string */
char* DeleteChar(char str[], char charToDel);

int
main(void)
{
	char str[STRSIZE], newStr[STRSIZE], /* strings */
		 charToDel,
		 temp; /* temporary char to hold \n */
	
	
	printf("Enter a string:");
	gets(str);

	/* Part 1 -- uncomment the line below to activate part 1 	
	if(isPalindrome(str))
		printf("'%s' is a palindrome", str);
	else
		printf("'%s' is not a palindrome", str);
	*/
	
	/* get char to delete */
	printf("Enter the char to delete:");
	scanf("%c%c",&charToDel, &temp);

	/* delete char and copy to newStr */
	strcpy(newStr, DeleteChar(str, charToDel));
	
	/* Print str and newStr */
	printf("Your new string is (defined in main) = '%s'\n", str);
	printf("Your new string is (returned from DeleteChar fn.) = '%s'\n", newStr);

	return 1;	
}

/* returns 1 if the word is palindrome (symmetric) */
int
isPalindrome(char *str)
{
	int length,
        i = 0,
		res = 0;
	
	length = GetStrLength(str);


	if(DEBUG)
		printf("length : %d\n", length);

	for(i=0;i<length/2;++i)
		res += (str[i] != str[length -1 - i]); 
	
	if(DEBUG)
		printf("res : %d\n", res);

	if(res > 0)
		return 0;
	else
		return 1;
}

/* strlen equivalent */
int
GetStrLength(char *str)
{	
	int length;
	/* get length */
	for(length=0;str[length]!='\0';++length);
	return length;
}

/* Delete charToDel occurrences from str string */
char* DeleteChar(char str[], char charToDel)
{
	int i,j, /* counters for loops */
		length,
		position;

	/* getting length */
	length = GetStrLength(str);

	/* scanning str for the occurrences of charToDel */
	for(i = 0; i<length; ++i)
	{
		if(str[i] == charToDel)
		{
			/* charToDel found in position i, shifting 
			   the next chars to one position back */ 
			for(j=i;j<length;j++)
				str[j] = str[j+1];
		}/* if str[i].. */
	}/* for i */
	return str;
}
