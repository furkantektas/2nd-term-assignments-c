/*############################################################################*/
/* HW02_111044029_PART_2.c                                                    */
/* ----------------------                                                     */
/* Created on 13.03.2012 by Furkan Tektas                                     */
/* ­­­­­                                                                           */
/* Description                                                                */
/* ----------------------                                                     */
/* This program get 5 chars from user and classify each of them with          */
/* different algorithms selected by user. Classification table is defined     */
/* below.                                                                     */
/*                                                                            */
/*############################################################################*/


/*############################################################################*/
/*                                  Includes                                  */
/*############################################################################*/
#include <stdio.h>
#include <math.h>
/*############################################################################*/
/*                                   Macros                                   */
/*############################################################################*/
#define valN 1000.0
#define valA 0.0
#define valB 5.0

double AreaUnderTheCurve(int a, int b, int n);
double UserFunctionF(double x);

int
main(void)
{
	printf("Area of f(x+5) (from %f to %f) = %f\n",valA, valB, AreaUnderTheCurve(valA, valB, valN) );
	return 1;
}


double
AreaUnderTheCurve(int a, int b, int n)
{
	double h,
	       result,
		   tempResult = 0, /* temporary result for calculate f(xi) */
		   i; /* counter of for loop */
	h = (double)(valB - valA)	/ valN;

	for(i=1; i<n; ++i)
		tempResult += UserFunctionF(h * i + a);

	result = h/2 * ( UserFunctionF(a) + UserFunctionF(b) + 2 * tempResult);
	
	return result;

}

double
UserFunctionF(double x)
{
	return x + 5;
}

double
UserFunctionG(double x)
{
	return (pow(x,2) - 3*x + 1);
}

double
UserFunctionH(double x)
{
	return pow(x,3);
}
