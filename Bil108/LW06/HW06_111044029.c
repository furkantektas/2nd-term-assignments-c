/*############################################################################*/
/* HW02_111044029_PART_2.c                                                    */
/* ----------------------                                                     */
/* Created on 13.03.2012 by Furkan Tektas                                     */
/* ­­­­­                                                                           */
/* Description                                                                */
/* ----------------------                                                     */
/* This program calculates the integral of three different function.          */
/* Functions are defined below as UserFunction*                               */
/*                                                                            */
/* Notes                                                                      */
/* ----------------------                                                     */
/* You can change the initial and final points on the x axis by changing      */
/* valA and valB macros.                                                      */
/*                                                                            */
/*############################################################################*/


/*############################################################################*/
/*                                  Includes                                  */
/*############################################################################*/
#include <stdio.h>
#include <math.h>

/*############################################################################*/
/*                                   Macros                                   */
/*############################################################################*/
#define valN 1000.0 /* Portiın number */
#define valA 0.0    /* Start Point */
#define valB 4.0    /* End Point */

double AreaUnderTheCurve(int a, int b, int n, double f(double x));
double UserFunctionF(double x);
double UserFunctionG(double x);
double UserFunctionH(double x);

typedef enum funcs
{
	linear,
	quadratic,
	cubic
}func;

int
main(void)
{
	int fn; /* User input - Function that will be calculated */

	printf("Select the function:\n");
	printf("\t[%d] f(x) = x+5\n", linear); 
	printf("\t[%d] g(x) = 2x^2-3x+1\n", quadratic); 
	printf("\t[%d] h(x) = x^3\n", cubic); 

	scanf("%d", &fn);
	return Select(fn);	
}


double
AreaUnderTheCurve(int a, int b, int n, double f(double x))
{
	double h,
	       result,
		   tempResult = 0, /* temporary result for calculate f(xi) */
		   i; /* counter of for loop */
	h = fabs((double)(b - a)	/ n);

	for(i=1; i<n; ++i)
		tempResult += f(h * i + a);

	result = h/2 * ( f(a) + f(b) + 2 * tempResult);
	
	return result;
}

double
UserFunctionF(double x)
{
	return x + 5;
}

double
UserFunctionG(double x)
{
	return (pow(x,2) - 3*x + 1);
}

double
UserFunctionH(double x)
{
	return pow(x,3);
}

int
Select(func fn)
{
		switch(fn)
		{
			case linear:
				printf("Area of f(x+5) (from %.2f to %.2f) = %.2f\n",
                        valA, valB, AreaUnderTheCurve(valA, valB, valN, UserFunctionF ) );
				return 1;
			case quadratic:
				printf("Area of g(x) = 2x^2-3x+1 (from %.2f to %.2f) = %.2f\n",
                        valA, valB, AreaUnderTheCurve(valA, valB, valN, UserFunctionG) );
				return 1;
			case cubic:
				printf("Area of h(x) = x^3 (from %.2f to %.2f) = %.2f\n",
                        valA, valB, AreaUnderTheCurve(valA, valB, valN, UserFunctionH) );
				return 1;
			default:
				printf("You've entered an invalid choice!\n");
				return -1;
		}
}
