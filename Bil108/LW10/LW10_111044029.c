/* LW10_111044029.c Furkan Tektas 11.05.2012 */
#include <stdio.h>
#include <math.h>

int isElement(int num, const int arr[], int sizeOfArr);
int isDisjoint(const int arr1[], int sizeOfArr1, const int arr2[], int sizeOfArr2);
int getNumOfDigits(int num);
int intRecReverser(int number, int numOfDig);
int intReverser(int number);
int isSet(int set[], int sizeOfSet);

int main(void)
{
	int     set1[] = {1,2,3,4,5,6,7,8,9,10},
	        sizeOfSet1 = 10,
	        set2[] = {1,3,5,7,9,11,13,15,17,19},
	        sizeOfSet2 = 10,
	        set3[] = {11,12,13,14,15,16,17,18,19,20},
	        sizeOfSet3 = 10,
	        set4[] = {3,4,5,6,4}, /* not a set */
	        sizeOfSet4 = 5,	        
	        testNum;
	
	/* Part 1 */	        
	testNum = 1;
	if(isElement(testNum, set1, sizeOfSet1))
		printf("%d is an element of set1 array\n", testNum);
	else
		printf("%d is not an element of set1 array\n", testNum);

	testNum = 12;
	if(isElement(testNum, set1, sizeOfSet1))
		printf("\t%d is an element of set1 array\n", testNum);
	else
		printf("\t%d is not an element of set1 array\n", testNum);


	/* Part 2 */	        
	if(isDisjoint(set1, sizeOfSet1, set2, sizeOfSet2))
		printf("\tset1 and set2 are disjoint\n");
	else
		printf("\tset1 and set2 are not disjoint\n");
		
	if(isDisjoint(set1, sizeOfSet1, set3, sizeOfSet3))
		printf("\tset1 and set3 are disjoint\n");
	else
		printf("\tset1 and set3 are not disjoint\n");
	
	/* Part 3 */
	testNum = 1234;
	printf("\t%d has %d digits\n", testNum, getNumOfDigits(testNum));	 
	testNum = 123;
	printf("\t%d has %d digits\n", testNum, getNumOfDigits(testNum));
	testNum = 7;
	printf("\t%d has %d digits\n", testNum, getNumOfDigits(testNum));
	
	/* Part 4 */
	printf("Using intRecReverser()\n");
	testNum = 1234;
	printf("\tReverse of the number %d is %d\n", testNum, intRecReverser(testNum,4));	 
	testNum = 123;
	printf("\tReverse of the number %d is %d\n", testNum, intRecReverser(testNum,3));
	
	printf("Using intReverser()\n");
	testNum = 1234;
	printf("\tReverse of the number %d is %d\n", testNum, intReverser(testNum));	 
	testNum = 123;
	printf("\tReverse of the number %d is %d\n", testNum, intReverser(testNum));
	
	/* Part 5 */
	printf("Part 5\n");
	if(isSet(set1, sizeOfSet1))
		printf("\tset1 is a set\n");
	else
		printf("\tset1 is not a set\n");
		
	if(isSet(set2, sizeOfSet2))
		printf("\tset2 is a set\n");
	else
		printf("\tset2 is not a set\n");

	if(isSet(set3, sizeOfSet3))
		printf("\tset3 is a set\n");
	else
		printf("\tset3 is not a set\n");
	
	if(isSet(set4, sizeOfSet4))
		printf("\tset4 is a set\n");
	else
		printf("\tset4 is not a set\n");
}

int
isElement(int num, const int arr[], int sizeOfArr) 
{
printf("%d-%d\n", num, arr[sizeOfArr]);
	if(sizeOfArr > 0) {
		if(arr[sizeOfArr-1] != num)
			return isElement(num, arr, sizeOfArr-1);
		else
			return 1;
	}
	else
		return 0;
}

int
isDisjoint(const int arr1[], int sizeOfArr1, const int arr2[], int sizeOfArr2)
{

	if(sizeOfArr1 > 0) {
		if(isElement(arr1[sizeOfArr1-1], arr2, sizeOfArr2))
			return 0;
		else
			return isDisjoint(arr1, sizeOfArr1-1, arr2, sizeOfArr2);
	}
	else
		return 1;
}

int
getNumOfDigits(int num)
{
	if(num < 10)
		return 1;
	return 1 + getNumOfDigits(num/10);
}

int
intRecReverser(int number, int numOfDig)
{
	if(numOfDig > 0) {
		return (number % 10)*pow(10,numOfDig-1) + intRecReverser(number/10, numOfDig-1); 
	}
	else
		return 0;
}

int
intReverser(int number)
{
	return intRecReverser(number, getNumOfDigits(number));
}

/* doesnt work */
int
isSet(int set[], int sizeOfSet)
{
	printf("sizeof=%d\n", sizeOfSet);
	if(sizeOfSet == 0) {
		return 1;
	}
	else {
		if(isElement(set[0], set, sizeOfSet))
			return 0;
		return isSet(&set[sizeOfSet-1], sizeOfSet-1);
	}
}
