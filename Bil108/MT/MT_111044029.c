/* 111044029 Furkan Tektas */
#include <stdio.h>
#include <string.h>

#define MAX_NUMBER_OF_STUDENTS 150
#define MAX_NAME_LEN 25

/*============================Function Prototypes=============================*/
/* part 1 */
char *strReverser(char *str);

/* part 2 */
int intReverser(int num);

/* part 3 */
void sorter(char names[][MAX_NAME_LEN], int grades[], unsigned int numberOfStudents);
void print(char names[][MAX_NAME_LEN], int grades[], unsigned int numberOfStudents);


/* returns 1 on success */
int
main(void)
{
	char str[MAX_NAME_LEN]; /*part1*/
	int num; /*part 2*/
	/* part 3 */
	unsigned int numberOfStudents;
	char names[MAX_NUMBER_OF_STUDENTS][MAX_NAME_LEN] = {"ali", "ahmet", "ayse", "yusuf", "can", "nur", "temel"};
	int grades[MAX_NUMBER_OF_STUDENTS] = {45,24,65,87,9,95,65};
	
	numberOfStudents = 7;
	
	/* part 1 */
	printf("============PART 1============\n");
	printf("Enter a string to reverse:");
	gets(str);
	str[MAX_NAME_LEN-1] = '\0'; /* safe str */
	printf("Reverse of the string: %s\n", strReverser(str));
	
	/* part 2 */
	printf("============PART 2============\n");	
	printf("Enter an integer:");
	scanf("%d",&num);
	printf("Reverse of the integer: %d\n", intReverser(num));
	
	/* part 3 */
	printf("============PART 3============\n");	
	print(names, grades, numberOfStudents);
	sorter(names, grades, numberOfStudents);
	print(names, grades, numberOfStudents);

	return 1;
}

/*==========================Function Implementations==========================*/

/* part 1 */
char
*strReverser(char *str)
{
	char temp;
	int i, length;
	length = strlen(str);
	for(i=0; i < (length)/2; ++i)
	{
		temp = str[length-1-i]; 
		str[length-1-i] = str[i];
		str[i] = temp;
	}
	return str;
}

/* part 2 */
int
intReverser(int num)
{
	int newnum = 0, tenpower, ceiltenpower;
	
	/* getting the power of ten bigger than num */
	for(ceiltenpower = 1; ceiltenpower < num; ceiltenpower *= 10);

	for(tenpower=10; tenpower <= ceiltenpower; tenpower *= 10)
	{
		newnum += (ceiltenpower/tenpower) * (num%10);
		num = (num - num%10)/10;		
	}
	return newnum;
}

/* part 3 */
void
sorter(char names[][MAX_NAME_LEN], int grades[], unsigned int numberOfStudents)
{
	int  tempGrade,i,j;
	char tempName[MAX_NAME_LEN];
	for(i=0;i<numberOfStudents; ++i)
	{
		for(j=0;j<numberOfStudents; ++j)
		{
			if(grades[j] > grades[i])
			{
				tempGrade = grades[j];
				strcpy(tempName, names[j]);
			
				grades[j] = grades[i];
				strcpy(names[j], names[i]);			
			
				grades[i] = tempGrade;
				strcpy(names[i],tempName);			
			}
		}
	}
}

void
print(char names[][MAX_NAME_LEN], int grades[], unsigned int numberOfStudents)
{
	unsigned int i;
	for(i=0;i<numberOfStudents; ++i)
		printf("%d\t%s\t%d\n", i+1, names[i], grades[i]);
	printf("\n");
}
