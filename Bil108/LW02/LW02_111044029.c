/*################################################################*/
/* LW02_111044029.c                                               */
/* ----------------------                                         */
/* Created on 02.03.2012 by Furkan Tektas                         */
/*                                                                */
/* Description                                                    */
/* ----------------------                                         */
/* Calculates approximate factorials of values from input.txt and */
/* put them to the output.txt.                                    */
/*                                                                */
/*################################################################*/


/*################################################################*/
/*                            Includes                            */
/*################################################################*/
#include<stdio.h>
#include <math.h>

/*################################################################*/
/*                             Macros                             */
/*################################################################*/
#define PI 3.14159265

/*################################################################*/
/*  int main()                                                    */
/*  ----------                                                    */  
/*  Return                                                        */
/*  ----------                                                    */
/*       0 on success                                             */
/*################################################################*/
int
main(void){

	FILE *  input, /* input file */
		 *	output;/* output file */

	double 	inputValue,/* value from input file */
			calculatedValue; /* calculated factorial */

	/* Initializing the files for input & output */
	input = fopen("input.txt","r");
	output = fopen("output.txt","w");

	/*     #####     Calculating 1st Value     ######     */
	fscanf(input, "%lf", &inputValue); /*Getting first input */
	calculatedValue = pow( inputValue , inputValue ) * ( exp( -inputValue ) ) * sqrt( ( 2 * PI * inputValue ) + 1.0/3.0 ); /* calculating factorial */
	fprintf(output, "%2.0f! equals approximately %20.2f.\n", inputValue, calculatedValue); /*putting value to the output file */

	/*     #####     Calculating 2nd Value     ######     */
	fscanf(input, "%lf", &inputValue); /*Getting second input */
	calculatedValue = pow( inputValue , inputValue ) * ( exp( -inputValue ) ) * sqrt( ( 2 * PI * inputValue ) + 1.0/3.0 ); /* calculating factorial */
	fprintf(output, "%2.0f! equals approximately %20.2f.\n", inputValue, calculatedValue); /*putting value to the output file */

	/*     #####     Calculating 3rd Value     ######     */
	fscanf(input, "%lf", &inputValue); /*Getting third input */
	calculatedValue = pow( inputValue , inputValue ) * ( exp( -inputValue ) ) * sqrt( ( 2 * PI * inputValue ) + 1.0/3.0 ); /* calculating factorial */
	fprintf(output, "%2.0f! equals approximately %20.2f.\n", inputValue, calculatedValue); /*putting value to the output file */

	/*     #####     Calculating 4th Value     ######     */
	fscanf(input, "%lf", &inputValue); /*Getting fourth input */
	calculatedValue = pow( inputValue , inputValue ) * ( exp( -inputValue ) ) * sqrt( ( 2 * PI * inputValue ) + 1.0/3.0 ); /* calculating factorial */
	fprintf(output, "%2.0f! equals approximately %20.2f.\n", inputValue, calculatedValue); /*putting value to the output file */

	/*     #####     Calculating 5th Value     ######     */
	fscanf(input, "%lf", &inputValue); /*Getting fifth input */
	calculatedValue = pow( inputValue , inputValue ) * ( exp( -inputValue ) ) * sqrt( ( 2 * PI * inputValue ) + 1.0/3.0 ); /* calculating factorial */
	fprintf(output, "%2.0f! equals approximately %20.2f.\n", inputValue, calculatedValue); /*putting value to the output file */


	/* Closing files */
	fclose(input); 
	fclose(output);

	return(0);

}
