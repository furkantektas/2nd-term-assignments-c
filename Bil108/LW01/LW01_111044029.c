/*################################################################*/
/* LW01_111044029.c                                               */
/* ----------------------                                         */
/* Created on 24.02.2012 by Furkan Tektas                         */
/*                                                                */
/* Description                                                    */
/* ----------------------                                         */
/* Converts fahreneit to celcius                                  */
/*                                                                */
/*################################################################*/


/*################################################################*/
/*                            Includes                            */
/*################################################################*/
#include <stdio.h>

/*################################################################*/
/*                             Macros                             */
/*################################################################*/
#define FAHRENHEIT_INIT 32            /* Corresponding degree of 0 celcius */
#define FAHRENHEIT_CELCIUS_RATE 1.8   /* Fahreneit/Celcius */


/*################################################################*/
/*  int main(void)                                                */
/*  ----------                                                    */  
/*  Return                                                        */
/*  ----------                                                    */
/*       0 on success                                             */
/*################################################################*/
int
main(void)
{
	double fahrenheitDegree,   /* User given fahreneit degree */
	       celciusDegree;      /* Calculated celcius degree */

	/* Get the fahrenheit degree */
	printf("Enter the fahrenheit degree to be converted to celcius:");
	scanf("%lf",&fahrenheitDegree);
	
	/* Calculating the celcius degree by given formula */
	celciusDegree = ( fahrenheitDegree - FAHRENHEIT_INIT ) / FAHRENHEIT_CELCIUS_RATE;

	/* Print the corresponding celcius value. */
	printf("Corresponding degree of %f fahrenheit is %f celcius.\n", fahrenheitDegree, celciusDegree);

	return(0);
}
