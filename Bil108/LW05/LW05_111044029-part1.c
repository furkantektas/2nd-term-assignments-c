/*############################################################################*/
/* HW02_111044029_PART_2.c                                                    */
/* ----------------------                                                     */
/* Created on 13.03.2012 by Furkan Tektas                                     */
/* ­­­­­                                                                           */
/* Description                                                                */
/* ----------------------                                                     */
/* This program get 5 chars from user and classify each of them with          */
/* different algorithms selected by user. Classification table is defined     */
/* below.                                                                     */
/*                                                                            */
/* Notes                                                                      */
/* ­­­­­-----                                                                      */
/* Number of loops can be changed by TESTCOUNT macro.                         */
/*                                                                            */
/*############################################################################*/


/*############################################################################*/
/*                                  Includes                                  */
/*############################################################################*/
#include <stdio.h>
#include <time.h>

#define MINTURN 10
#define MAXTURN 100

#define DEBUG 0

int PrintStar(int num);
int GetRandomInt(int mod);

int PrintHistogram(int numOfOnes, int numOfTwos, int numOfThrees,
                   int numOfFours, int numOfFives, int numOfSixes);

void
TurnCounter(int currentTurn, int *numOfOnes, int *numOfTwos, int *numOfThrees, 
            int *numOfFours, int *numOfFives, int *numOfSixes);

int
main(void){
    int numOfTurns,
		dice,
		printedStarNum,
		i; /* counter */
	int
        numOfOnes = 0,
        numOfTwos = 0,
        numOfThrees = 0,
        numOfFours = 0,
        numOfFives = 0,
        numOfSixes = 0;
		
	srand(time(NULL)); /* seed the generator */

	do
	{        
    printf("Number of turns [%d,%d]:", MINTURN, MAXTURN);
	scanf("%d", &numOfTurns);
	} while( (numOfTurns >= MAXTURN) || (numOfTurns <= MINTURN) );

	for(i=0; i<numOfTurns; ++i)
	{
		dice = ThrowDice(numOfTurns);

		if(DEBUG)
			printf("********* dice = %d *********\n", dice);

		switch(dice)
		{
			case 1:
				++numOfOnes;
				break;
			case 2:
				++numOfTwos;
				break;
			case 3:
				++numOfThrees;
				break;
			case 4:
				++numOfFours;
				break;
			case 5:
				++numOfFives;
				break;
			default: /* case = 6 */
				++numOfSixes;
		}

		
	}

	printedStarNum = PrintHistogram(numOfOnes, numOfTwos, numOfThrees, numOfFours, numOfFives, numOfSixes);

	if(DEBUG)
		printf("printed stars = %d user input = %d",printedStarNum, numOfTurns);

	if(printedStarNum == numOfTurns)
		printf("%d\n",printedStarNum);

    return 1;
}


int
PrintHistogram(int numOfOnes, int numOfTwos, int numOfThrees,
                   int numOfFours, int numOfFives, int numOfSixes)
{
	int counter = 0;
	if(DEBUG)
			printf("******1:%d 2:%d 3:%d 4:%d 5:%d 6:%d******\n", 
		            numOfOnes, numOfTwos, numOfThrees, numOfFours, numOfFives, numOfSixes);

	printf("1: ");
	if(numOfOnes > 0)
		counter += PrintStar(numOfOnes);

	printf("2: ");
	if(numOfTwos > 0)
		counter += PrintStar(numOfTwos);

	printf("3: ");
	if(numOfThrees > 0)
		counter += PrintStar(numOfThrees);

	printf("4: ");
	if(numOfFours > 0)
		counter += PrintStar(numOfFours);

	printf("5: ");
	if(numOfFives > 0)
		counter += PrintStar(numOfFives);

	printf("6: ");
	if(numOfSixes > 0)
		counter += PrintStar(numOfSixes);

	return counter;
}


int
PrintStar(int num)
{
    char ch = '*';
    int counter, /* total printed char counter */
    i; /* for loop counter */
    counter = 0;
	if(num > 0)
	{
        for(i=0;i<num;++i)
            counter += printf("%c",ch);
        printf("\n"); /* line break */
    } 
   	return counter; 
}


int
ThrowDice()
{
	return GetRandomInt(6) + 1;
}

void
TurnCounter(int currentTurn, int *numOfOnes, int *numOfTwos, int *numOfThrees, 
            int *numOfFours, int *numOfFives, int *numOfSixes)
{
	switch(currentTurn)
	{
		case 1:
			++*numOfOnes;
			break;
		case 2:
			++*numOfTwos;
			break;
		case 3:
			++*numOfThrees;
			break;
		case 4:
			++*numOfFours;
			break;
		case 5:
			++*numOfFives;
			break;
		default: /* case = 6 */
			++*numOfSixes;
	}
}

/* Generates random integer with given mod */
int
GetRandomInt(int mod)
{
    return rand() % mod;
}
