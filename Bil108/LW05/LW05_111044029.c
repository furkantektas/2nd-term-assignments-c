/*############################################################################*/
/* HW02_111044029_PART_2.c                                                    */
/* ----------------------                                                     */
/* Created on 13.03.2012 by Furkan Tektas                                     */
/* ­­­­­                                                                           */
/* Description                                                                */
/* ----------------------                                                     */
/* This program get 5 chars from user and classify each of them with          */
/* different algorithms selected by user. Classification table is defined     */
/* below.                                                                     */
/*                                                                            */
/* Notes                                                                      */
/* ­­­­­-----                                                                      */
/* Number of loops can be changed by TESTCOUNT macro.                         */
/*                                                                            */
/*############################################################################*/


/*############################################################################*/
/*                                  Includes                                  */
/*############################################################################*/
#include <stdio.h>
#include <time.h>

#define MINTURN 5000
#define MAXTURN 10000

#define DEBUG 0

int PrintStar(int num);
int GetRandomInt(int mod);

int PrintHistogram(int numOfOnes, int numOfTwos, int numOfThrees,
                   int numOfFours, int numOfFives, int numOfSixes);

int
FindBiggest(int numOfOnes, int numOfTwos, int numOfThrees,
            int numOfFours, int numOfFives, int numOfSixes);

int
main(void){
    int numOfTurns,
		dice,
		printedStarNum,
		i; /* counter */
	char continueThrowing = 'y';
	int
        numOfOnes = 0,
        numOfTwos = 0,
        numOfThrees = 0,
        numOfFours = 0,
        numOfFives = 0,
        numOfSixes = 0;
		
	srand(time(NULL)); /* seed the generator */

	while(continueThrowing == 'y')
	{
		do
		{        
			numOfTurns = GetRandomInt(MAXTURN - MINTURN) + MINTURN;
		} while( (numOfTurns >= MAXTURN) || (numOfTurns <= MINTURN) );

		printf("Number of turns: %d\n", numOfTurns);
		numOfOnes = 0;
		numOfTwos = 0;
        numOfThrees = 0;
        numOfFours = 0;
        numOfFives = 0;
        numOfSixes = 0;
		for(i=0; i<numOfTurns; ++i)
		{
			dice = ThrowDice(&numOfOnes, &numOfTwos, &numOfThrees, &numOfFours, &numOfFives, &numOfSixes);
		}

		printedStarNum = PrintHistogram(numOfOnes, numOfTwos, numOfThrees, numOfFours, numOfFives, numOfSixes);

		if(DEBUG)
			printf("printed stars = %d user input = %d\n",printedStarNum, numOfTurns);


		printf("Do you want to continue? (y->yes others for exit)");
		scanf(" %c",&continueThrowing);

	}/* while continue*/
    return 1;
}


int
PrintHistogram(int numOfOnes, int numOfTwos, int numOfThrees,
                   int numOfFours, int numOfFives, int numOfSixes)
{
	int counter = 0,
		biggest,
		dicePerStar;

	biggest = FindBiggest(numOfOnes, numOfTwos, numOfThrees, numOfFours, numOfFives, numOfSixes);

	dicePerStar = (int)biggest / 40;

	numOfOnes /= dicePerStar;
	numOfTwos /= dicePerStar;
	numOfThrees /= dicePerStar;
	numOfFours /= dicePerStar;
	numOfFives /= dicePerStar;
	numOfSixes /= dicePerStar;

	if(DEBUG)
			printf("******1:%d 2:%d 3:%d 4:%d 5:%d 6:%d******\n", 
		            numOfOnes, numOfTwos, numOfThrees, numOfFours, numOfFives, numOfSixes);

	printf("1: ");
	if(numOfOnes > 0)
		counter += PrintStar(numOfOnes);

	printf("2: ");
	if(numOfTwos > 0)
		counter += PrintStar(numOfTwos);

	printf("3: ");
	if(numOfThrees > 0)
		counter += PrintStar(numOfThrees);

	printf("4: ");
	if(numOfFours > 0)
		counter += PrintStar(numOfFours);

	printf("5: ");
	if(numOfFives > 0)
		counter += PrintStar(numOfFives);

	printf("6: ");
	if(numOfSixes > 0)
		counter += PrintStar(numOfSixes);

	printf("Each * show %d turns.\n", dicePerStar);

	return counter;
}

int
FindBiggest(int numOfOnes, int numOfTwos, int numOfThrees,
            int numOfFours, int numOfFives, int numOfSixes)
{
	if( (numOfOnes -( numOfTwos+numOfThrees+numOfFours+numOfFives+numOfSixes)) > 0)
		return numOfOnes;

	else if( (numOfTwos -( numOfOnes+numOfThrees+numOfFours+numOfFives+numOfSixes)) > 0)
		return numOfTwos;

	else if( (numOfThrees -( numOfOnes+numOfTwos+numOfFours+numOfFives+numOfSixes)) > 0)
		return numOfThrees;

	else if( (numOfFours -( numOfOnes+numOfThrees+numOfTwos+numOfFives+numOfSixes)) > 0)
		return numOfFours;	

	else if( (numOfFives -( numOfOnes+numOfThrees+numOfTwos+numOfFours+numOfSixes)) > 0)
		return numOfFives;	

 	else /* numOfSixes is the biggest */
		return numOfSixes;	
}
int
PrintStar(int num)
{
    char ch = '*';
    int counter = 0, /* total printed char counter */
    i; /* for loop counter */

	if(num > 0)
	{
        for(i=0;i<num;++i)
            counter += printf("%c",ch);
        printf("\n"); /* line break */
    } 
   	return counter; 
}


int
ThrowDice(int *numOfOnes, int *numOfTwos, int *numOfThrees, 
            int *numOfFours, int *numOfFives, int *numOfSixes)
{
	int dice;
	dice = GetRandomInt(6) + 1;

	switch(dice)
				{
					case 1:
						++*numOfOnes;
						break;
					case 2:
						++*numOfTwos;
						break;
					case 3:
						++*numOfThrees;
						break;
					case 4:
						++*numOfFours;
						break;
					case 5:
						++*numOfFives;
						break;
					default: /* case = 6 */
						++*numOfSixes;
				}
}

void
TurnCounter(int currentTurn, int *numOfOnes, int *numOfTwos, int *numOfThrees, 
            int *numOfFours, int *numOfFives, int *numOfSixes)
{
	switch(currentTurn)
	{
		case 1:
			++*numOfOnes;
			break;
		case 2:
			++*numOfTwos;
			break;
		case 3:
			++*numOfThrees;
			break;
		case 4:
			++*numOfFours;
			break;
		case 5:
			++*numOfFives;
			break;
		default: /* case = 6 */
			++*numOfSixes;
	}
}

/* Generates random integer with given mod */
int
GetRandomInt(int mod)
{
    return rand() % mod;
}
