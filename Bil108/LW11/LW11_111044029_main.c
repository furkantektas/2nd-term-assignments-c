/* LW11_111044029.c Furkan Tektas 18.05.2012 */

#include "LW11_111044029_seller.c"

int
main(void)
{
	item_t	items[NUMOFITEMS],
		testItem;
	int i,j;
	FILE *file;
	itemWithCont_t	itemWithContacts[NUMOFITEMS];
	

	for(j=0;j<3; ++j) {
		getItems(items, NUMOFITEMS);
	
		for(i=0;i<NUMOFITEMS;++i)
			printItem(items[i]);

		if(NUMOFITEMS == writeItems(FILENAME, items, NUMOFITEMS))
			printf("Succesfully wrote data to file\n");
		else
			printf("Could not write data to file\n");	
	}
	
	printf("\nReading from file:\n");
	file = fopen(FILENAME, "rb");

	for(i=0;i<3*NUMOFITEMS; ++i) {
		testItem = readAnItem(file);

		if(testItem.id < 0)
			printf("Could not read an item from file\n");
		else
			printItem(testItem);
	}
	/* bonus */
	contactInfoAdder(items, NUMOFITEMS, itemWithContacts);
	for(i=0;i<NUMOFITEMS;++i)
		printItemWithContactInfo(itemWithContacts[i]);
	return 0;
}




