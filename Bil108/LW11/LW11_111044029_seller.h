#ifndef SELLER_H_INCLUDED
#define SELLER_H_INCLUDED

#define NUMOFITEMS	2
#define FILENAME	"LW_11_111044029_data.bin"

typedef struct {
	int id;
	char name[50];
	double price;
} item_t;

typedef union {
	int phone;
	char mail[25];
}contactInfo_t;

typedef struct {
	item_t item;
	contactInfo_t	contInfo;
}itemWithCont_t;



item_t *getItems(item_t items[], int size);
void printItem(item_t item);
int writeItems(char filename[],item_t items[], int size);
item_t readAnItem(FILE *file);
void contactInfoAdder(item_t items[], int size, itemWithCont_t contacts[]);

void flushInputBuffer();

#endif /* SELLER_H_INCLUDED */
