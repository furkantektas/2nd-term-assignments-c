/* 
 * File:   main.c
 * Author: furkan
 *
 * Created on June 7, 2012, 8:09 PM
 */

#include <stdio.h>
#include <stdlib.h>

#define SENTINEL -1

typedef struct list_s {
    int num;
    struct list_s *next;
} list_t;

/* Pre: tail is null */
list_t *push(list_t *element)
{
    int num;
    list_t *temp;
    
    /* filling new entry's name from user input */
    printf("Enter a num (%d for exit) => ", SENTINEL);
    scanf("%d",&num);
    
    if(num != SENTINEL) {
        temp = (list_t *)malloc(sizeof(list_t));
        temp->num = num;
        /* putting NULL to end of list */
        temp->next = NULL;
        temp = element;
        temp->next = push(element->next);
    }
    return element;
}

/*
 Deletes the next list element of element pointer.
 */
void
pop(list_t *element)
{
    list_t *temp;
    temp = element->next;
    element->next = element->next->next;
    free(temp);
}

void
popWithNum(int num, list_t *head) {
    int i;
    for(i=1;i<num;++i)
        head = head->next;
    pop(head);
}

void printList(list_t *head)
{
    if(head == NULL) {
        printf("List is empty!\n");
    }
    
    for(;head != NULL; head = head->next)
        printf("Num : %d \n", head->num);
}

/* stack */
int
main(int argc, char** argv) {
    int i;
    list_t *head,
           *tail;
    
    head = NULL;
    tail = NULL;
    
    /* Should print an error */
    printList(head);
    
    
    head = push(tail);
    

    printList(head);
    

    puts("Enter the order of element to pop");
    scanf("%d",&i);
    popWithNum(i,head);

    printList(head);
    
    return (EXIT_SUCCESS);
}

